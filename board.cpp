#include <stdio.h>
#include <string.h>
#include "board.h"
#include "hash.h"
#include "movechain.h"

#include <iostream>
using namespace std;

#ifdef WITHHASH
extern Hash HSH;
#endif

/*
	mvv-lva score 		unused		check	en-passant		promo		beatenfigure	figure		to	  		from
		  	  111| 	  	 0| 	  		1|			1|		111|		111|			111|		111111|		111111
	   	   	 	24 	 	 23	     		22			21       18  		   15   		   12     	      6          0
*/

Board::Board()
{
	initClear();
}
// ############for testing purpuses
void Board::testing_clone(Board *brdcopy)
{

	brdcopy->wb_figures = wb_figures;
	brdcopy->w_figures = w_figures;
	brdcopy->b_figures = b_figures;
	brdcopy->w_pawns = w_pawns;
	brdcopy->w_knights = w_knights;
	brdcopy->w_bishops = w_bishops;
	brdcopy->w_rooks = w_rooks;
	brdcopy->w_queens = w_queens;
	brdcopy->w_kings = w_kings;
	brdcopy->b_pawns = b_pawns;
	brdcopy->b_knights = b_knights;
	brdcopy->b_bishops = b_bishops;
	brdcopy->b_rooks = b_rooks;
	brdcopy->b_queens = b_queens;
	brdcopy->b_kings = b_kings;

	brdcopy->w_enpassant = w_enpassant;
	brdcopy->b_enpassant = b_enpassant;

	brdcopy->number_of_white_figures = number_of_white_figures;
	brdcopy->number_of_white_pawns = number_of_white_pawns;
	brdcopy->number_of_white_rooks = number_of_white_rooks;
	brdcopy->number_of_white_knights = number_of_white_knights;
	brdcopy->number_of_white_bishops = number_of_white_bishops;
	brdcopy->number_of_white_queens = number_of_white_queens;

	brdcopy->number_of_black_figures = number_of_black_figures;
	brdcopy->number_of_black_pawns = number_of_black_pawns;
	brdcopy->number_of_black_rooks = number_of_black_rooks;
	brdcopy->number_of_black_knights = number_of_black_knights;
	brdcopy->number_of_black_bishops = number_of_black_bishops;
	brdcopy->number_of_black_queens = number_of_black_queens;

	brdcopy->number_of_total_figures = number_of_total_figures;

	brdcopy->wOOrokirovka = wOOrokirovka;
	brdcopy->bOOrokirovka = bOOrokirovka;

	brdcopy->wOOOrokirovka = wOOOrokirovka;
	brdcopy->bOOOrokirovka = bOOOrokirovka;

	brdcopy->wkingmoved = wkingmoved;
	brdcopy->bkingmoved = bkingmoved;

	brdcopy->z_hash = z_hash;
}
bool Board::testing_compare(Board *brdcopy)
{

	if (brdcopy->wb_figures != wb_figures)
		return false;
	if (brdcopy->w_figures != w_figures)
		return false;
	if (brdcopy->b_figures != b_figures)
		return false;
	if (brdcopy->w_pawns != w_pawns)
		return false;
	if (brdcopy->w_knights != w_knights)
		return false;
	if (brdcopy->w_bishops != w_bishops)
		return false;
	if (brdcopy->w_rooks != w_rooks)
		return false;
	if (brdcopy->w_queens != w_queens)
		return false;
	if (brdcopy->w_kings != w_kings)
		return false;
	if (brdcopy->b_pawns != b_pawns)
		return false;
	if (brdcopy->b_knights != b_knights)
		return false;
	if (brdcopy->b_bishops != b_bishops)
		return false;
	if (brdcopy->b_rooks != b_rooks)
		return false;
	if (brdcopy->b_queens != b_queens)
		return false;
	if (brdcopy->b_kings != b_kings)
		return false;

	//if(brdcopy->w_enpassant!=w_enpassant)return false;
	//if(brdcopy->b_enpassant!=b_enpassant)return false;

	if (brdcopy->number_of_white_figures != number_of_white_figures)
		return false;
	if (brdcopy->number_of_white_pawns != number_of_white_pawns)
		return false;
	if (brdcopy->number_of_white_rooks != number_of_white_rooks)
		return false;
	if (brdcopy->number_of_white_knights != number_of_white_knights)
		return false;
	if (brdcopy->number_of_white_bishops != number_of_white_bishops)
		return false;
	if (brdcopy->number_of_white_queens != number_of_white_queens)
		return false;

	if (brdcopy->number_of_black_figures != number_of_black_figures)
		return false;
	if (brdcopy->number_of_black_pawns != number_of_black_pawns)
		return false;
	if (brdcopy->number_of_black_rooks != number_of_black_rooks)
		return false;
	if (brdcopy->number_of_black_knights != number_of_black_knights)
		return false;
	if (brdcopy->number_of_black_bishops != number_of_black_bishops)
		return false;
	if (brdcopy->number_of_black_queens != number_of_black_queens)
		return false;

	if (brdcopy->number_of_total_figures != number_of_total_figures)
		return false;

	if (brdcopy->wOOrokirovka != wOOrokirovka)
		return false;
	if (brdcopy->bOOrokirovka != bOOrokirovka)
		return false;

	if (brdcopy->wOOOrokirovka != wOOOrokirovka)
		return false;
	if (brdcopy->bOOOrokirovka != bOOOrokirovka)
		return false;

	if (brdcopy->wkingmoved != wkingmoved)
		return false;
	if (brdcopy->bkingmoved != bkingmoved)
		return false;

	if (brdcopy->z_hash != z_hash)
		return false;

	return true;
}

void Board::initClear()
{
	wb_figures = 0;
	w_figures = 0;
	b_figures = 0;
	w_pawns = 0;
	w_knights = 0;
	w_bishops = 0;
	w_rooks = 0;
	w_queens = 0;
	w_kings = 0;
	b_pawns = 0;
	b_knights = 0;
	b_bishops = 0;
	b_rooks = 0;
	b_queens = 0;
	b_kings = 0;

	w_enpassant = 0;
	b_enpassant = 0;

	number_of_white_figures = 0;
	number_of_white_pawns = 0;
	number_of_white_rooks = 0;
	number_of_white_knights = 0;
	number_of_white_bishops = 0;
	number_of_white_queens = 0;

	number_of_black_figures = 0;
	number_of_black_pawns = 0;
	number_of_black_rooks = 0;
	number_of_black_knights = 0;
	number_of_black_bishops = 0;
	number_of_black_queens = 0;

	number_of_total_figures = 0;

	wOOrokirovka = false;
	bOOrokirovka = false;

	wOOOrokirovka = false;
	bOOOrokirovka = false;

	wkingmoved = 0;
	bkingmoved = 0;

	z_hash = 0;
}

void Board::initDefault()
{
	initClear();
	setFigure('P', "A2");
	setFigure('P', "B2");
	setFigure('P', "C2");
	setFigure('P', "D2");
	setFigure('P', "E2");
	setFigure('P', "F2");
	setFigure('P', "G2");
	setFigure('P', "H2");
	setFigure('R', "A1");
	setFigure('R', "H1");
	setFigure('N', "B1");
	setFigure('N', "G1");
	setFigure('B', "C1");
	setFigure('B', "F1");
	setFigure('Q', "D1");
	setFigure('K', "E1");

	setFigure('p', "A7");
	setFigure('p', "B7");
	setFigure('p', "C7");
	setFigure('p', "D7");
	setFigure('p', "E7");
	setFigure('p', "F7");
	setFigure('p', "G7");
	setFigure('p', "H7");
	setFigure('r', "A8");
	setFigure('r', "H8");
	setFigure('n', "B8");
	setFigure('n', "G8");
	setFigure('b', "C8");
	setFigure('b', "F8");
	setFigure('q', "D8");
	setFigure('k', "E8");
}

bool Board::setFigure(char figure, const char *pole)
{
	int pos;
	BITBOARD bitboardpos;

	pos = poleToPosition(pole);
	bitboardpos = positionToBITBoard(pos);

	if ((wb_figures & bitboardpos) != 0)
		return false;

	wb_figures |= bitboardpos;
	number_of_total_figures++;

	if (nameToColor(figure) == WHITE)
	{
		w_figures |= bitboardpos;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][nameToType(figure)][pos];
#endif

		switch (nameToType(figure))
		{
		case PAWN:
			w_pawns |= bitboardpos;
			number_of_white_pawns++;
			break;
		case ROOK:
			w_rooks |= bitboardpos;
			number_of_white_rooks++;
			break;
		case KNIGHT:
			w_knights |= bitboardpos;
			number_of_white_knights++;
			break;
		case BISHOP:
			w_bishops |= bitboardpos;
			number_of_white_bishops++;
			break;
		case KING:
			w_kings |= bitboardpos;
			break;
		case QUEEN:
			w_queens |= bitboardpos;
			number_of_white_queens++;
			break;
		}
		number_of_white_figures++;
		return true;
	}
	if (nameToColor(figure) == BLACK)
	{
		b_figures |= bitboardpos;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][nameToType(figure)][pos];
#endif

		switch (nameToType(figure))
		{
		case PAWN:
			b_pawns |= bitboardpos;
			number_of_black_pawns++;
			break;
		case ROOK:
			b_rooks |= bitboardpos;
			number_of_black_rooks++;
			break;
		case KNIGHT:
			b_knights |= bitboardpos;
			number_of_black_knights++;
			break;
		case BISHOP:
			b_bishops |= bitboardpos;
			number_of_black_bishops++;
			break;
		case KING:
			b_kings |= bitboardpos;
			break;
		case QUEEN:
			b_queens |= bitboardpos;
			number_of_black_queens++;
			break;
		}
		number_of_black_figures++;
		return true;
	}

	return false;
}

void Board::showBoardDebug() const
{
	int pos;
	BITBOARD bitboardpos;

	for (int i = 7; i >= 0; i--)
	{
		for (int j = 0; j < 8; j++)
		{
			pos = j + i * 8;
			bitboardpos = positionToBITBoard(pos);
			if (((wb_figures & bitboardpos) == 0))
				cout << ". ";
			else
			{
				if ((w_figures & bitboardpos) != 0)
				{
					if ((w_pawns & bitboardpos) != 0)
						cout << "P ";
					if ((w_rooks & bitboardpos) != 0)
						cout << "R ";
					if ((w_knights & bitboardpos) != 0)
						cout << "N ";
					if ((w_bishops & bitboardpos) != 0)
						cout << "B ";
					if ((w_kings & bitboardpos) != 0)
						cout << "K ";
					if ((w_queens & bitboardpos) != 0)
						cout << "Q ";
				}
				if ((b_figures & bitboardpos) != 0)
				{
					if ((b_pawns & bitboardpos) != 0)
						cout << "p ";
					if ((b_rooks & bitboardpos) != 0)
						cout << "r ";
					if ((b_knights & bitboardpos) != 0)
						cout << "n ";
					if ((b_bishops & bitboardpos) != 0)
						cout << "b ";
					if ((b_kings & bitboardpos) != 0)
						cout << "k ";
					if ((b_queens & bitboardpos) != 0)
						cout << "q ";
				}
			}
		}
		cout << endl;
	}
	cout << endl
		 << std::flush;
}

void Board::addFigureCapturesAndMoves(char color, BITBOARD bb, BITBOARD ourfrom, MoveList *ml, char req_hashdepth)
{
	BITBOARD ourcaptures;
	BITBOARD ourmoves;
	unsigned char to;
	int move;

	ourmoves = (bb ^ wb_figures) & bb;
	if (color == WHITE)
		ourcaptures = bb & b_figures;
	else
		ourcaptures = bb & w_figures;

	while (ourcaptures)
	{
		to = LastOne(ourcaptures);
		if (color == WHITE)
			move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
		else
			move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
		makeCapture(move, color);
		if (!underCheck(color))
		{

#ifdef WITHHASHSORT
			ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif

#ifndef WITHHASHSORT
			ml->addCapture(move);
#endif
		}
		rollBackCapture(move, color);
		Clear(to, &ourcaptures);
	}
	while (ourmoves)
	{
		to = LastOne(ourmoves);
		move = ourfrom | (to << 6);
		makeMove(move, color);
		if (!underCheck(color))
		{

#ifdef WITHHASHSORT
			//			if(z_hash==461172452919494632){
			//				showBoardDebug();
			//				cout << showMoveUCI(move);
			//			}
			ml->addMove(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif

#ifndef WITHHASHSORT
			ml->addMove(move);
#endif
		}
		rollBackMove(move, color);
		Clear(to, &ourmoves);
	}
}

void Board::addFigureCaptures(char color, BITBOARD bb, BITBOARD ourfrom, MoveList *ml, char req_hashdepth)
{
	BITBOARD ourcaptures;
	unsigned char to;
	int move;

	if (color == WHITE)
		ourcaptures = bb & b_figures;
	else
		ourcaptures = bb & w_figures;

	while (ourcaptures)
	{
		to = LastOne(ourcaptures);
		if (color == WHITE)
			move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
		else
			move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
		makeCapture(move, color);
		if (!underCheck(color))
		{
#ifdef WITHHASHSORT
			ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
			ml->addCapture(move);
#endif
		}
		rollBackCapture(move, color);
		Clear(to, &ourcaptures);
	}
}

void Board::addFigureCapturesAndChecks(char color, BITBOARD bb, BITBOARD ourfrom, MoveList *ml, char req_hashdepth)
{
	BITBOARD ourcaptures;
	BITBOARD ourmoves;
	unsigned char to;
	int move;

	ourmoves = (bb ^ wb_figures) & bb;
	if (color == WHITE)
		ourcaptures = bb & b_figures;
	else
		ourcaptures = bb & w_figures;

	while (ourcaptures)
	{
		to = LastOne(ourcaptures);
		if (color == WHITE)
			move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
		else
			move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
		makeCapture(move, color);
		if (!underCheck(color))
		{
#ifdef WITHHASHSORT
			ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
			ml->addCapture(move);
#endif
		}
		rollBackCapture(move, color);
		Clear(to, &ourcaptures);
	}
	while (ourmoves)
	{
		to = LastOne(ourmoves);
		move = ourfrom | (to << 6);
		makeMove(move, color);
		if (!underCheck(color))
		{
			if (underCheck(3 - color))
#ifdef WITHHASHSORT
				ml->addMove(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
			ml->addMove(move);
#endif
		}
		rollBackMove(move, color);
		Clear(to, &ourmoves);
	}
}

void Board::generateMoveList(MoveList *ml, char color, int killer1, int killer2, char req_hashdepth)
{
	BITBOARD ourfigures;
	BITBOARD bb;
	unsigned char from;
	int ourfrom;

	BITBOARD ourmoves;
	unsigned char to;
	int move;

	//clearEnPassant(color);

	ml->init(color);

	if (color == WHITE)
	{
		if (underCheck(WHITE))
		{
			ml->setMateFlag();
		}
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			ourfrom = from + (KNIGHT << 12);
			bb = BB.knight_bitboard[from];

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_kings;
		//kings moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourfrom = from + (KING << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourfrom = from + (BISHOP << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourfrom = from + (ROOK << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourfrom = from + (QUEEN << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from > 15)
				bb = BB.wpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom2Line(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				if (to > 55)
					move |= (QUEEN << 18);
				makeMove(move, 1);
				if (!underCheck(WHITE))
				{
#ifdef WITHHASHSORT
					ml->addMove(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addMove(move);
#endif
				}
				rollBackMove(move, 1);
				Clear(to, &ourmoves);
			}

			//wpawn captures
			bb = BB.wpawn_capture_bitboard[from] & b_figures;
			ourmoves = bb & b_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				if (to > 55)
					move |= (QUEEN << 18);
				makeCapture(move, 1);
				if (!underCheck(WHITE))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, 1);
				Clear(to, &ourmoves);
			}

			//wpawn en passant captures
			bb = BB.wpawn_capture_bitboard[from] & b_enpassant;
			ourmoves = bb & b_enpassant; // double?
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				makeCapture(move, WHITE);
				if (!underCheck(WHITE))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, WHITE);
				Clear(to, &ourmoves);
			}

			Clear(from, &ourfigures);
		}

		// white rokirovka O-O
		if ((!wkingmoved) && (!wOOrokirovka))
			if (w_kings & E1) //double check for static positions
				if (!(wb_figures & F1G1))
					if (w_rooks & H1)
						if (!ml->mateflag)
						{ // if king under check - at e1 - forbiden
							w_kings = F1;
							if (!underCheck(1))
							{
								w_kings = G1;
								if (!underCheck(WHITE))
								{
#ifdef WITHHASHSORT
									w_kings = E1;
									makeMove(1, WHITE); //only for hash update
									ml->addMove(1, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
									rollBackMove(1, WHITE);
#endif
#ifndef WITHHASHSORT
									ml->addMove(1);
#endif
								}
							}
							w_kings = E1;
						}
		// white rokirovka O-O-O
		if ((!wkingmoved) && (!wOOOrokirovka))
			if (w_kings & E1) //double check for static positions
				if (!(wb_figures & B1C1D1))
					if (w_rooks & A1)
						if (!ml->mateflag) // if king under check - at e1 - forbiden
						{
							w_kings = D1;
							if (!underCheck(1))
							{
								w_kings = C1;
								if (!underCheck(WHITE))
								{
#ifdef WITHHASHSORT
									w_kings = E1;
									makeMove(2, WHITE); //only for hash update
									ml->addMove(2, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
									rollBackMove(2, WHITE);
#endif
#ifndef WITHHASHSORT
									ml->addMove(2);
#endif
								}
							}
							w_kings = E1;
						}
	}
	////////////////////////////////////////////////////////////////////////
	//black
	///////////////////////////////////////////////////////////////////////
	if (color == BLACK)
	{
		if (underCheck(BLACK))
			ml->setMateFlag();
		//knights
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];
			ourfrom = from + (KNIGHT << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth - 1);

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_kings;
		//kings moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourfrom = from + (KING << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourfrom = from + (BISHOP << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourfrom = from + (ROOK << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourfrom = from + (QUEEN << 12);

			addFigureCapturesAndMoves(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from < 48)
				bb = BB.bpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom7Line(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				if (to < 8)
					move |= (QUEEN << 18);
				makeMove(move, 2);
				if (!underCheck(BLACK))
				{
#ifdef WITHHASHSORT
					ml->addMove(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addMove(move);
#endif
				}
				rollBackMove(move, 2);
				Clear(to, &ourmoves);
			}

			//bpawn captures
			bb = BB.bpawn_capture_bitboard[from] & w_figures;
			ourmoves = bb & w_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				if (to < 8)
					move |= (QUEEN << 18);
				makeCapture(move, BLACK);
				if (!underCheck(BLACK))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, BLACK);
				Clear(to, &ourmoves);
			}

			//bpawn en passant captures
			bb = BB.bpawn_capture_bitboard[from] & w_enpassant;
			ourmoves = bb & w_enpassant;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				makeCapture(move, BLACK);
				if (!underCheck(BLACK))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, BLACK);
				Clear(to, &ourmoves);
			}

			Clear(from, &ourfigures);
		}

		// black rokirovka O-O
		if ((!bkingmoved) && (!bOOrokirovka))
			if (b_kings & E8) //double check for static positions
				if (!(wb_figures & F8G8))
					if (b_rooks & H8)
						if (!ml->mateflag) // if king under check - at e1 - forbiden
						{
							b_kings = F8;
							if (!underCheck(2))
							{
								b_kings = G8;
								if (!underCheck(BLACK))
								{
#ifdef WITHHASHSORT
									b_kings = E8;
									makeMove(3, BLACK); //only for hash update
									ml->addMove(3, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
									rollBackMove(3, BLACK);
#endif
#ifndef WITHHASHSORT
									ml->addMove(3);
#endif
								}
							}
							b_kings = E8;
						}
		// black rokirovka O-O-O
		if ((!bkingmoved) && (!bOOOrokirovka))
			if (b_kings & E8) //double check for static positions
				if (!(wb_figures & B8C8D8))
					if (b_rooks & A8)
						if (!ml->mateflag) // if king under check - at e1 - forbiden
						{
							b_kings = D8;
							if (!underCheck(2))
							{
								b_kings = C8;
								if (!underCheck(BLACK))
								{
#ifdef WITHHASHSORT
									b_kings = E8;
									makeMove(4, BLACK); //only for hash update
									ml->addMove(4, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
									rollBackMove(4, BLACK);
#endif
#ifndef WITHHASHSORT
									ml->addMove(4);
#endif
								}
							}
							b_kings = E8;
						}
	}
	ml->sortMoveList(killer1, killer2, HSH.hashProbeMove(color,z_hash,0));
}

void Board::generatePLMoveList(MoveList *ml, char color)
{
	BITBOARD ourfigures;
	BITBOARD ourcaptures;
	BITBOARD ourmoves;
	BITBOARD bb;
	unsigned char from, to;
	int move;
	int ourfrom;

	ml->init(color);

	if (color == WHITE)
	{

		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];
			ourfrom = from + (KNIGHT << 12);

			ourmoves = (bb ^ wb_figures) & bb;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & b_figures;
			while (ourcaptures)
			{
				to = LastOne(ourcaptures);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourcaptures);
			}

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_kings;
		//kings moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (KING << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & b_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (BISHOP << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & b_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (ROOK << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & b_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			//	    ourmoves = (bb ^ wb_figures) & bb; // dont like wandering queen
			ourfrom = from + (QUEEN << 12);
			//	    while (ourmoves) {
			//			to = LastOne(ourmoves);
			//			move = ourfrom | (to << 6);
			//			ml->addMove(move);
			//			Clear(to, &ourmoves);
			//	    }

			ourmoves = bb & b_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from > 15)
				bb = BB.wpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom2Line(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				if (to > 55)
					move |= (QUEEN << 18);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			//wpawn captures
			bb = BB.wpawn_capture_bitboard[from] & b_figures;
			ourmoves = bb & b_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				if (to > 55)
					move |= (QUEEN << 18);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}

			//wpawn en passant captures
			bb = BB.wpawn_capture_bitboard[from] & b_enpassant;
			ourmoves = bb & b_enpassant;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		// white rokirovka O-O
		if (!wkingmoved)
			if (!(wb_figures & F1G1))
				if (w_rooks & H1)
					ml->addMove(1); //1 O-O

		// white rokirovka O-O-O
		if (!wkingmoved)
			if (!(wb_figures & B1C1D1))
				if (w_rooks & A1)
					ml->addMove(2); //2 O-O-O
									////////////////////////////////////////////////////////////////////////
	}
	//black
	///////////////////////////////////////////////////////////////////////
	if (color == BLACK)
	{
		//knights
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];
			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (KNIGHT << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & w_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_kings;
		//kings moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (KING << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & w_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (BISHOP << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & w_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (ROOK << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			ourmoves = bb & w_figures;

			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			//	    ourmoves = (bb ^ wb_figures) & bb; // dont like wandering queen
			ourfrom = from + (QUEEN << 12);
			//	    while (ourmoves) {
			//			to = LastOne(ourmoves);
			//			move = ourfrom | (to << 6);
			//			ml->addMove(move);
			//			Clear(to, &ourmoves);
			//	    }

			ourmoves = bb & w_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}
			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from < 48)
				bb = BB.bpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom7Line(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				if (to < 8)
					move |= (QUEEN << 18);
				ml->addMove(move);
				Clear(to, &ourmoves);
			}

			//bpawn captures
			bb = BB.bpawn_capture_bitboard[from] & w_figures;
			ourmoves = bb & w_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				if (to < 8)
					move |= (QUEEN << 18);
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}

			//bpawn en passant captures
			bb = BB.bpawn_capture_bitboard[from] & w_enpassant;
			ourmoves = bb & w_enpassant;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				ml->addCapture(move);
				Clear(to, &ourmoves);
			}

			Clear(from, &ourfigures);
		}

		// black rokirovka O-O
		if (!bkingmoved)
			if (!(wb_figures & F8G8))
				if (b_rooks & H8)
					ml->addMove(3); //1 O-O

		// black rokirovka O-O-O
		if (!bkingmoved)
			if (!(wb_figures & B8C8D8))
				if (b_rooks & A8)
					ml->addMove(4); //2 O-O-O
	}
}

void Board::generatePLMoveCount(MoveList *ml, char color)
{
	BITBOARD ourfigures;
	BITBOARD ourcaptures;
	BITBOARD ourmoves;
	BITBOARD bb;
	unsigned char from;
	int mvcnt;

	ml->init(color);

	if (color == WHITE)
	{

		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];

			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & b_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_kings;
		//kings moves
		from = LastOne(ourfigures);
		bb = BB.king_bitboard[from];
		ourmoves = (bb ^ wb_figures) & bb;
		ourcaptures = bb & b_figures;

		mvcnt = PopCnt(ourmoves);
		ml->totalmoves += mvcnt;
		ml->totalmacs += mvcnt;

		mvcnt = PopCnt(ourcaptures);
		ml->totalcaptures += mvcnt;
		ml->totalmacs += mvcnt;

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & b_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & b_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & b_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from > 15)
				bb = BB.wpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom2Line(from);

			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			//wpawn captures
			bb = BB.wpawn_capture_bitboard[from] & b_figures;
			ourcaptures = bb & b_figures;
			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			//wpawn en passant captures
			bb = BB.wpawn_capture_bitboard[from] & b_enpassant;
			ourcaptures = bb & b_enpassant;
			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		// white rokirovka O-O
		if (!wkingmoved)
			if (!(wb_figures & F1G1))
				if (w_rooks & H1)
					ml->addMove(1); //1 O-O

		// white rokirovka O-O-O
		if (!wkingmoved)
			if (!(wb_figures & B1C1D1))
				if (w_rooks & A1)
					ml->addMove(2); //2 O-O-O
									////////////////////////////////////////////////////////////////////////
	}
	//black
	///////////////////////////////////////////////////////////////////////
	if (color == BLACK)
	{
		//knights
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];

			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & w_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_kings;
		//kings moves
		from = LastOne(ourfigures);
		bb = BB.king_bitboard[from];
		ourmoves = (bb ^ wb_figures) & bb;
		ourcaptures = bb & w_figures;

		mvcnt = PopCnt(ourmoves);
		ml->totalmoves += mvcnt;
		ml->totalmacs += mvcnt;

		mvcnt = PopCnt(ourcaptures);
		ml->totalcaptures += mvcnt;
		ml->totalmacs += mvcnt;

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & w_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & w_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourcaptures = bb & w_figures;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from < 48)
				bb = BB.bpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom7Line(from);
			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			//bpawn captures
			bb = BB.bpawn_capture_bitboard[from] & w_figures;
			ourcaptures = bb & w_figures;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			//bpawn en passant captures
			bb = BB.bpawn_capture_bitboard[from] & w_enpassant;
			ourcaptures = bb & w_enpassant;

			mvcnt = PopCnt(ourcaptures);
			ml->totalcaptures += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		// black rokirovka O-O
		if (!bkingmoved)
			if (!(wb_figures & F8G8))
				if (b_rooks & H8)
					ml->addMove(3); //1 O-O

		// black rokirovka O-O-O
		if (!bkingmoved)
			if (!(wb_figures & B8C8D8))
				if (b_rooks & A8)
					ml->addMove(4); //2 O-O-O
	}
}

void Board::generatePLPLMoveCount(MoveList *ml, char color) //extra light version without captures, enpass, castling
{
	BITBOARD ourfigures;
	BITBOARD ourmoves;
	BITBOARD bb;
	unsigned char from;
	int mvcnt;

	ml->init(color);

	if (color == WHITE)
	{

		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];

			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_kings;
		//kings moves
		from = LastOne(ourfigures);
		bb = BB.king_bitboard[from];
		ourmoves = (bb ^ wb_figures) & bb;

		mvcnt = PopCnt(ourmoves);
		ml->totalmoves += mvcnt;
		ml->totalmacs += mvcnt;

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);

			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);

			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}
		/*
//queens
/////////////////////////////////////////////////////////////////////////
	ourfigures = w_queens;
//queens moves
	while (ourfigures) {
	    from = LastOne(ourfigures);
	    bb = kvinskafunk(from);
	    ourmoves = (bb ^ wb_figures) & bb;

	    mvcnt=PopCnt(ourmoves);
	    ml->totalmoves+=mvcnt;
	    ml->totalmacs+=mvcnt;

	    Clear(from, &ourfigures);
	}
*/
		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from > 15)
				bb = BB.wpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom2Line(from);

			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}
	}
	//black
	///////////////////////////////////////////////////////////////////////
	if (color == BLACK)
	{
		//knights
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];

			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_kings;
		//kings moves
		from = LastOne(ourfigures);
		bb = BB.king_bitboard[from];
		ourmoves = (bb ^ wb_figures) & bb;

		mvcnt = PopCnt(ourmoves);
		ml->totalmoves += mvcnt;
		ml->totalmacs += mvcnt;

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourmoves = (bb ^ wb_figures) & bb;
			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}
		/*
//queens
/////////////////////////////////////////////////////////////////////////
	ourfigures = b_queens;
//queens moves
	while (ourfigures) {
	    from = LastOne(ourfigures);
	    bb = kvinskafunk(from);
	    ourmoves = (bb ^ wb_figures) & bb;

	    mvcnt=PopCnt(ourmoves);
	    ml->totalmoves+=mvcnt;
	    ml->totalmacs+=mvcnt;

	    Clear(from, &ourfigures);
	}
*/
		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from < 48)
				bb = BB.bpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom7Line(from);
			ourmoves = (bb ^ wb_figures) & bb;

			mvcnt = PopCnt(ourmoves);
			ml->totalmoves += mvcnt;
			ml->totalmacs += mvcnt;

			Clear(from, &ourfigures);
		}
	}
}

void Board::generateCaptureAndCheckList(MoveList *ml, char color, int killer1, int killer2)
{

	BITBOARD ourfigures;
	BITBOARD ourmoves;
	BITBOARD bb;
	unsigned char from, to;
	int move;
	int ourfrom;

	char req_hashdepth = 0;

	//clearEnPassant(color); //possible we should do it at make move

	ml->init(color);

	if (color == WHITE)
	{
		/*    	if (underCheck(1)) {// Looks like we cann not be shure to se mate flag if move list is shortened
    		ml->setMateFlag();
	}*/
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];
			ourfrom = from + (KNIGHT << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_kings;
		//kings moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourfrom = from + (KING << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourfrom = from + (BISHOP << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourfrom = from + (ROOK << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourfrom = from + (QUEEN << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);

			if (from > 15)
				bb = BB.wpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom2Line(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				if (to > 55)
					move |= (QUEEN << 18);
				makeMove(move, 1);
				if (!underCheck(1))
					if (underCheck(2))
						ml->addCheck(move);
				rollBackMove(move, 1);
				Clear(to, &ourmoves);
			}

			bb = BB.wpawn_capture_bitboard[from] & b_figures;
			ourmoves = bb & b_figures;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				if (to > 55)
					move |= (QUEEN << 18);
				makeCapture(move, 1);
				if (!underCheck(WHITE))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, 1);
				Clear(to, &ourmoves);
			}

			//wpawn en passant captures
			bb = BB.wpawn_capture_bitboard[from] & b_enpassant;
			ourmoves = bb & b_enpassant;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				makeCapture(move, WHITE);
				if (!underCheck(WHITE))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, WHITE);
				Clear(to, &ourmoves);
			}

			Clear(from, &ourfigures);
		}
	}
	if (color == BLACK)
	{
		/*    	if (underCheck(2))
    		ml->setMateFlag();*/
		//knights
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_knights;
		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];
			ourfrom = from + (KNIGHT << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_kings;
		//kings moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourfrom = from + (KING << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourfrom = from + (BISHOP << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourfrom = from + (ROOK << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourfrom = from + (QUEEN << 12);

			addFigureCapturesAndChecks(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_pawns;
		//pawns moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			if (from < 48)
				bb = BB.bpawn_forward_bitboard[from];
			else
				bb = pawnMoveFuncFrom7Line(from);

			ourmoves = (bb ^ wb_figures) & bb;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6);
				if (to < 8)
					move |= (QUEEN << 18);
				makeMove(move, 2);
				if (!underCheck(2))
					if (underCheck(1))
						ml->addCheck(move);
				rollBackMove(move, 2);
				Clear(to, &ourmoves);
			}

			bb = BB.bpawn_capture_bitboard[from] & w_figures;
			ourmoves = bb & w_figures;

			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				if (to < 8)
					move |= (QUEEN << 18);
				makeCapture(move, 2);
				if (!underCheck(BLACK))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, 2);
				Clear(to, &ourmoves);
			}

			//bpawn en passant captures
			bb = BB.bpawn_capture_bitboard[from] & w_enpassant;
			ourmoves = bb & w_enpassant;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				makeCapture(move, BLACK);
				if (!underCheck(BLACK))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, BLACK);
				Clear(to, &ourmoves);
			}

			Clear(from, &ourfigures);
		}
	}
	ml->sortMoveList(killer1, killer2, HSH.hashProbeMove(color,z_hash,0));
}

void Board::generateCaptureList(MoveList *ml, char color, int killer1, int killer2)
{

	BITBOARD ourfigures;
	BITBOARD ourmoves;
	BITBOARD bb;
	unsigned char from, to;
	int move;
	int ourfrom;

	char req_hashdepth = 0;

	//clearEnPassant(color);

	ml->init(color);

	if (color == WHITE)
	{
		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_pawns;
		//pawns captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);

			bb = BB.wpawn_capture_bitboard[from] & b_figures;
			ourmoves = bb & b_figures;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfBlackFigureOn(to) << 15);
				if (to > 55)
					move |= (QUEEN << 18);
				makeCapture(move, 1);
				if (!underCheck(WHITE))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, 1);
				Clear(to, &ourmoves);
			}

			//wpawn en passant captures
			bb = BB.wpawn_capture_bitboard[from] & b_enpassant;
			ourmoves = bb & b_enpassant;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				makeCapture(move, WHITE);
				if (!underCheck(WHITE))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, WHITE);
				Clear(to, &ourmoves);
			}

			Clear(from, &ourfigures);
		}
		//knights
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_knights;
		//knight captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];
			ourfrom = from + (KNIGHT << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_bishops;

		//bishops captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourfrom = from + (BISHOP << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_rooks;

		//rooks captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourfrom = from + (ROOK << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_queens;

		//queens captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourfrom = from + (QUEEN << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}
		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_kings;
		//kings captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourfrom = from + (KING << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}
	}
	if (color == BLACK)
	{
		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_pawns;
		//pawns captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.bpawn_capture_bitboard[from] & w_figures;
			ourmoves = bb & w_figures;
			ourfrom = from + (PAWN << 12);
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (positionOfWhiteFigureOn(to) << 15);
				if (to < 8)
					move |= (QUEEN << 18);
				makeCapture(move, 2);
				if (!underCheck(BLACK))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, 2);
				Clear(to, &ourmoves);
			}

			//bpawn en passant captures
			bb = BB.bpawn_capture_bitboard[from] & w_enpassant;
			ourmoves = bb & w_enpassant;
			while (ourmoves)
			{
				to = LastOne(ourmoves);
				move = ourfrom | (to << 6) | (PAWN << 15) | (1 << 21); //add enpass
				makeCapture(move, BLACK);
				if (!underCheck(BLACK))
				{
#ifdef WITHHASHSORT
					ml->addCapture(move, -HSH.hashProbeQuick(3 - color, z_hash, req_hashdepth - 1));
#endif
#ifndef WITHHASHSORT
					ml->addCapture(move);
#endif
				}
				rollBackCapture(move, BLACK);
				Clear(to, &ourmoves);
			}

			Clear(from, &ourfigures);
		}

		//knights
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_knights;

		//knight captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];
			ourfrom = (int)(from + (KNIGHT << 12));

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_bishops;

		//bishops captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);
			ourfrom = from + (BISHOP << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_rooks;

		//rooks captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);
			ourfrom = from + (ROOK << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_queens;

		//queens captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourfrom = from + (QUEEN << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}
		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_kings;

		//kings captures
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.king_bitboard[from];
			ourfrom = from + (KING << 12);

			addFigureCaptures(color, bb, ourfrom, ml, req_hashdepth);

			Clear(from, &ourfigures);
		}
	}

	ml->sortMoveList(killer1, killer2, HSH.hashProbeMove(color,z_hash,0));
}

char Board::positionOfBlackFigureOn(unsigned char to)
{
	BITBOARD tempcapture = BB.bitpos[to];
	if ((tempcapture & b_pawns) != 0)
		return PAWN;
	else if ((tempcapture & b_rooks) != 0)
		return ROOK;
	else if ((tempcapture & b_knights) != 0)
		return KNIGHT;
	else if ((tempcapture & b_bishops) != 0)
		return BISHOP;
	else if ((tempcapture & b_kings) != 0)
		return KING;
	else if ((tempcapture & b_queens) != 0)
		return QUEEN;
	return 0;
}

char Board::positionOfWhiteFigureOn(unsigned char to)
{
	BITBOARD tempcapture = BB.bitpos[to];
	if ((tempcapture & w_pawns) != 0)
		return PAWN;
	else if ((tempcapture & w_rooks) != 0)
		return ROOK;
	else if ((tempcapture & w_knights) != 0)
		return KNIGHT;
	else if ((tempcapture & w_bishops) != 0)
		return BISHOP;
	else if ((tempcapture & w_kings) != 0)
		return KING;
	else if ((tempcapture & w_queens) != 0)
		return QUEEN;
	return 0;
}

///////////////////////////////////////////////////////////////
//                under check                               //
///////////////////////////////////////////////////////////////

bool Board::underCheck(char color)
{
	BITBOARD func;
	unsigned char from;
	// white
	if (color == WHITE)
	{ //threat to white king
		from = LastOne(w_kings);
		if (BB.knight_bitboard[from] & b_knights)
			return true;
		func = slonyachafunk(from);
		if (func & b_bishops)
			return true;
		if (func & b_queens)
			return true;
		func = ladyachafunk(from);
		if (func & b_rooks)
			return true;
		if (func & b_queens)
			return true;
		if (BB.wpawn_capture_bitboard[from] & b_pawns) //eto ne shutka - wpawn!
			return true;
		if (BB.king_bitboard[from] & b_kings)
			return true;

		return false;
	}
	else //threat to black king
	{
		from = LastOne(b_kings);
		if (BB.knight_bitboard[from] & w_knights)
			return true;
		func = slonyachafunk(from);
		if (func & w_bishops)
			return true;
		if (func & w_queens)
			return true;
		func = ladyachafunk(from);
		if (func & w_rooks)
			return true;
		if (func & w_queens)
			return true;
		if (BB.bpawn_capture_bitboard[from] & w_pawns) //eto ne shutka - bpawn!
			return true;
		if (BB.king_bitboard[from] & w_kings)
			return true;

		return false;
	}
}

///////////////////////////////////////////////////////////////
//                square under atack                         //
///////////////////////////////////////////////////////////////

bool Board::isSQUnderAtack(char color, unsigned char pos)
{
	BITBOARD func;
	// white
	if (color == WHITE)
	{
		if (BB.knight_bitboard[pos] & b_knights)
			return true;
		func = slonyachafunk(pos);
		if (func & b_bishops)
			return true;
		if (func & b_queens)
			return true;
		func = ladyachafunk(pos);
		if (func & b_rooks)
			return true;
		if (func & b_queens)
			return true;
		if (BB.wpawn_capture_bitboard[pos] & b_pawns) //eto ne shutka - wpawn!
			return true;
		if (BB.king_bitboard[pos] & b_kings)
			return true;

		return false;
	}
	else
	{
		if (BB.knight_bitboard[pos] & w_knights)
			return true;
		func = slonyachafunk(pos);
		if (func & w_bishops)
			return true;
		if (func & w_queens)
			return true;
		func = ladyachafunk(pos);
		if (func & w_rooks)
			return true;
		if (func & w_queens)
			return true;
		if (BB.bpawn_capture_bitboard[pos] & w_pawns) //eto ne shutka - bpawn!
			return true;
		if (BB.king_bitboard[pos] & w_kings)
			return true;

		return false;
	}
}

////////////////////////////////////////////////////////////////
//                move functions                              //
////////////////////////////////////////////////////////////////

BITBOARD Board::slonyachafunk(unsigned char pos)
{
	BITBOARD fn;
	BITBOARD row;
	int stopbit;

	fn = (BITBOARD)0;

	if (BB.vectorm7[pos])
	{ //trying to optimize empty vectors
		fn = wb_figures & BB.vectorm7[pos];
		if (fn)
		{
			stopbit = FirstOne(fn);
			fn = BB.vectorm7[pos] ^ BB.vectorm7[63 - stopbit];
		}
		else
		{
			fn = BB.vectorm7[pos];
		}
	}

	if (BB.vectorm9[pos])
	{
		row = wb_figures & BB.vectorm9[pos];
		if (row)
		{
			stopbit = FirstOne(row);
			fn |= BB.vectorm9[pos] ^ BB.vectorm9[63 - stopbit];
		}
		else
		{
			fn |= BB.vectorm9[pos];
		}
	}

	if (BB.vectorp7[pos])
	{
		row = wb_figures & BB.vectorp7[pos];
		if (row)
		{
			stopbit = LastOne(row);
			fn |= BB.vectorp7[pos] ^ BB.vectorp7[stopbit];
		}
		else
		{
			fn |= BB.vectorp7[pos];
		}
	}

	if (BB.vectorp9[pos])
	{
		row = wb_figures & BB.vectorp9[pos];
		if (row)
		{
			stopbit = LastOne(row);
			fn |= BB.vectorp9[pos] ^ BB.vectorp9[stopbit];
		}
		else
		{
			fn |= BB.vectorp9[pos];
		}
	}
	return fn;
}

BITBOARD Board::ladyachafunk(unsigned char pos)
{
	BITBOARD fn;
	BITBOARD row;
	int stopbit;

	fn = wb_figures & BB.vectorm1[pos];
	if (fn)
	{
		stopbit = FirstOne(fn);
		fn = BB.vectorm1[pos] ^ BB.vectorm1[63 - stopbit];
	}
	else
	{
		fn = BB.vectorm1[pos];
	}

	row = wb_figures & BB.vectorm8[pos];
	if (row)
	{
		stopbit = FirstOne(row);
		fn |= BB.vectorm8[pos] ^ BB.vectorm8[63 - stopbit];
	}
	else
	{
		fn |= BB.vectorm8[pos];
	}

	row = wb_figures & BB.vectorp1[pos];
	if (row)
	{
		stopbit = LastOne(row);
		fn |= BB.vectorp1[pos] ^ BB.vectorp1[stopbit];
	}
	else
		fn |= BB.vectorp1[pos];

	row = wb_figures & BB.vectorp8[pos];
	if (row)
	{
		stopbit = LastOne(row);
		fn |= BB.vectorp8[pos] ^ BB.vectorp8[stopbit];
	}
	else
	{
		fn |= BB.vectorp8[pos];
	}

	return fn;
}

BITBOARD Board::RooksConnectedV(unsigned char pos)
{
	BITBOARD fn;
	BITBOARD vert_edges=0;
/*
	fn = wb_figures & BB.vectorm8[pos];
	if (fn)
	{
		vert_edges = BB.bitpos[FirstOne(fn)] ;
	}
*/
	//only lookup  to up direction  of first found rook

	fn = wb_figures & BB.vectorp8[pos];
	if (fn)
	{
		vert_edges = BB.bitpos[LastOne(fn)] ;
	}

	return vert_edges;
}

BITBOARD Board::kvinskafunk(unsigned char pos)
{
	return ladyachafunk(pos) | slonyachafunk(pos);
}

BITBOARD Board::pawnMoveFuncFrom2Line(unsigned char pos)
{
	BITBOARD func;
	func = BB.bitpos[pos + 8];
	if ((func & wb_figures) != 0)
		return func;
	return BB.wpawn_forward_bitboard[pos];
}

BITBOARD Board::pawnMoveFuncFrom7Line(unsigned char pos)
{
	BITBOARD func;
	func = BB.bitpos[pos - 8];
	if ((func & wb_figures) != 0)
		return func;
	return BB.bpawn_forward_bitboard[pos];
}

void Board::makeCaptureOrMove(int move, char color)
{
	//if ((move >> 15) & 7)
	if (BeatenFiguere(move))
		makeCapture(move, color);
	else
		makeMove(move, color);
}

void Board::rollBackCaptureOrMove(int move, char color)
{
	//if ((move >> 15) & 7)
	if (BeatenFiguere(move))
		rollBackCapture(move, color);
	else
		rollBackMove(move, color);
}

void Board::makeMove(int move, char color)
{
	BITBOARD from;
	BITBOARD to;
	int movingfigure;
	char promotion;
#ifdef WITHHASH
	unsigned char frompos, topos;
#endif
	//clearEnPassant(color);

	if (move == 1) //white O-O
	{
		wkingmoved++;
		wOOrokirovka = true;
		wb_figures ^= w_kings;
		wb_figures ^= w_rooks;
		w_figures ^= w_kings;
		w_figures ^= w_rooks;
		w_kings = G1;
		w_rooks ^= H1;
		w_rooks |= F1;
		wb_figures |= w_kings;
		wb_figures |= w_rooks;
		w_figures |= w_kings;
		w_figures |= w_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSE1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSG1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSH1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSF1];
#endif
		return;
	}
	if (move == 3)
	{
		bkingmoved++;
		bOOrokirovka = true;
		wb_figures ^= b_kings;
		wb_figures ^= b_rooks;
		b_figures ^= b_kings;
		b_figures ^= b_rooks;
		b_kings = G8; //black O-O
		b_rooks ^= H8;
		b_rooks |= F8;
		wb_figures |= b_kings;
		wb_figures |= b_rooks;
		b_figures |= b_kings;
		b_figures |= b_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSE8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSG8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSH8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSF8];
#endif
		return;
	}
	if (move == 2) //white O-O-O
	{
		wkingmoved++;
		wOOOrokirovka = true;
		wb_figures ^= w_kings;
		wb_figures ^= w_rooks;
		w_figures ^= w_kings;
		w_figures ^= w_rooks;
		w_kings = C1;
		w_rooks ^= A1;
		w_rooks |= D1;
		wb_figures |= w_kings;
		wb_figures |= w_rooks;
		w_figures |= w_kings;
		w_figures |= w_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSE1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSC1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSA1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSD1];
#endif
		return;
	}
	if (move == 4)
	{
		bkingmoved++;
		bOOOrokirovka = true;
		wb_figures ^= b_kings;
		wb_figures ^= b_rooks;
		b_figures ^= b_kings;
		b_figures ^= b_rooks;
		b_kings = C8; //black O-O-O
		b_rooks ^= A8;
		b_rooks |= D8;
		wb_figures |= b_kings;
		wb_figures |= b_rooks;
		b_figures |= b_kings;
		b_figures |= b_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSE8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSC8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSA8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSD8];
#endif
		return;
	}
#ifdef WITHHASH
	//frompos=move & 63;
	//topos=(move >> 6) & 63;
	frompos = FromPos(move);
	topos = ToPos(move);
#endif
	promotion = ((move >> 18) & 7);
	from = BB.bitpos[move & 63];
	to = BB.bitpos[(move >> 6) & 63];
	movingfigure = (move >> 12) & 7;

	if (color == WHITE)
	{
		wb_figures ^= from;
		w_figures ^= from;
		wb_figures |= to;
		w_figures |= to;

#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][topos];
		}
#endif

		switch (movingfigure)
		{
		case PAWN: //need to add different types of promoutions
			w_pawns ^= from;
			if ((from << 16) == to)
			{ //en passant
				w_enpassant = from << 8;
			}
			if (promotion)
			{ // why only queen? change.
				w_queens |= to;
				number_of_white_pawns--;
				number_of_white_queens++;
			}
			else
				w_pawns |= to;
			break;
		case ROOK:
			w_rooks ^= from;
			w_rooks |= to;
			break;
		case KNIGHT:
			w_knights ^= from;
			w_knights |= to;
			break;
		case BISHOP:
			w_bishops ^= from;
			w_bishops |= to;
			break;
		case KING:
			wkingmoved++;
			w_kings ^= from;
			w_kings |= to;
			break;
		case QUEEN:
			w_queens ^= from;
			w_queens |= to;
		}
	}
	else
	{
		wb_figures ^= from;
		b_figures ^= from;
		wb_figures |= to;
		b_figures |= to;

#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][topos];
		}
#endif

		switch (movingfigure)
		{
		case PAWN:
			b_pawns ^= from;
			if ((from >> 16) == to)
			{ //en passant
				b_enpassant = from >> 8;
			}
			if (promotion)
			{
				b_queens |= to;
				number_of_black_pawns--;
				number_of_black_queens++;
			}
			else
				b_pawns |= to;
			break;
		case ROOK:
			b_rooks ^= from;
			b_rooks |= to;
			break;
		case KNIGHT:
			b_knights ^= from;
			b_knights |= to;
			break;
		case BISHOP:
			b_bishops ^= from;
			b_bishops |= to;
			break;
		case KING:
			bkingmoved++;
			b_kings ^= from;
			b_kings |= to;
			break;
		case QUEEN:
			b_queens ^= from;
			b_queens |= to;
		}
	}
}

void Board::rollBackMove(int move, char color) //just reverse from-to in make move ^)
{
	BITBOARD from;
	BITBOARD to;
	int movingfigure;
	char promotion;
#ifdef WITHHASH
	unsigned char frompos, topos;
#endif
	if (move == 1)
	{ //white O-O
		wkingmoved--;
		wOOrokirovka = false;
		wb_figures ^= w_kings;
		wb_figures ^= w_rooks;
		w_figures ^= w_kings;
		w_figures ^= w_rooks;
		w_kings = E1;
		w_rooks ^= F1;
		w_rooks |= H1;
		wb_figures |= w_kings;
		wb_figures |= w_rooks;
		w_figures |= w_kings;
		w_figures |= w_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSE1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSG1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSH1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSF1];
#endif
		return;
	}
	if (move == 3)
	{
		bkingmoved--;
		bOOrokirovka = false;
		wb_figures ^= b_kings;
		wb_figures ^= b_rooks;
		b_figures ^= b_kings;
		b_figures ^= b_rooks;
		b_kings = E8; //black O-O
		b_rooks ^= F8;
		b_rooks |= H8;
		wb_figures |= b_kings;
		wb_figures |= b_rooks;
		b_figures |= b_kings;
		b_figures |= b_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSE8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSG8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSH8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSF8];
#endif
		return;
	}
	if (move == 2)
	{ //white O-O-O
		wkingmoved--;
		wOOOrokirovka = false;
		wb_figures ^= w_kings;
		wb_figures ^= w_rooks;
		w_figures ^= w_kings;
		w_figures ^= w_rooks;
		w_kings = E1;
		w_rooks ^= D1;
		w_rooks |= A1;
		wb_figures |= w_kings;
		wb_figures |= w_rooks;
		w_figures |= w_kings;
		w_figures |= w_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSE1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][KING][POSC1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSA1];
		z_hash = z_hash ^ HSH.zobrist[WHITE][ROOK][POSD1];
#endif
		return;
	}
	if (move == 4)
	{
		bkingmoved--;
		bOOOrokirovka = false;
		wb_figures ^= b_kings;
		wb_figures ^= b_rooks;
		b_figures ^= b_kings;
		b_figures ^= b_rooks;
		b_kings = E8; //black O-O-O
		b_rooks ^= D8;
		b_rooks |= A8;
		wb_figures |= b_kings;
		wb_figures |= b_rooks;
		b_figures |= b_kings;
		b_figures |= b_rooks;
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSE8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][KING][POSC8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSA8];
		z_hash = z_hash ^ HSH.zobrist[BLACK][ROOK][POSD8];
#endif
		return;
	}
#ifdef WITHHASH
	frompos = move & 63;
	topos = (move >> 6) & 63;
#endif
	promotion = ((move >> 18) & 7);
	to = BB.bitpos[move & 63]; // why from and to in wrong places? need to rewrite
	from = BB.bitpos[(move >> 6) & 63];
	movingfigure = (move >> 12) & 7;
	if (color == 1)
	{
		wb_figures ^= from;
		w_figures ^= from;
		wb_figures |= to;
		w_figures |= to;

#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][topos];
		}
#endif

		switch (movingfigure)
		{
		case PAWN:
			if (promotion)
			{
				w_queens ^= from;
				number_of_white_pawns++;
				number_of_white_queens--;
			}
			else
			{
				w_pawns ^= from;
			}
			if ((to << 16) == from)
			{ //en passant
				w_enpassant = 0;
			}
			w_pawns |= to;
			break;
		case ROOK:
			w_rooks ^= from;
			w_rooks |= to;
			break;
		case KNIGHT:
			w_knights ^= from;
			w_knights |= to;
			break;
		case BISHOP:
			w_bishops ^= from;
			w_bishops |= to;
			break;
		case KING:
			wkingmoved--;
			w_kings ^= from;
			w_kings |= to;
			break;
		case QUEEN:
			w_queens ^= from;
			w_queens |= to;
		}
	}
	else
	{
		wb_figures ^= from;
		b_figures ^= from;
		wb_figures |= to;
		b_figures |= to;

#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][topos];
		}
#endif

		switch (movingfigure)
		{
		case PAWN:
			if (promotion)
			{
				b_queens ^= from;
				number_of_black_pawns++;
				number_of_black_queens--;
			}
			else
			{
				b_pawns ^= from;
			}
			if ((to >> 16) == from)
			{ //en passant
				b_enpassant = 0;
			}
			b_pawns |= to;
			break;
		case ROOK:
			b_rooks ^= from;
			b_rooks |= to;
			break;
		case KNIGHT:
			b_knights ^= from;
			b_knights |= to;
			break;
		case BISHOP:
			b_bishops ^= from;
			b_bishops |= to;
			break;
		case KING:
			bkingmoved--;
			b_kings ^= from;
			b_kings |= to;
			break;
		case QUEEN:
			b_queens ^= from;
			b_queens |= to;
		}
	}
}

void Board::makeCapture(int move, char color)
{
	BITBOARD from;
	BITBOARD to;
	int movingfigure;
	int capturedfigure;
	char promotion;
	char enpassant;
	unsigned char frompos, topos;

	//clearEnPassant(color);

	enpassant = ((move >> 21) & 1);
	promotion = ((move >> 18) & 7);
	frompos = move & 63;
	topos = (move >> 6) & 63;
	from = BB.bitpos[frompos];
	to = BB.bitpos[topos];
	movingfigure = (move >> 12) & 7;
	capturedfigure = (move >> 15) & 7;

	number_of_total_figures--;

	if (color == WHITE)
	{
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][topos];
		}
		if (enpassant)
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][capturedfigure][topos - 8];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][capturedfigure][topos];
		}
#endif

		number_of_black_figures--;
		wb_figures ^= from;
		w_figures ^= from;
		wb_figures |= to;
		w_figures |= to;
		if (enpassant)
		{
			wb_figures ^= to >> 8;
			b_figures ^= to >> 8;
		}
		else
		{
			b_figures ^= to;
		}
		switch (movingfigure)
		{
		case PAWN:
			w_pawns ^= from;
			if (promotion)
			{
				w_queens |= to;
				number_of_white_pawns--;
				number_of_white_queens++;
			}
			else
				w_pawns |= to;
			break;
		case ROOK:
			w_rooks ^= from;
			w_rooks |= to;
			break;
		case KNIGHT:
			w_knights ^= from;
			w_knights |= to;
			break;
		case BISHOP:
			w_bishops ^= from;
			w_bishops |= to;
			break;
		case KING:
			wkingmoved++;
			w_kings ^= from;
			w_kings |= to;
			break;
		case QUEEN:
			w_queens ^= from;
			w_queens |= to;
		}
		switch (capturedfigure)
		{
		case PAWN:
			if (enpassant)
			{
				b_pawns ^= to >> 8;
			}
			else
			{
				b_pawns ^= to;
			}
			number_of_black_pawns--;
			break;
		case ROOK:
			b_rooks ^= to;
			number_of_black_rooks--;
			break;
		case KNIGHT:
			b_knights ^= to;
			number_of_black_knights--;
			break;
		case BISHOP:
			b_bishops ^= to;
			number_of_black_bishops--;
			break;
		case KING:
			b_kings ^= to;
			break;
		case QUEEN:
			b_queens ^= to;
			number_of_black_queens--;
		}
	}
	else
	{
#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][topos];
		}
		if (enpassant)
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][capturedfigure][topos + 8];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][capturedfigure][topos];
		}
#endif
		number_of_white_figures--;
		wb_figures ^= from;
		b_figures ^= from;
		wb_figures |= to;
		b_figures |= to;
		if (enpassant)
		{
			w_figures ^= to << 8;
			wb_figures ^= to << 8;
		}
		else
		{
			w_figures ^= to;
		}
		switch (movingfigure)
		{
		case PAWN:
			b_pawns ^= from;
			if (promotion)
			{
				b_queens |= to;
				number_of_black_pawns--;
				number_of_black_queens++;
			}
			else
				b_pawns |= to;
			break;
		case ROOK:
			b_rooks ^= from;
			b_rooks |= to;
			break;
		case KNIGHT:
			b_knights ^= from;
			b_knights |= to;
			break;
		case BISHOP:
			b_bishops ^= from;
			b_bishops |= to;
			break;
		case KING:
			bkingmoved++;
			b_kings ^= from;
			b_kings |= to;
			break;
		case QUEEN:
			b_queens ^= from;
			b_queens |= to;
		}
		switch (capturedfigure)
		{
		case PAWN:
			if (enpassant)
			{
				w_pawns ^= to << 8;
			}
			else
			{
				w_pawns ^= to;
			}
			number_of_white_pawns--;
			break;
		case ROOK:
			w_rooks ^= to;
			number_of_white_rooks--;
			break;
		case KNIGHT:
			w_knights ^= to;
			number_of_white_knights--;
			break;
		case BISHOP:
			w_bishops ^= to;
			number_of_white_bishops--;
			break;
		case KING:
			w_kings ^= to;
			break;
		case QUEEN:
			w_queens ^= to;
			number_of_white_queens--;
		}
	}
}

void Board::rollBackCapture(int move, char color)
{
	BITBOARD from;
	BITBOARD to;
	int movingfigure;
	int capturedfigure;
	char promotion;
	char enpassant;
#ifdef WITHHASH
	unsigned char frompos, topos;
	frompos = move & 63;
	topos = (move >> 6) & 63;

#endif
	enpassant = ((move >> 21) & 1);
	promotion = ((move >> 18) & 7);

	from = BB.bitpos[move & 63];
	to = BB.bitpos[(move >> 6) & 63];
	movingfigure = (move >> 12) & 7;
	capturedfigure = (move >> 15) & 7;

	number_of_total_figures++;
	if (color == WHITE)
	{

#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][movingfigure][topos];
		}
		if (enpassant)
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][capturedfigure][topos - 8];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][capturedfigure][topos];
		}
#endif

		number_of_black_figures++;
		w_figures ^= to;
		wb_figures |= from;
		w_figures |= from;
		if (enpassant)
		{
			b_figures |= to >> 8;
			wb_figures |= to >> 8;
			wb_figures ^= to;
		}
		else
		{
			b_figures |= to;
		}

		switch (movingfigure)
		{
		case PAWN:
			if (promotion)
			{
				w_queens ^= to;
				number_of_white_queens--;
				number_of_white_pawns++;
			}
			else
			{
				w_pawns ^= to;
			}
			w_pawns |= from;
			break;
		case ROOK:
			w_rooks ^= to;
			w_rooks |= from;
			break;
		case KNIGHT:
			w_knights ^= to;
			w_knights |= from;
			break;
		case BISHOP:
			w_bishops ^= to;
			w_bishops |= from;
			break;
		case KING:
			wkingmoved--;
			w_kings ^= to;
			w_kings |= from;
			break;
		case QUEEN:
			w_queens ^= to;
			w_queens |= from;
		}
		switch (capturedfigure)
		{
		case PAWN:
			if (enpassant)
			{
				b_pawns |= to >> 8;
			}
			else
			{
				b_pawns |= to;
			}
			number_of_black_pawns++;
			break;
		case ROOK:
			b_rooks |= to;
			number_of_black_rooks++;
			break;
		case KNIGHT:
			b_knights |= to;
			number_of_black_knights++;
			break;
		case BISHOP:
			b_bishops |= to;
			number_of_black_bishops++;
			break;
		case KING:
			b_kings |= to;
			break;
		case QUEEN:
			b_queens |= to;
			number_of_black_queens++;
		}
	}
	else
	{

#ifdef WITHHASH
		z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][frompos];
		if (promotion)
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][QUEEN][topos];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[BLACK][movingfigure][topos];
		}

		if (enpassant)
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][capturedfigure][topos + 8];
		}
		else
		{
			z_hash = z_hash ^ HSH.zobrist[WHITE][capturedfigure][topos];
		}
#endif

		number_of_white_figures++;
		b_figures |= from;
		wb_figures |= from;
		b_figures ^= to;
		if (enpassant)
		{
			w_figures |= to << 8;
			wb_figures |= to << 8;
			wb_figures ^= to;
		}
		else
		{
			w_figures |= to;
		}
		switch (movingfigure)
		{
		case PAWN:
			if (promotion)
			{
				b_queens ^= to;
				number_of_black_queens--;
				number_of_black_pawns++;
			}
			else
			{
				b_pawns ^= to;
			}
			b_pawns |= from;
			break;
		case ROOK:
			b_rooks ^= to;
			b_rooks |= from;
			break;
		case KNIGHT:
			b_knights ^= to;
			b_knights |= from;
			break;
		case BISHOP:
			b_bishops ^= to;
			b_bishops |= from;
			break;
		case KING:
			bkingmoved--;
			b_kings ^= to;
			b_kings |= from;
			break;
		case QUEEN:
			b_queens ^= to;
			b_queens |= from;
		}
		switch (capturedfigure)
		{
		case PAWN:
			if (enpassant)
			{
				w_pawns |= to << 8;
			}
			else
			{
				w_pawns |= to;
			}
			number_of_white_pawns++;
			break;
		case ROOK:
			w_rooks |= to;
			number_of_white_rooks++;
			break;
		case KNIGHT:
			w_knights |= to;
			number_of_white_knights++;
			break;
		case BISHOP:
			w_bishops |= to;
			number_of_white_bishops++;
			break;
		case KING:
			w_kings |= to;
			break;
		case QUEEN:
			w_queens |= to;
			number_of_white_queens++;
		}
	}
}

short Board::getQuickBoardScore(char color)
{
	short score;
	int mgc = number_of_total_figures;
	int egc = 34 - number_of_total_figures;

	//    PAWN_MG 100
	//   PAWN_EG 140

	//    KNIGHT_MG 350
	//    KNIGHT_EG 370

	//    BISHOP_MG 410
	//    BISHOP_EG 440

	//    ROOK_MG 630
	//    ROOK_EG 6800

	//    QUEEN_MG 1050
	//    QUEEN_MG 1150

	score = ((PAWN_MG * mgc + PAWN_EG * egc) / 34) * (number_of_white_pawns - number_of_black_pawns) +
			((KNIGHT_MG * mgc + KNIGHT_EG * egc) / 34) * (number_of_white_knights - number_of_black_knights) +
			((BISHOP_MG * mgc + BISHOP_EG * egc) / 34) * (number_of_white_bishops - number_of_black_bishops) +
			((ROOK_MG * mgc + ROOK_EG * egc) / 34) * (number_of_white_rooks - number_of_black_rooks) +
			((QUEEN_MG * mgc + QUEEN_EG * egc) / 34) * (number_of_white_queens - number_of_black_queens);

	if (color == BLACK)
		score = -score;
	return score;
}

short Board::getFullBoardScore(char color, int alpha, int beta)
{
	short score = getQuickBoardScore(color);
	MoveList ml, oppml;

	ml.init(color);
	oppml.init(3 - color);

#ifdef LAZYCUTOFF

	extern int lazytest;
	/*if(score>beta)
     return score;
     if(score+100 <alpha)
     return score;*/
	if ((score - LAZYCUTOFFSCORE - lazytest) > beta) /// what the heck? why so many cutoff when /8
		return score;
	if ((score + LAZYCUTOFFSCORE + lazytest) < alpha)
		return score;
#endif

	//    short mgc=number_of_total_figures;
	//    short egc=34-number_of_total_figures;

	score += getPawnsBonus(color) - getPawnsBonus(3 - color);
	score += getKnightsBonus(color) - getKnightsBonus(3 - color);
	score += getBishopsBonus(color) - getBishopsBonus(3 - color);
	score += getRooksBonus(color) - getRooksBonus(3 - color);
	score += getQueenBonus(color) - getQueenBonus(3 - color);
	score += getKingBonus(color) - getKingBonus(3 - color);

#ifdef LAZYCUTOFF
	if ((score - LAZYCUTOFFSCORE - lazytest) > beta)
		return score;
	if ((score + LAZYCUTOFFSCORE + lazytest) < alpha)
		return score;
#endif

#ifdef MOBILITYBONUS
	////generatePLMoveCount(&oppml, 3 - color); //shortened. moves, captures. no validity check
	////generatePLMoveCount(&ml, color);

	//generatePLPLMoveCount(&oppml, 3 - color); //only moves. no validity check
	//generatePLPLMoveCount(&ml, color);
	//score+=getMobilityBonus(&ml)-getMobilityBonus(&oppml);
	score += getMobilityBonus2(color) - getMobilityBonus2(3 - color); //unverified moves and captures
#endif

	return score;
}

short Board::getPawnsBonus(char color)
{
	short score = 0;
	unsigned char pawnpos;
	BITBOARD ourfigures;

	//int mgc=34;
	int mgc = number_of_total_figures;
	//	short egc=34-number_of_total_figures;

#ifdef HEAVY_PAWN
	int line;
	int row;
#endif

	if (color == WHITE)
	{
		ourfigures = w_pawns & W_PAWN_EVAL_SQS;
		while (ourfigures)
		{
			pawnpos = LastOne(ourfigures);
			//position board
			score += wpawnbonus[pawnpos];

#ifdef HEAVY_PAWN
			//			BITBOARD bbpos;

			line = pawnpos & 7;
			row = pawnpos / 8;
			//			bbpos=BB.bitpos[pawnpos];
			//isolated white pawns
			if (line == 0)
				if (!(BB.bbline[1] & w_pawns))
					score += PAWN_ISOLATED;
			if (line == 7)
				if (!(BB.bbline[6] & w_pawns))
					score += PAWN_ISOLATED;
			if ((line > 0) && (line < 7))
			{
				if (!(BB.bbline[line - 1] & w_pawns))
					score += PAWN_ISOLATED / 2;
				if (!(BB.bbline[line + 1] & w_pawns))
					score += PAWN_ISOLATED / 2;
			}
			//backward white pawns
			//quick and dirty backward
			//if(!((w_pawns &(bbpos>>9))|(w_pawns &(bbpos>>7))))
			//	score+=PAWN_BACKWARDS;
			if (!(w_pawns & BB.bpawn_capture_bitboard[pawnpos]))
				score += PAWN_BACKWARDS;
#endif
#ifdef HEAVY_PASSED_PAWN
			//passed pawns
			line = pawnpos & 7;
			row = pawnpos / 8;
			if (!(BB.wpawn_half_passed[pawnpos] & b_pawns))
			{
				score += PAWN_PASSED_FREE_MAX / ((7 - row) * 2); // hope none of white pawns ever appear on 7th row
				//if(!(BB.vectorp8[pawnpos] & b_figures))
				//	score+=PAWN_PASSED_FREE_MAX/((7-row)*4);
				if (!(BB.vectorp8[pawnpos] & wb_figures))
					score += PAWN_PASSED_FREE_MAX / ((7 - row) * 2);
				if (!isSQUnderAtack(WHITE, pawnpos + 8))
				{ //if pawn can move one step forward
					score += PAWN_PASSED_FREE_MAX / (7 - row);
				}
				//connected pawns
				//if(0)
				if(BB.wpawn_connected_bitboard[pawnpos]&w_pawns)
				{
					score += w_pawn_connected[pawnpos];
				}
			}
#endif

			Clear(pawnpos, &ourfigures);
		}

		// avoid double pawns
		score += PopCnt(w_pawns & (w_pawns >> 8)) * DOUBLE_PAWN;
		//substituted by king_shield

//no holes in the castle. if no rokirovka at all or white short
#ifndef TEST_KING_SHIELD
		if (wOOrokirovka)
			if (w_kings & F1G1H1)
				if (!(((w_pawns | w_bishops) & F2G2H2) == F2G2H2))
					score += (HOLE_IN_CASTLE * mgc) / 34;

		if (wOOOrokirovka)
			if (w_kings & A1B1C1D1)
				if (!(((w_pawns | w_bishops) & A2B2C2) == A2B2C2))
					score += (HOLE_IN_CASTLE * mgc) / 34;
#endif

#ifdef TEST_KING_SHIELD
		if (wOOrokirovka) // do not only move king but make castling
			if (w_kings & F1G1H1)
			{
				score += ((PopCnt((w_pawns | w_bishops) & F2G2H2) * KING_SHIELD_2LINE) * mgc) / 34;
				score += ((PopCnt((w_pawns | w_bishops) & F3G3H3) * KING_SHIELD_3LINE) * mgc) / 34;
			}
		if (wOOOrokirovka)
			if (w_kings & A1B1C1)
			{
				score += ((PopCnt((w_pawns | w_bishops) & A2B2C2) * KING_SHIELD_2LINE) * mgc) / 34;
				score += ((PopCnt((w_pawns | w_bishops) & A3B3C3) * KING_SHIELD_3LINE) * mgc) / 34;
			}
#endif
	}
	if (color == BLACK)
	{
		ourfigures = b_pawns & B_PAWN_EVAL_SQS;
		while (ourfigures)
		{
			pawnpos = LastOne(ourfigures);
			// position board
			score += bpawnbonus[pawnpos];

#ifdef HEAVY_PAWN
			//			BITBOARD bbpos;
			//			bbpos=BB.bitpos[pawnpos];
			line = pawnpos & 7;
			row = pawnpos / 8;

			//isolated black pawns
			if (line == 0)
				if (!(BB.bbline[1] & b_pawns))
					score += PAWN_ISOLATED;
			if (line == 7)
				if (!(BB.bbline[6] & b_pawns))
					score += PAWN_ISOLATED;
			if ((line > 0) && (line < 7))
			{
				if (!(BB.bbline[line - 1] & b_pawns))
					score += PAWN_ISOLATED / 2;
				if (!(BB.bbline[line + 1] & b_pawns))
					score += PAWN_ISOLATED / 2;
			}
			//backward black pawns
			//quick and dirty
			//if(!((b_pawns &(bbpos<<9))|(b_pawns &(bbpos<<7))))
			//		score+=PAWN_BACKWARDS;
			if (!(b_pawns & BB.wpawn_capture_bitboard[pawnpos]))
				score += PAWN_BACKWARDS;

//dirty - need to check only oposite pawns in front
#endif
#ifdef HEAVY_PASSED_PAWN
			//passed pawns
			line = pawnpos & 7;
			row = pawnpos / 8;
			if (!(BB.bpawn_half_passed[pawnpos] & w_pawns))
			{
				score += PAWN_PASSED_FREE_MAX / ((row)*2);
				if (!(BB.vectorm8[pawnpos] & wb_figures))
					score += PAWN_PASSED_FREE_MAX / ((row)*2);
				if (!isSQUnderAtack(BLACK, pawnpos - 8))
				{ //if pawn can move one step forward
					score += PAWN_PASSED_FREE_MAX / (row);
				}
				//connected pawns
				//if(0)
				if(BB.bpawn_connected_bitboard[pawnpos]&b_pawns)
				{
					score += b_pawn_connected[pawnpos];
				}
			}
#endif

			Clear(pawnpos, &ourfigures);
		}

		// avoid double pawns
		score += PopCnt(b_pawns & (b_pawns >> 8)) * DOUBLE_PAWN;
		// - substituted by pawn shield
#ifndef TEST_KING_SHIELD
		//no holes in the castle
		if (bOOrokirovka)
			if (b_kings & F8G8H8)
				if (!(((b_pawns | b_bishops) & F7G7H7) == F7G7H7))
					score += (HOLE_IN_CASTLE * mgc) / 34;

		if (bOOOrokirovka)
			if (b_kings & A8B8C8D8)
				if (!(((b_pawns | b_bishops) & A7B7C7) == A7B7C7))
					score += (HOLE_IN_CASTLE * mgc) / 34;
#endif
#ifdef TEST_KING_SHIELD
		if (bOOrokirovka)
			if (b_kings & F8G8H8)
			{
				score += ((PopCnt((b_pawns | b_bishops) & F7G7H7) * KING_SHIELD_2LINE) * mgc) / 34;
				score += ((PopCnt((b_pawns | b_bishops) & F6G6H6) * KING_SHIELD_3LINE) * mgc) / 34;
			}
		if (bOOOrokirovka)
			if (b_kings & A8B8C8)
			{
				score += ((PopCnt((b_pawns | b_bishops) & A7B7C7) * KING_SHIELD_2LINE) * mgc) / 34;
				score += ((PopCnt((b_pawns | b_bishops) & A6B6C6) * KING_SHIELD_3LINE) * mgc) / 34;
			}
#endif
	}

	return score;
}

short Board::getKingBonus(char color)
{
	short score = 0;
	unsigned char kingpos;

	int mgc = number_of_total_figures;
	int egc = 34 - number_of_total_figures;

	//int mgc=34;
	//int egc=0;

	// king  likes to be in the castle
	if (color == WHITE)
	{
		kingpos = LastOne(w_kings);
		score += (kinstartgbonus[kingpos] * mgc + kingendbonus[kingpos] * egc) / 34;
		// king in castle - substituted by pawn king shield
#ifndef TEST_KING_SHIELD
		if (wOOrokirovka)
			if (w_kings & F1G1H1)
				score += (KING_IN_CASTLE * mgc) / 34;

		if (wOOOrokirovka)
			if (w_kings & A1B1C1D1)
				score += (KING_IN_CASTLE * mgc) / 34;
#endif
		// penalty for moving king and rooks without castling
		if (!(wOOrokirovka || wOOOrokirovka))
			if (wkingmoved)
				if (b_queens)
					score += (KING_WANDERS * mgc) / 34;
#ifdef HEAVY_TROPISM1
		//tropism

		for (int dist = 1; dist < 5; dist++)
		{
			// threat to our king
			if (BB.distance_sq[kingpos][dist] & b_queens)
			{
				score += (mgc * QUEEN_THROPISM_MG / dist + egc * QUEEN_THROPISM_EG / dist) / 34;
			}
			if (BB.distance_sq[kingpos][dist] & b_rooks)
			{
				score += (mgc * ROOK_THROPISM_MG / dist + egc * ROOK_THROPISM_EG / dist) / 34;
			}
			if (BB.distance_sq[kingpos][dist] & b_knights)
			{
				score += (mgc * KNIGHT_THROPISM_MG / dist + egc * KNIGHT_THROPISM_EG / dist) / 34;
			}
		}
#endif
#ifdef HEAVY_TROPISM2
		//tropism v2
		int trpsm = 0;
		for (int dist = 1; dist < 5; dist++)
		{
			// threat to our king
			if (BB.distance_sq[kingpos][dist] & b_queens)
			{
				trpsm += king_tropism_queen[dist];
			}
			if (BB.distance_sq[kingpos][dist] & b_rooks)
			{
				trpsm += king_tropism_rook[dist];
			}
			if (BB.distance_sq[kingpos][dist] & b_knights)
			{
				trpsm += king_tropism_knight[dist];
			}
			if (BB.distance_sq[kingpos][dist] & b_bishops)
			{
				trpsm += king_tropism_bishop[dist];
			}
		}
		trpsm = max(15, trpsm);
		score += tropism_vector[trpsm];
#endif
	}
	if (color == BLACK)
	{
		kingpos = LastOne(b_kings);
		score += (kinstartgbonus[kingpos] * mgc + kingendbonus[kingpos] * egc) / 34;
// king in castle - substituted by pawn king shield
#ifndef TEST_KING_SHIELD
		if (bOOrokirovka)
			if (b_kings & F8G8H8)
				score += (KING_IN_CASTLE * mgc) / 34;

		if (bOOOrokirovka)
			if (b_kings & A8B8C8D8)
				score += (KING_IN_CASTLE * mgc) / 34;
#endif
		// penalty for moving king and rooks without castling
		if (!(bOOrokirovka || bOOOrokirovka))
			if (bkingmoved)
				if (w_queens)
					score += (KING_WANDERS * mgc) / 34;
#ifdef HEAVY_TROPISM1
		//tropism threat to our king

		for (int dist = 1; dist < 5; dist++)
		{
			if (BB.distance_sq[kingpos][dist] & w_queens)
			{
				score += (mgc * QUEEN_THROPISM_MG / dist + egc * QUEEN_THROPISM_EG / dist) / 34;
			}
			if (BB.distance_sq[kingpos][dist] & w_rooks)
			{
				score += (mgc * ROOK_THROPISM_MG / dist + egc * ROOK_THROPISM_EG / dist) / 34;
			}
			if (BB.distance_sq[kingpos][dist] & w_knights)
			{
				score += (mgc * KNIGHT_THROPISM_MG / dist + egc * KNIGHT_THROPISM_EG / dist) / 34;
			}
		}
#endif
#ifdef HEAVY_TROPISM2
		//tropism v2
		int trpsm = 0;
		for (int dist = 1; dist < 5; dist++)
		{
			// threat to our king
			if (BB.distance_sq[kingpos][dist] & w_queens)
			{
				trpsm += king_tropism_queen[dist];
			}
			if (BB.distance_sq[kingpos][dist] & w_rooks)
			{
				trpsm += king_tropism_rook[dist];
			}
			if (BB.distance_sq[kingpos][dist] & w_knights)
			{
				trpsm += king_tropism_knight[dist];
			}
			if (BB.distance_sq[kingpos][dist] & w_bishops)
			{
				trpsm += king_tropism_bishop[dist];
			}
		}
		trpsm = max(15, trpsm);
		score += tropism_vector[trpsm];
#endif
	}

	return score;
}

short Board::getQueenBonus(char color)
{
	short score = 0;
	// queen should not move until king in the castle
	if (color == WHITE)
	{
		if (number_of_total_figures < 26)
			return 0;
		if (!w_queens)
			return 0;
		if (!(w_queens & D1))
			if (!(wOOrokirovka || wOOOrokirovka))
				score += EARLY_QUEEN;
	}
	if (color == BLACK)
	{
		if (number_of_total_figures < 26)
			return 0;
		if (!b_queens)
			return 0;
		if (!(b_queens & D8))
			if (!(bOOrokirovka || bOOOrokirovka))
				score += EARLY_QUEEN;
	}

	return score;
}

short Board::getKnightsBonus(char color)
{
	short score = 0;
	unsigned char knightpos;
	BITBOARD ourfigures;
	if (color == WHITE)
	{
		ourfigures = w_knights;
		while (ourfigures)
		{
			knightpos = LastOne(ourfigures);
			score += knightbonus[knightpos];
#ifdef HEAVY_KNIGHTS
			// knight outposts
			unsigned char supporters;
			int line = knightpos & 7;
			int row = knightpos / 8;
			if (row > 1)
				if (row < 6)
					if (line > 0)
						if (line < 7)
						{
							supporters = PopCnt((BB.bitpos[knightpos - 9] | BB.bitpos[knightpos - 7]) & w_pawns);
							if (supporters)
							{
								score += wknightoutpost[knightpos] * supporters;
								if (!((BB.vectorp8[knightpos - 1] | BB.vectorp8[knightpos + 1]) & b_pawns))
									score += wknightoutpost[knightpos] * supporters / 2;
							}
						}
			//knight trapped positions
			if (knightpos == POSH8)
			{
				if (b_pawns & (F7 | H7))
					score += KNIGHT_TRAPPED_H8;
			}
			if (knightpos == POSA8)
			{
				if (b_pawns & (A7 | C7))
					score += KNIGHT_TRAPPED_H8;
			}
			if (knightpos == POSH7)
			{
				if ((b_pawns & G7) && (b_pawns & (H6 | F6)))
					score += KNIGHT_TRAPPED_H7;
			}
			if (knightpos == POSA7)
			{
				if ((b_pawns & B7) && (b_pawns & (A6 | C6)))
					score += KNIGHT_TRAPPED_H7;
			}

#endif

			Clear(knightpos, &ourfigures);
		}
	}
	if (color == BLACK)
	{
		ourfigures = b_knights;
		while (ourfigures)
		{
			knightpos = LastOne(ourfigures);
			score += knightbonus[knightpos];
#ifdef HEAVY_KNIGHTS
			// knight outposts
			unsigned char supporters;
			int line = knightpos & 7;
			int row = knightpos / 8;
			if (row < 5)
				if (row > 1)
					if (line > 0)
						if (line < 7)
						{
							supporters = PopCnt((BB.bitpos[knightpos + 9] | BB.bitpos[knightpos + 7]) & b_pawns);
							if (supporters)
							{
								score += bknightoutpost[knightpos] * supporters;
								if (!((BB.vectorm8[knightpos - 1] | BB.vectorm8[knightpos + 1]) & w_pawns))
									score += bknightoutpost[knightpos] * supporters / 2;
							}
						}
			//knight trapped positions
			if (knightpos == POSH1)
			{
				if (w_pawns & (F2 | H2))
					score += KNIGHT_TRAPPED_H8;
			}
			if (knightpos == POSA1)
			{
				if (w_pawns & (A2 | A7))
					score += KNIGHT_TRAPPED_H8;
			}
			if (knightpos == POSH2)
			{
				if ((w_pawns & G2) && (w_pawns & (H3 | F3)))
					score += KNIGHT_TRAPPED_H7;
			}
			if (knightpos == POSA2)
			{
				if ((w_pawns & B2) && (w_pawns & (A3 | C3)))
					score += KNIGHT_TRAPPED_H7;
			}
#endif
			Clear(knightpos, &ourfigures);
		}
	}

	return score;
}

short Board::getRooksBonus(char color)
{
	short score = 0;
	BITBOARD ourfigures;

#ifdef HEAVY_EVAL
	unsigned char rookpos;
	short connect_checked=0;
#endif

	if (color == WHITE)
	{
		ourfigures = w_rooks;
		short rooksOn7 = PopCnt(ourfigures & A7H7);
		if (rooksOn7)
			if ((b_kings & A8H8) || (b_pawns & A7H7))
				score += ROOK_7HT * rooksOn7;
#ifdef HEAVY_EVAL
		while (ourfigures)
		{
			rookpos = LastOne(ourfigures);
			if (!(BB.vectorp8[rookpos] & w_pawns))
			{
				score += ROOK_OPEN / 2;
				if (!(BB.vectorp8[rookpos] & b_figures))
					score += ROOK_OPEN / 2;
				//if(0)
				if(!connect_checked)
				{	
					connect_checked++;
					if(RooksConnectedV(rookpos)  & w_rooks)
					{
						score += ROOK_CONNECTED;
					}
				}
			}
			Clear(rookpos, &ourfigures);
		}
#endif
	}
	if (color == BLACK)
	{
		ourfigures = b_rooks;
		short rooksOn7 = PopCnt(ourfigures & A2H2);
		if (rooksOn7)
			if ((w_kings & A1H1) || (w_pawns & A2H2))
				score += ROOK_7HT * rooksOn7;
#ifdef HEAVY_EVAL
		while (ourfigures)
		{
			rookpos = LastOne(ourfigures);
			if (!(BB.vectorm8[rookpos] & b_pawns))
			{
				score += ROOK_OPEN / 2;
				if (!(BB.vectorm8[rookpos] & w_figures))
					score += ROOK_OPEN / 2;
				//if(0)
				if(!connect_checked)
				{	
					connect_checked++;
					if(RooksConnectedV(rookpos)  & b_rooks){
						score += ROOK_CONNECTED;
					}
				}
			}
			Clear(rookpos, &ourfigures);
		}
#endif
	}

	return score;
}

short Board::getBishopsBonus(char color)
{
	short score = 0;
	unsigned char bishoppos;
	BITBOARD ourfigures;

	int mgc = number_of_total_figures;
	int egc = 34 - number_of_total_figures;

	if (color == WHITE)
	{

		if (number_of_white_bishops > 1)
		{
			score += (mgc * BISHOP_PAIR_MG + egc * BISHOP_PAIR_EG) / 34;
		}

		ourfigures = w_bishops;
		while (ourfigures)
		{
			bishoppos = LastOne(ourfigures);
			score += bishopbonus[bishoppos];
#ifdef HEAVY_BISHOPS
			// bishops outposts
			//+ score
			//if no pawns + score* supporters /2
			//		if no knights and same color bishops ++score
			int line = bishoppos & 7;
			int row = bishoppos / 8;
			unsigned char supporters;

			if (row > 1)
				if (row < 6)
					if (line > 0)
						if (line < 7)
						{
							supporters = PopCnt((BB.bitpos[bishoppos - 9] | BB.bitpos[bishoppos - 7]) & w_pawns);
							if (supporters)
							{
								score += wbishopoutpost[bishoppos] * supporters;
								if (!((BB.vectorp8[bishoppos - 1] | BB.vectorp8[bishoppos + 1]) & b_pawns))
								{
									score += wbishopoutpost[bishoppos] * supporters / 2;
									if (!b_knights)
									{
										if (positionToBITBoard(bishoppos) & WHITE_SQS)
										{ //white sqs bishop
											if (!(b_bishops & WHITE_SQS))
												score += wbishopoutpost[bishoppos] * supporters;
										}
										else
										{ //black sqs bishop
											if (!(b_bishops & BLACK_SQS))
												score += wbishopoutpost[bishoppos] * supporters;
										}
									}
								}
							}
						}

			//bishop friendly pawn with same color blocking
			if (number_of_white_bishops == 1)
			{
				if (w_bishops & WHITE_SQS)
				{
					score += PopCnt(w_pawns & WHITE_SQS) * LONE_BISHOP_PAWN_SAME_COLOR;
				}
				else
				{
					score += PopCnt(w_pawns & BLACK_SQS) * LONE_BISHOP_PAWN_SAME_COLOR;
				}
			}
			//bishop trapped positions
			if (bishoppos == POSH7)
			{
				if (b_pawns & (F7 | G6))
					score += BISHOP_TRAPPED_H7;
			}
			if (bishoppos == POSA7)
			{
				if (b_pawns & (C7 | B6))
					score += BISHOP_TRAPPED_H7;
			}
			if (bishoppos == POSH6)
			{
				if (b_pawns & (F6 | G5))
					score += BISHOP_TRAPPED_H6;
			}
			if (bishoppos == POSA6)
			{
				if (b_pawns & (C6 | B7))
					score += BISHOP_TRAPPED_H6;
			}

#endif
			Clear(bishoppos, &ourfigures);
		}
	}
	if (color == BLACK)
	{

		if (number_of_black_bishops > 1)
		{
			score += (mgc * BISHOP_PAIR_MG + egc * BISHOP_PAIR_EG) / 34;
		}
		ourfigures = b_bishops;
		while (ourfigures)
		{
			bishoppos = LastOne(ourfigures);
			score += bishopbonus[bishoppos];
#ifdef HEAVY_BISHOPS
			// bishops outposts
			int line = bishoppos & 7;
			int row = bishoppos / 8;
			unsigned char supporters;

			if (row > 1)
				if (row < 6)
					if (line > 0)
						if (line < 7)
						{
							supporters = PopCnt((BB.bitpos[bishoppos + 9] | BB.bitpos[bishoppos + 7]) & b_pawns);
							if (supporters)
							{
								score += bbishopoutpost[bishoppos] * supporters;
								if (!((BB.vectorm8[bishoppos - 1] | BB.vectorm8[bishoppos + 1]) & w_pawns))
								{
									score += bbishopoutpost[bishoppos] * supporters / 2;
									if (!w_knights)
									{
										if (positionToBITBoard(bishoppos) & WHITE_SQS)
										{ //white sqs bishop
											if (!(w_bishops & WHITE_SQS))
												score += bbishopoutpost[bishoppos] * supporters;
										}
										else
										{ //black sqs bishop
											if (!(w_bishops & BLACK_SQS))
												score += bbishopoutpost[bishoppos] * supporters;
										}
									}
								}
							}
						}

			//bishop friendly pawn with same color blocking
			if (number_of_black_bishops == 1)
			{
				if (b_bishops & WHITE_SQS)
				{
					score += PopCnt(b_pawns & WHITE_SQS) * LONE_BISHOP_PAWN_SAME_COLOR;
				}
				else
				{
					score += PopCnt(b_pawns & BLACK_SQS) * LONE_BISHOP_PAWN_SAME_COLOR;
				}
			}
			//bishop trapped positions
			if (bishoppos == POSH2)
			{
				if (w_pawns & (F2 | G3))
					score += BISHOP_TRAPPED_H7;
			}
			if (bishoppos == POSA2)
			{
				if (w_pawns & (C2 | B3))
					score += BISHOP_TRAPPED_H7;
			}
			if (bishoppos == POSH3)
			{
				if (w_pawns & (F3 | G4))
					score += BISHOP_TRAPPED_H6;
			}
			if (bishoppos == POSA3)
			{
				if (w_pawns & (C3 | B4))
					score += BISHOP_TRAPPED_H6;
			}

#endif

			Clear(bishoppos, &ourfigures);
		}
	}

	return score;
}

bool Board::badQuickMatCheck(char color)
{
	if (color == WHITE)
	{
		if (!w_kings)
			return true;
		else
			return false;
	}
	else if (!b_kings)
		return true;
	return false;
}

char Board::loadFEN(const char *FEN)
{
	//returns color
	//[FEN "6rk/1p1br2p/pqp5/3pNP2/1P1Pp3/P5PR/5PKR/Q7 w - - 0 1"]
	initClear();

	int n = 0;
	char chr;
	char figure;
	char pole[2];
	for (char y = '8'; y > '0'; y--)
		for (char x = 'a'; x < 'i'; x++)
		{
			chr = FEN[n++];
			if ((chr > '0') && (chr < '9'))
				x = x + (chr - '0' - 1);
			if (((chr > 'a') && (chr < 'z')) || ((chr > 'A') && (chr < 'Z')))
			{
				figure = chr;
				pole[0] = x;
				pole[1] = y;
				setFigure(figure, pole);
			}
			if (chr == '/')
				x--;
		}
	n++;
	switch (FEN[n])
	{
	case 'b':
		return BLACK;
	case 'w':
		return WHITE;
	}
	return WHITE;
}

int Board::mnemToMove(const char *mnem) // need to add promoutions parsing
{
	char from, to;
	BITBOARD bbFrom;
	int FromTo, move;
	MoveList ml;

	int rokirovka = 0;

	if (!memcmp(mnem, "e1g1", 4) && (w_kings == E1) && !wkingmoved) //add check for rook on position -later
		rokirovka = 1;
	if (!memcmp(mnem, "e8g8", 4) && (b_kings == E8) && !bkingmoved)
		rokirovka = 3;
	if (!memcmp(mnem, "e1c1", 4) && (w_kings == E1) && !wkingmoved)
		rokirovka = 2;
	if (!memcmp(mnem, "e8c8", 4) && (b_kings == E8) && !bkingmoved)
		rokirovka = 4;

	if ((rokirovka == 1) || (rokirovka == 2))
	{
		ml.init(WHITE);
		generateMoveList(&ml, WHITE, 0, 0, 127);
		while (ml.ableTomac())
		{
			if (rokirovka == ml.getNextmac())
				return rokirovka;
		}
		return 0;
	}
	if ((rokirovka == 3) || (rokirovka == 4))
	{
		ml.init(BLACK);
		generateMoveList(&ml, BLACK, 0, 0, 127);
		while (ml.ableTomac())
		{
			if (rokirovka == ml.getNextmac())
				return rokirovka;
		}
		return 0;
	}

	from = poleToPosition(mnem);
	to = poleToPosition(&mnem[2]);

	if ((from > 63) || (from < 0))
		return 0;
	if ((to > 63) || (to < 0))
		return 0;

	bbFrom = positionToBITBoard(from);

	FromTo = from | (to << 6);
	if (bbFrom & w_figures) // white figure
	{
		ml.init(1);
		generateMoveList(&ml, WHITE, 0, 0, 127);
		while (ml.ableTomac())
		{
			move = ml.getNextmac();
			if ((move & 4095) == FromTo)
				return move;
		}
		return 0;
	}
	if (bbFrom & b_figures) // black figure
	{
		ml.init(2);
		generateMoveList(&ml, BLACK, 0, 0, 127);
		while (ml.ableTomac())
		{
			move = ml.getNextmac();
			if ((move & 4095) == FromTo)
				return move;
		}
		return 0;
	}

	return 0;
}

int Board::halfmnemToMove(string mnem, char color) // need to add promoutions parsing
{
	char to;
	int move, halfmove;
	int line = -1;
	MoveList ml;

	int rokirovka = 0;

	if (color == WHITE)
	{
		if ((mnem == "O-O") && (w_kings == E1) && !wkingmoved) //add check for rook on position -later
			rokirovka = 1;
		if ((mnem == "O-O-O") && (w_kings == E1) && !wkingmoved)
			rokirovka = 2;
	}
	else
	{
		if ((mnem == "O-O") && (b_kings == E8) && !bkingmoved)
			rokirovka = 3;
		if ((mnem == "O-O-O") && (b_kings == E8) && !bkingmoved)
			rokirovka = 4;
	}

	if ((mnem == "e1g1") && (w_kings == E1) && !wkingmoved) //add check for rook on position -later
		rokirovka = 1;
	if ((mnem == "e8g8") && (b_kings == E8) && !bkingmoved)
		rokirovka = 3;
	if ((mnem == "e1c1") && (w_kings == E1) && !wkingmoved)
		rokirovka = 2;
	if ((mnem == "e8c8") && (b_kings == E8) && !bkingmoved)
		rokirovka = 4;

	if ((rokirovka == 1) || (rokirovka == 2))
	{
		ml.init(WHITE);
		generateMoveList(&ml, WHITE, 0, 0, 127);
		while (ml.ableTomac())
		{
			if (rokirovka == ml.getNextmac())
				return rokirovka;
		}
		return 0;
	}
	if ((rokirovka == 3) || (rokirovka == 4))
	{
		ml.init(BLACK);
		generateMoveList(&ml, BLACK, 0, 0, 127);
		while (ml.ableTomac())
		{
			if (rokirovka == ml.getNextmac())
				return rokirovka;
		}
		return 0;
	}

	if ((mnem[0] >= 'a') && (mnem[0] <= 'h'))
		mnem = "P" + mnem; //e2 -add pawn
	if (mnem[1] == 'x')
		mnem.erase(1, 1);					  //Qxe2 - delete x
	if ((mnem[1] >= 'a') && (mnem[1] <= 'h')) //Rfd1;
		if ((mnem[2] >= 'a') && (mnem[2] <= 'h'))
		{
			line = mnem[1] - 'a'; //get line
			mnem.erase(1, 1);	  //delete line
		}

	halfmove = nameToType(mnem[0]) << 12;
	to = poleToPosition(&mnem[1]);
	halfmove |= to << 6;

	if ((to > 63) || (to < 0))
		return 0;

	ml.init(color);
	generateMoveList(&ml, color, 0, 0, 0);
	while (ml.ableTomac())
	{
		move = ml.getNextmac();
		if ((move & 0b111111111000000) == halfmove)
		{
			if (line != -1)
			{
				if ((move & 0b111) == line) //Rfd1;
					return move;
			}
			else
			{
				return move;
			}
		}
	}

	return 0;
}

void Board::clearEnPassant(char color)
{
	if (color == WHITE)
	{
		w_enpassant = 0;
	}
	else
	{
		b_enpassant = 0;
	}
}

short Board::getMobilityBonus(MoveList *ml)
{
	//if (totalchecks+totalcaptures)
	//	cout << "!";
	//return 3*MOBILITY_MULT * ml->totalchecks + 2*MOBILITY_MULT * ml->totalcaptures + MOBILITY_MULT*ml->totalmoves;
	return 3 * ml->totalchecks + 2 * ml->totalcaptures + ml->totalmoves;
}

short Board::getMobilityBonus2(char color) //unverified moves and captures
{
	BITBOARD ourfigures;
	BITBOARD ourmoves;
	BITBOARD bb;
	BITBOARD tf;
	unsigned char tf_pos;
	BITBOARD enemy_pawn_atacks;
	unsigned char from;

	int score = 0;
	int mvcnt;

	int mgc = number_of_total_figures;
	int egc = 34 - number_of_total_figures;

	if (color == WHITE)
	{
		/////////////////////////////////////////////////////////////////////////
		// all squares atacked by enemy pawns
		#ifdef HEAVY_MOBYLITY
			tf=b_pawns;
			enemy_pawn_atacks=0;
			while (tf)
			{
				tf_pos=LastOne(tf);
				enemy_pawn_atacks |=BB.bpawn_capture_bitboard[tf_pos];
				Clear(tf_pos,&tf);
			}
		#endif
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_knights;

		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];

			ourmoves = (bb ^ w_figures) & bb;

#ifdef HEAVY_MOBYLITY
			ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif
			mvcnt = PopCnt(ourmoves);
			score += (mgc * KnightMobilityBonus_mg[mvcnt] + egc * KnightMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_kings;
		//kings moves
		from = LastOne(ourfigures);
		bb = BB.king_bitboard[from];

		ourmoves = (bb ^ w_figures) & bb;

#ifdef HEAVY_MOBYLITY
		//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

		mvcnt = PopCnt(ourmoves);
		score += (mgc * KingMobilityBonus_mg[mvcnt] + egc * KingMobilityBonus_eg[mvcnt]) / 34;

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);

			ourmoves = (bb ^ w_figures) & bb;

#ifdef HEAVY_MOBYLITY
			//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

			mvcnt = PopCnt(ourmoves);
			score += (mgc * BishopMobilityBonus_mg[mvcnt] + egc * BishopMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);

			ourmoves = (bb ^ w_figures) & bb;

#ifdef HEAVY_MOBYLITY
			//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

			mvcnt = PopCnt(ourmoves);
			score += (mgc * RookMobilityBonus_mg[mvcnt] + egc * RookMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = w_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourmoves = (bb ^ w_figures) & bb;

#ifdef HEAVY_MOBYLITY
			//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

			mvcnt = PopCnt(ourmoves);
			score += (mgc * QueenMobilityBonus_mg[mvcnt] + egc * QueenMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = (w_pawns ^ (wb_figures >> 8)) & w_pawns; //only moves. dont look at ramed pawns
															  //pawns moves
		mvcnt = PopCnt(ourmoves);
		score += (mgc * mvcnt * PAWN_MOBILITY_MULT_MG + egc * mvcnt * PAWN_MOBILITY_MULT_EG) / 34; //simplified. dont count move from 2d line
	}
	//black
	///////////////////////////////////////////////////////////////////////
	if (color == BLACK)
	{
		/////////////////////////////////////////////////////////////////////////
		// all squares atacked by enemy pawns
		#ifdef HEAVY_MOBYLITY
			tf=w_pawns;
			enemy_pawn_atacks=0;
			while (tf)
			{
				tf_pos=LastOne(tf);
				enemy_pawn_atacks |=BB.wpawn_capture_bitboard[tf_pos];
				Clear(tf_pos,&tf);
			}
		#endif
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_knights;

		//knight moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = BB.knight_bitboard[from];

			ourmoves = (bb ^ b_figures) & bb;

#ifdef HEAVY_MOBYLITY
			ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif
			mvcnt = PopCnt(ourmoves);
			score += (mgc * KnightMobilityBonus_mg[mvcnt] + egc * KnightMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//kings
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_kings;
		//kings moves
		from = LastOne(ourfigures);
		bb = BB.king_bitboard[from];
		ourmoves = (bb ^ b_figures) & bb;

#ifdef HEAVY_MOBYLITY
		//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

		mvcnt = PopCnt(ourmoves);
		score += (mgc * KingMobilityBonus_mg[mvcnt] + egc * KingMobilityBonus_eg[mvcnt]) / 34;

		//bishops
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_bishops;
		//bishops moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = slonyachafunk(from);

			ourmoves = (bb ^ b_figures) & bb;

#ifdef HEAVY_MOBYLITY
			//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

			mvcnt = PopCnt(ourmoves);
			score += (mgc * BishopMobilityBonus_mg[mvcnt] + egc * BishopMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//rooks
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_rooks;
		//rooks moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = ladyachafunk(from);

			ourmoves = (bb ^ b_figures) & bb;

#ifdef HEAVY_MOBYLITY
			//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

			mvcnt = PopCnt(ourmoves);
			score += (mgc * RookMobilityBonus_mg[mvcnt] + egc * RookMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//queens
		/////////////////////////////////////////////////////////////////////////
		ourfigures = b_queens;
		//queens moves
		while (ourfigures)
		{
			from = LastOne(ourfigures);
			bb = kvinskafunk(from);
			ourmoves = (bb ^ b_figures) & bb;

#ifdef HEAVY_MOBYLITY
			//ourmoves = (ourmoves ^ enemy_pawn_atacks) & ourmoves;
#endif

			mvcnt = PopCnt(ourmoves);
			score += (mgc * QueenMobilityBonus_mg[mvcnt] + egc * QueenMobilityBonus_eg[mvcnt]) / 34;

			Clear(from, &ourfigures);
		}

		//pawns
		/////////////////////////////////////////////////////////////////////////
		ourfigures = (b_pawns ^ (wb_figures << 8)) & b_pawns; //only moves. dont look at ramed pawns
															  //pawns moves
		mvcnt = PopCnt(ourmoves);
		score += (mgc * mvcnt * PAWN_MOBILITY_MULT_MG + egc * mvcnt * PAWN_MOBILITY_MULT_EG) / 34;
	}

	return score;
}

char *Board::GetFEN(char curlevelcolor) const
{

	static char fen[256];
	char *p = fen;
	int boardpos;
	char space;
	BITBOARD bitboardpos;

	// piece positions
	for (int y = 7; y >= 0; --y)
	{
		space = 0;
		for (int x = 0; x < 8; ++x)
		{
			boardpos = x + y * 8;
			bitboardpos = positionToBITBoard(boardpos);
			if (w_figures & bitboardpos)
			{
				if (space)
				{
					*p++ = '0' + space;
					space = 0;
				}
				if (w_pawns & bitboardpos)
					*p++ = 'P';
				if (w_rooks & bitboardpos)
					*p++ = 'R';
				if (w_knights & bitboardpos)
					*p++ = 'N';
				if (w_bishops & bitboardpos)
					*p++ = 'B';
				if (w_kings & bitboardpos)
					*p++ = 'K';
				if (w_queens & bitboardpos)
					*p++ = 'Q';
			}
			else if (b_figures & bitboardpos)
			{
				if (space)
				{
					*p++ = '0' + space;
					space = 0;
				}
				if (b_pawns & bitboardpos)
					*p++ = 'p';
				if (b_rooks & bitboardpos)
					*p++ = 'r';
				if (b_knights & bitboardpos)
					*p++ = 'n';
				if (b_bishops & bitboardpos)
					*p++ = 'b';
				if (b_kings & bitboardpos)
					*p++ = 'k';
				if (b_queens & bitboardpos)
					*p++ = 'q';
			}
			else
			{
				space++;
			}
		}
		if (space)
		{
			*p++ = '0' + space;
			space = 0;
		}
		if (y > 0)
		{
			*p++ = '/';
		}
	}
	// color to move
	*p++ = ' ';
	*p++ = (curlevelcolor - 1 ? 'w' : 'b');

	// castling
	*p++ = ' ';
	if (!(wOOrokirovka | wOOOrokirovka))
	{
		if (!wOOrokirovka)
			*p++ = 'K';
		if (!wOOOrokirovka)
			*p++ = 'Q';
	}
	if (!(bOOrokirovka | bOOOrokirovka))
	{
		if (!bOOrokirovka)
			*p++ = 'k';
		if (!bOOOrokirovka)
			*p++ = 'q';
	}
	if (wOOrokirovka | wOOOrokirovka | bOOrokirovka | bOOOrokirovka)
		*p++ = '-';

	// en passant square
	*p++ = ' ';
	if (w_enpassant | b_enpassant)
	{
		positionToPole(63 - FirstOne(w_enpassant | b_enpassant), p);
		p += 2;
	}
	else
	{
		*p++ = '-';
	}
	/*
	  // reversible half-move count and full move count
	  snprintf(p, (sizeof(fen) - strlen(fen)), " %d %d",
	           rcount, ((mcount + 1) / 2));
	  */
	return fen;
}
