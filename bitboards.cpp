#include "defenitions.h"
#include "bitboards.h"
#include "utils.h"
#include <stdio.h>


void BITBoards::dumpBITBoards()
{

    for(int i=0; i<64; i++){	printf("knight_bitboard[%d]\n",i); showBITBOARD(knight_bitboard[i]);    }
    for(int i=0; i<64; i++){	printf("king_bitboard[%d]\n",i); showBITBOARD(king_bitboard[i]);    }
    for(int i=0; i<64; i++){	printf("wpawn_forward_bitboard[%d]\n",i); showBITBOARD(wpawn_forward_bitboard[i]);    }
    for(int i=0; i<64; i++){	printf("bpawn_forward_bitboard[%d]\n",i); showBITBOARD(bpawn_forward_bitboard[i]);    }
    for(int i=0; i<64; i++){	printf("wpawn_capture_bitboard[%d]\n",i); showBITBOARD(wpawn_capture_bitboard[i]);    }
    for(int i=0; i<64; i++){	printf("bpawn_capture_bitboard[%d]\n",i); showBITBOARD(bpawn_capture_bitboard[i]);    }
    for(int i=0; i<64; i++){	printf("vectorm9[%d]\n",i); showBITBOARD( vectorm9[i]); }
    for(int i=0; i<64; i++){	printf("vectorm8[%d]\n",i); showBITBOARD( vectorm8[i]); }
    for(int i=0; i<64; i++){	printf("vectorm7[%d]\n",i); showBITBOARD( vectorm7[i]); }
    for(int i=0; i<64; i++){	printf("vectorm1[%d]\n",i); showBITBOARD( vectorm1[i]); }
    for(int i=0; i<64; i++){	printf("vectorp9[%d]\n",i); showBITBOARD( vectorp9[i]); }
    for(int i=0; i<64; i++){	printf("vectorp8[%d]\n",i); showBITBOARD( vectorp8[i]); }
    for(int i=0; i<64; i++){	printf("vectorp7[%d]\n",i); showBITBOARD( vectorp7[i]); }
    for(int i=0; i<64; i++){	printf("vectorp1[%d]\n",i); showBITBOARD( vectorp1[i]); }
    
    for(int i=0; i<8; i++){	printf("bbline[%d]\n",i); showBITBOARD( bbline[i]);}
    for(int k=1; k<8; k++)
        for(int i=0; i<64; i++){	printf("distance[%d][%d]\n",i,k); showBITBOARD( distance_sq[i][k]);}
    for(int i=0; i<64; i++){	printf("wpawn_half_passed[%d]\n",i); showBITBOARD( wpawn_half_passed[i]);}
    for(int i=0; i<64; i++){	printf("bpawn_half_passed[%d]\n",i); showBITBOARD( bpawn_half_passed[i]);}

    for(int i=0; i<64; i++){	printf("bitpos[%d]\n",i); showBITBOARD( bitpos[i]);}

}

void BITBoards::fillVectors() {
/*
    BITBOARD baseBit;
    BITBOARD tempBit;
    int i, j;
    int n = 0;
    for (int y = 0; y < 8; y++)
      for (int x = 0; x < 8; x++) {
        baseBit = (BITBOARD)1L << n;
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( ( (i--) > 0) && ( (j++) < 7)) {
          tempBit = (BITBOARD) tempBit << 7;
          vectorp7[n] |= tempBit;
        }
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( (j++) < 7) {
          tempBit = (BITBOARD) tempBit << 8;
          vectorp8[n] |= tempBit;
        }
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( (i++) < 7) {
          tempBit = (BITBOARD) tempBit << 1;
          vectorp1[n] |= tempBit;
        }
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( ( (i++) < 7) && ( (j++) < 7)) {
          tempBit = (BITBOARD) tempBit << 9;
          vectorp9[n] |= tempBit;
        }
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( ( (i++) < 7) && ( (j--) > 0)) {
          tempBit = (BITBOARD) tempBit >> 7;
          vectorm7[n] |= tempBit;
        }
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( (j--) > 0) {
          tempBit = (BITBOARD) tempBit >> 8;
          vectorm8[n] |= tempBit;
        }
        //
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( (i--) > 0) {
          tempBit = (BITBOARD) tempBit >> 1;
          vectorm1[n] |= tempBit;
        }
        //
        tempBit = baseBit;
        i = x;
        j = y;
        while ( ( (i--) > 0) && ( (j--) > 0)) {
          tempBit = (BITBOARD) tempBit >> 9;
          vectorm9[n] |= tempBit;
        }
// w pawn captures - to lasy
        tempBit = baseBit;
        wpawn_capture_bitboard[n] = 0;
        if (x != 0)
          if (y != 7) {
            tempBit = (BITBOARD) tempBit << 7;
            wpawn_capture_bitboard[n] = tempBit;
          }
        tempBit = baseBit;
        if (x != 7)
          if (y != 7) {
            tempBit = (BITBOARD) tempBit << 9;
            wpawn_capture_bitboard[n] |= tempBit;
          }

// b pawn captures - to lasy
        tempBit = baseBit;
        bpawn_capture_bitboard[n] = 0;
        if (x != 0)
          if (y != 0) {
            tempBit = (BITBOARD) tempBit >> 9;
            bpawn_capture_bitboard[n] = tempBit;
          }
        tempBit = baseBit;
        if (x != 7)
          if (y != 0) {
            tempBit = (BITBOARD) tempBit >> 7;
            bpawn_capture_bitboard[n] |= tempBit;
          }

        n++;
      }
      */

	int x,y,i,j;

	//position to bitboard
    for (int i = 0; i < 64; i++){
    	bitpos[i] =(BITBOARD)1LU <<i;
    }

    //fill distance squares
	//distance_sq[64][8]
    int pos=0;
    int tmppos=0;
    int d;

    for (y = 0; y < 8; y++){
    	for (x = 0; x < 8; x++){
    		pos=x+y*8;
    		for (d = 1; d < 8; d++){
    			distance_sq[pos][d] =0;
    			for(i = x-d; i<=x+d; i++){
    				j=y-d;
    				if((i>=0)&&(j>=0)&&(i<8)&&(j<8)){
    					tmppos=i+j*8;
    					distance_sq[pos][d] |= bitpos[tmppos];
    				}
    				j=y+d;
    				if((i>=0)&&(j>=0)&&(i<8)&&(j<8)){
    					tmppos=i+j*8;
    					distance_sq[pos][d] |= bitpos[tmppos];
    				}
    			}

    			for(j = y-d; j<=y+d; j++){
    				i=x-d;
    				if((i>=0)&&(j>=0)&&(i<8)&&(j<8)){
    					tmppos=i+j*8;
    					distance_sq[pos][d] |= bitpos[tmppos];
    				}
    				i=x+d;
    				if((i>=0)&&(j>=0)&&(i<8)&&(j<8)){
    					tmppos=i+j*8;
    					distance_sq[pos][d] |= bitpos[tmppos];
    				}
    			}
    		}
    	}
    }
    //white and black half passed. (free left, right and front forward columns)
    for (y = 0; y < 7; y++){
        for (x = 0; x < 8; x++){
        		pos=x+y*8;
        		if(x==0){
        			wpawn_half_passed[pos]=vectorp8[pos+1];
        		} else
        		if(x==7){
        			wpawn_half_passed[pos]=vectorp8[pos-1];
        		} else
        		{
        			wpawn_half_passed[pos]=vectorp8[pos+1]|vectorp8[pos-1];
        		}
        		wpawn_half_passed[pos]|=vectorp8[pos];
        }
    }
    for (y = 7; y > 0; y--){
         for (x = 0; x < 8; x++){
         		pos=x+y*8;
         		if(x==0){
         			bpawn_half_passed[pos]=vectorm8[pos+1];
         		} else
         		if(x==7){
         			bpawn_half_passed[pos]=vectorm8[pos-1];
         		} else
         		{
         			bpawn_half_passed[pos]=vectorm8[pos+1]|vectorm8[pos-1];
         		}
         		bpawn_half_passed[pos]|=vectorm8[pos];
         }
     }
     // connected pawns
    //if(0)
    for (y = 6; y > 0; y--){
         for (x = 0; x < 8; x++){
         		pos=x+y*8;
         		if(x==0){
         			wpawn_connected_bitboard[pos]=bitpos[pos+1];
              wpawn_connected_bitboard[pos]|=bitpos[pos-7];
              bpawn_connected_bitboard[pos]=bitpos[pos+1];
              bpawn_connected_bitboard[pos]|=bitpos[pos+9];
         		} else
         		if(x==7){
         			wpawn_connected_bitboard[pos]=bitpos[pos-1];
              wpawn_connected_bitboard[pos]|=bitpos[pos-9];
         			bpawn_connected_bitboard[pos]=bitpos[pos-1];
              bpawn_connected_bitboard[pos]|=bitpos[pos+7];
         		} else
         		{
         			wpawn_connected_bitboard[pos]=bitpos[pos+1];
              wpawn_connected_bitboard[pos]|=bitpos[pos-7];
         			wpawn_connected_bitboard[pos]|=bitpos[pos-1];
              wpawn_connected_bitboard[pos]|=bitpos[pos-9];
         			bpawn_connected_bitboard[pos]=bitpos[pos+1];
              bpawn_connected_bitboard[pos]|=bitpos[pos+7];
         			bpawn_connected_bitboard[pos]|=bitpos[pos-1];
              bpawn_connected_bitboard[pos]|=bitpos[pos+9];
         		}
         }
     }

}


void BITBoards::init()
{

knight_bitboard[0] = 132096LU;
knight_bitboard[1] = 329728LU;
knight_bitboard[2] = 659712LU;
knight_bitboard[3] = 1319424LU;
knight_bitboard[4] = 2638848LU;
knight_bitboard[5] = 5277696LU;
knight_bitboard[6] = 10489856LU;
knight_bitboard[7] = 4202496LU;
knight_bitboard[8] = 33816580LU;
knight_bitboard[9] = 84410376LU;
knight_bitboard[10] = 168886289LU;
knight_bitboard[11] = 337772578LU;
knight_bitboard[12] = 675545156LU;
knight_bitboard[13] = 1351090312LU;
knight_bitboard[14] = 2685403152LU;
knight_bitboard[15] = 1075839008LU;
knight_bitboard[16] = 8657044482LU;
knight_bitboard[17] = 21609056261LU;
knight_bitboard[18] = 43234889994LU;
knight_bitboard[19] = 86469779988LU;
knight_bitboard[20] = 172939559976LU;
knight_bitboard[21] = 345879119952LU;
knight_bitboard[22] = 687463207072LU;
knight_bitboard[23] = 275414786112LU;
knight_bitboard[24] = 2216203387392LU;
knight_bitboard[25] = 5531918402816LU;
knight_bitboard[26] = 11068131838464LU;
knight_bitboard[27] = 22136263676928LU;
knight_bitboard[28] = 44272527353856LU;
knight_bitboard[29] = 88545054707712LU;
knight_bitboard[30] = 175990581010432LU;
knight_bitboard[31] = 70506185244672LU;
knight_bitboard[32] = 567348067172352LU;
knight_bitboard[33] = 1416171111120896LU;
knight_bitboard[34] = 2833441750646784LU;
knight_bitboard[35] = 5666883501293568LU;
knight_bitboard[36] = 11333767002587136LU;
knight_bitboard[37] = 22667534005174272LU;
knight_bitboard[38] = 45053588738670592LU;
knight_bitboard[39] = 18049583422636032LU;
knight_bitboard[40] = 145241105196122112LU;
knight_bitboard[41] = 362539804446949376LU;
knight_bitboard[42] = 725361088165576704LU;
knight_bitboard[43] = 1450722176331153408LU;
knight_bitboard[44] = 2901444352662306816LU;
knight_bitboard[45] = 5802888705324613632LU;
knight_bitboard[46] = 11533718717099671552LU;
knight_bitboard[47] = 4620693356194824192LU;
knight_bitboard[48] = 288234782788157440LU;
knight_bitboard[49] = 576469569871282176LU;
knight_bitboard[50] = 1224997833292120064LU;
knight_bitboard[51] = 2449995666584240128LU;
knight_bitboard[52] = 4899991333168480256LU;
knight_bitboard[53] = 9799982666336960512LU;
knight_bitboard[54] = 1152939783987658752LU;
knight_bitboard[55] = 2305878468463689728LU;
knight_bitboard[56] = 1128098930098176LU;
knight_bitboard[57] = 2257297371824128LU;
knight_bitboard[58] = 4796069720358912LU;
knight_bitboard[59] = 9592139440717824LU;
knight_bitboard[60] = 19184278881435648LU;
knight_bitboard[61] = 38368557762871296LU;
knight_bitboard[62] = 4679521487814656LU;
knight_bitboard[63] = 9077567998918656LU;
king_bitboard[0] = 770LU;
king_bitboard[1] = 1797LU;
king_bitboard[2] = 3594LU;
king_bitboard[3] = 7188LU;
king_bitboard[4] = 14376LU;
king_bitboard[5] = 28752LU;
king_bitboard[6] = 57504LU;
king_bitboard[7] = 49216LU;
king_bitboard[8] = 197123LU;
king_bitboard[9] = 460039LU;
king_bitboard[10] = 920078LU;
king_bitboard[11] = 1840156LU;
king_bitboard[12] = 3680312LU;
king_bitboard[13] = 7360624LU;
king_bitboard[14] = 14721248LU;
king_bitboard[15] = 12599488LU;
king_bitboard[16] = 50463488LU;
king_bitboard[17] = 117769984LU;
king_bitboard[18] = 235539968LU;
king_bitboard[19] = 471079936LU;
king_bitboard[20] = 942159872LU;
king_bitboard[21] = 1884319744LU;
king_bitboard[22] = 3768639488LU;
king_bitboard[23] = 3225468928LU;
king_bitboard[24] = 12918652928LU;
king_bitboard[25] = 30149115904LU;
king_bitboard[26] = 60298231808LU;
king_bitboard[27] = 120596463616LU;
king_bitboard[28] = 241192927232LU;
king_bitboard[29] = 482385854464LU;
king_bitboard[30] = 964771708928LU;
king_bitboard[31] = 825720045568LU;
king_bitboard[32] = 3307175149568LU;
king_bitboard[33] = 7718173671424LU;
king_bitboard[34] = 15436347342848LU;
king_bitboard[35] = 30872694685696LU;
king_bitboard[36] = 61745389371392LU;
king_bitboard[37] = 123490778742784LU;
king_bitboard[38] = 246981557485568LU;
king_bitboard[39] = 211384331665408LU;
king_bitboard[40] = 846636838289408LU;
king_bitboard[41] = 1975852459884544LU;
king_bitboard[42] = 3951704919769088LU;
king_bitboard[43] = 7903409839538176LU;
king_bitboard[44] = 15806819679076352LU;
king_bitboard[45] = 31613639358152704LU;
king_bitboard[46] = 63227278716305408LU;
king_bitboard[47] = 54114388906344448LU;
king_bitboard[48] = 216739030602088448LU;
king_bitboard[49] = 505818229730443264LU;
king_bitboard[50] = 1011636459460886528LU;
king_bitboard[51] = 2023272918921773056LU;
king_bitboard[52] = 4046545837843546112LU;
king_bitboard[53] = 8093091675687092224LU;
king_bitboard[54] = 16186183351374184448LU;
king_bitboard[55] = 13853283560024178688LU;
king_bitboard[56] = 144959613005987840LU;
king_bitboard[57] = 362258295026614272LU;
king_bitboard[58] = 724516590053228544LU;
king_bitboard[59] = 1449033180106457088LU;
king_bitboard[60] = 2898066360212914176LU;
king_bitboard[61] = 5796132720425828352LU;
king_bitboard[62] = 11592265440851656704LU;
king_bitboard[63] = 4665729213955833856LU;
wpawn_forward_bitboard[0] = 256LU;
wpawn_forward_bitboard[1] = 512LU;
wpawn_forward_bitboard[2] = 1024LU;
wpawn_forward_bitboard[3] = 2048LU;
wpawn_forward_bitboard[4] = 4096LU;
wpawn_forward_bitboard[5] = 8192LU;
wpawn_forward_bitboard[6] = 16384LU;
wpawn_forward_bitboard[7] = 32768LU;
wpawn_forward_bitboard[8] = 16842752LU;
wpawn_forward_bitboard[9] = 33685504LU;
wpawn_forward_bitboard[10] = 67371008LU;
wpawn_forward_bitboard[11] = 134742016LU;
wpawn_forward_bitboard[12] = 269484032LU;
wpawn_forward_bitboard[13] = 538968064LU;
wpawn_forward_bitboard[14] = 1077936128LU;
wpawn_forward_bitboard[15] = 2155872256LU;
wpawn_forward_bitboard[16] = 16777216LU;
wpawn_forward_bitboard[17] = 33554432LU;
wpawn_forward_bitboard[18] = 67108864LU;
wpawn_forward_bitboard[19] = 134217728LU;
wpawn_forward_bitboard[20] = 268435456LU;
wpawn_forward_bitboard[21] = 536870912LU;
wpawn_forward_bitboard[22] = 1073741824LU;
wpawn_forward_bitboard[23] = 2147483648LU;
wpawn_forward_bitboard[24] = 4294967296LU;
wpawn_forward_bitboard[25] = 8589934592LU;
wpawn_forward_bitboard[26] = 17179869184LU;
wpawn_forward_bitboard[27] = 34359738368LU;
wpawn_forward_bitboard[28] = 68719476736LU;
wpawn_forward_bitboard[29] = 137438953472LU;
wpawn_forward_bitboard[30] = 274877906944LU;
wpawn_forward_bitboard[31] = 549755813888LU;
wpawn_forward_bitboard[32] = 1099511627776LU;
wpawn_forward_bitboard[33] = 2199023255552LU;
wpawn_forward_bitboard[34] = 4398046511104LU;
wpawn_forward_bitboard[35] = 8796093022208LU;
wpawn_forward_bitboard[36] = 17592186044416LU;
wpawn_forward_bitboard[37] = 35184372088832LU;
wpawn_forward_bitboard[38] = 70368744177664LU;
wpawn_forward_bitboard[39] = 140737488355328LU;
wpawn_forward_bitboard[40] = 281474976710656LU;
wpawn_forward_bitboard[41] = 562949953421312LU;
wpawn_forward_bitboard[42] = 1125899906842624LU;
wpawn_forward_bitboard[43] = 2251799813685248LU;
wpawn_forward_bitboard[44] = 4503599627370496LU;
wpawn_forward_bitboard[45] = 9007199254740992LU;
wpawn_forward_bitboard[46] = 18014398509481984LU;
wpawn_forward_bitboard[47] = 36028797018963968LU;
wpawn_forward_bitboard[48] = 72057594037927936LU;
wpawn_forward_bitboard[49] = 144115188075855872LU;
wpawn_forward_bitboard[50] = 288230376151711744LU;
wpawn_forward_bitboard[51] = 576460752303423488LU;
wpawn_forward_bitboard[52] = 1152921504606846976LU;
wpawn_forward_bitboard[53] = 2305843009213693952LU;
wpawn_forward_bitboard[54] = 4611686018427387904LU;
wpawn_forward_bitboard[55] = 9223372036854775808LU;
wpawn_forward_bitboard[56] = 0LU;
wpawn_forward_bitboard[57] = 0LU;
wpawn_forward_bitboard[58] = 0LU;
wpawn_forward_bitboard[59] = 0LU;
wpawn_forward_bitboard[60] = 0LU;
wpawn_forward_bitboard[61] = 0LU;
wpawn_forward_bitboard[62] = 0LU;
wpawn_forward_bitboard[63] = 0LU;
bpawn_forward_bitboard[0] = 0LU;
bpawn_forward_bitboard[1] = 0LU;
bpawn_forward_bitboard[2] = 0LU;
bpawn_forward_bitboard[3] = 0LU;
bpawn_forward_bitboard[4] = 0LU;
bpawn_forward_bitboard[5] = 0LU;
bpawn_forward_bitboard[6] = 0LU;
bpawn_forward_bitboard[7] = 0LU;
bpawn_forward_bitboard[8] = 1LU;
bpawn_forward_bitboard[9] = 2LU;
bpawn_forward_bitboard[10] = 4LU;
bpawn_forward_bitboard[11] = 8LU;
bpawn_forward_bitboard[12] = 16LU;
bpawn_forward_bitboard[13] = 32LU;
bpawn_forward_bitboard[14] = 64LU;
bpawn_forward_bitboard[15] = 128LU;
bpawn_forward_bitboard[16] = 256LU;
bpawn_forward_bitboard[17] = 512LU;
bpawn_forward_bitboard[18] = 1024LU;
bpawn_forward_bitboard[19] = 2048LU;
bpawn_forward_bitboard[20] = 4096LU;
bpawn_forward_bitboard[21] = 8192LU;
bpawn_forward_bitboard[22] = 16384LU;
bpawn_forward_bitboard[23] = 32768LU;
bpawn_forward_bitboard[24] = 65536LU;
bpawn_forward_bitboard[25] = 131072LU;
bpawn_forward_bitboard[26] = 262144LU;
bpawn_forward_bitboard[27] = 524288LU;
bpawn_forward_bitboard[28] = 1048576LU;
bpawn_forward_bitboard[29] = 2097152LU;
bpawn_forward_bitboard[30] = 4194304LU;
bpawn_forward_bitboard[31] = 8388608LU;
bpawn_forward_bitboard[32] = 16777216LU;
bpawn_forward_bitboard[33] = 33554432LU;
bpawn_forward_bitboard[34] = 67108864LU;
bpawn_forward_bitboard[35] = 134217728LU;
bpawn_forward_bitboard[36] = 268435456LU;
bpawn_forward_bitboard[37] = 536870912LU;
bpawn_forward_bitboard[38] = 1073741824LU;
bpawn_forward_bitboard[39] = 2147483648LU;
bpawn_forward_bitboard[40] = 4294967296LU;
bpawn_forward_bitboard[41] = 8589934592LU;
bpawn_forward_bitboard[42] = 17179869184LU;
bpawn_forward_bitboard[43] = 34359738368LU;
bpawn_forward_bitboard[44] = 68719476736LU;
bpawn_forward_bitboard[45] = 137438953472LU;
bpawn_forward_bitboard[46] = 274877906944LU;
bpawn_forward_bitboard[47] = 549755813888LU;
bpawn_forward_bitboard[48] = 1103806595072LU;
bpawn_forward_bitboard[49] = 2207613190144LU;
bpawn_forward_bitboard[50] = 4415226380288LU;
bpawn_forward_bitboard[51] = 8830452760576LU;
bpawn_forward_bitboard[52] = 17660905521152LU;
bpawn_forward_bitboard[53] = 35321811042304LU;
bpawn_forward_bitboard[54] = 70643622084608LU;
bpawn_forward_bitboard[55] = 141287244169216LU;
bpawn_forward_bitboard[56] = 281474976710656LU;
bpawn_forward_bitboard[57] = 562949953421312LU;
bpawn_forward_bitboard[58] = 1125899906842624LU;
bpawn_forward_bitboard[59] = 2251799813685248LU;
bpawn_forward_bitboard[60] = 4503599627370496LU;
bpawn_forward_bitboard[61] = 9007199254740992LU;
bpawn_forward_bitboard[62] = 18014398509481984LU;
bpawn_forward_bitboard[63] = 36028797018963968LU;
wpawn_capture_bitboard[0] = 512LU;
wpawn_capture_bitboard[1] = 1280LU;
wpawn_capture_bitboard[2] = 2560LU;
wpawn_capture_bitboard[3] = 5120LU;
wpawn_capture_bitboard[4] = 10240LU;
wpawn_capture_bitboard[5] = 20480LU;
wpawn_capture_bitboard[6] = 40960LU;
wpawn_capture_bitboard[7] = 16384LU;
wpawn_capture_bitboard[8] = 131072LU;
wpawn_capture_bitboard[9] = 327680LU;
wpawn_capture_bitboard[10] = 655360LU;
wpawn_capture_bitboard[11] = 1310720LU;
wpawn_capture_bitboard[12] = 2621440LU;
wpawn_capture_bitboard[13] = 5242880LU;
wpawn_capture_bitboard[14] = 10485760LU;
wpawn_capture_bitboard[15] = 4194304LU;
wpawn_capture_bitboard[16] = 33554432LU;
wpawn_capture_bitboard[17] = 83886080LU;
wpawn_capture_bitboard[18] = 167772160LU;
wpawn_capture_bitboard[19] = 335544320LU;
wpawn_capture_bitboard[20] = 671088640LU;
wpawn_capture_bitboard[21] = 1342177280LU;
wpawn_capture_bitboard[22] = 2684354560LU;
wpawn_capture_bitboard[23] = 1073741824LU;
wpawn_capture_bitboard[24] = 8589934592LU;
wpawn_capture_bitboard[25] = 21474836480LU;
wpawn_capture_bitboard[26] = 42949672960LU;
wpawn_capture_bitboard[27] = 85899345920LU;
wpawn_capture_bitboard[28] = 171798691840LU;
wpawn_capture_bitboard[29] = 343597383680LU;
wpawn_capture_bitboard[30] = 687194767360LU;
wpawn_capture_bitboard[31] = 274877906944LU;
wpawn_capture_bitboard[32] = 2199023255552LU;
wpawn_capture_bitboard[33] = 5497558138880LU;
wpawn_capture_bitboard[34] = 10995116277760LU;
wpawn_capture_bitboard[35] = 21990232555520LU;
wpawn_capture_bitboard[36] = 43980465111040LU;
wpawn_capture_bitboard[37] = 87960930222080LU;
wpawn_capture_bitboard[38] = 175921860444160LU;
wpawn_capture_bitboard[39] = 70368744177664LU;
wpawn_capture_bitboard[40] = 562949953421312LU;
wpawn_capture_bitboard[41] = 1407374883553280LU;
wpawn_capture_bitboard[42] = 2814749767106560LU;
wpawn_capture_bitboard[43] = 5629499534213120LU;
wpawn_capture_bitboard[44] = 11258999068426240LU;
wpawn_capture_bitboard[45] = 22517998136852480LU;
wpawn_capture_bitboard[46] = 45035996273704960LU;
wpawn_capture_bitboard[47] = 18014398509481984LU;
wpawn_capture_bitboard[48] = 144115188075855872LU;
wpawn_capture_bitboard[49] = 360287970189639680LU;
wpawn_capture_bitboard[50] = 720575940379279360LU;
wpawn_capture_bitboard[51] = 1441151880758558720LU;
wpawn_capture_bitboard[52] = 2882303761517117440LU;
wpawn_capture_bitboard[53] = 5764607523034234880LU;
wpawn_capture_bitboard[54] = 11529215046068469760LU;
wpawn_capture_bitboard[55] = 4611686018427387904LU;
wpawn_capture_bitboard[56] = 0LU;
wpawn_capture_bitboard[57] = 0LU;
wpawn_capture_bitboard[58] = 0LU;
wpawn_capture_bitboard[59] = 0LU;
wpawn_capture_bitboard[60] = 0LU;
wpawn_capture_bitboard[61] = 0LU;
wpawn_capture_bitboard[62] = 0LU;
wpawn_capture_bitboard[63] = 0LU;
bpawn_capture_bitboard[0] = 0LU;
bpawn_capture_bitboard[1] = 0LU;
bpawn_capture_bitboard[2] = 0LU;
bpawn_capture_bitboard[3] = 0LU;
bpawn_capture_bitboard[4] = 0LU;
bpawn_capture_bitboard[5] = 0LU;
bpawn_capture_bitboard[6] = 0LU;
bpawn_capture_bitboard[7] = 0LU;
bpawn_capture_bitboard[8] = 2LU;
bpawn_capture_bitboard[9] = 5LU;
bpawn_capture_bitboard[10] = 10LU;
bpawn_capture_bitboard[11] = 20LU;
bpawn_capture_bitboard[12] = 40LU;
bpawn_capture_bitboard[13] = 80LU;
bpawn_capture_bitboard[14] = 160LU;
bpawn_capture_bitboard[15] = 64LU;
bpawn_capture_bitboard[16] = 512LU;
bpawn_capture_bitboard[17] = 1280LU;
bpawn_capture_bitboard[18] = 2560LU;
bpawn_capture_bitboard[19] = 5120LU;
bpawn_capture_bitboard[20] = 10240LU;
bpawn_capture_bitboard[21] = 20480LU;
bpawn_capture_bitboard[22] = 40960LU;
bpawn_capture_bitboard[23] = 16384LU;
bpawn_capture_bitboard[24] = 131072LU;
bpawn_capture_bitboard[25] = 327680LU;
bpawn_capture_bitboard[26] = 655360LU;
bpawn_capture_bitboard[27] = 1310720LU;
bpawn_capture_bitboard[28] = 2621440LU;
bpawn_capture_bitboard[29] = 5242880LU;
bpawn_capture_bitboard[30] = 10485760LU;
bpawn_capture_bitboard[31] = 4194304LU;
bpawn_capture_bitboard[32] = 33554432LU;
bpawn_capture_bitboard[33] = 83886080LU;
bpawn_capture_bitboard[34] = 167772160LU;
bpawn_capture_bitboard[35] = 335544320LU;
bpawn_capture_bitboard[36] = 671088640LU;
bpawn_capture_bitboard[37] = 1342177280LU;
bpawn_capture_bitboard[38] = 2684354560LU;
bpawn_capture_bitboard[39] = 1073741824LU;
bpawn_capture_bitboard[40] = 8589934592LU;
bpawn_capture_bitboard[41] = 21474836480LU;
bpawn_capture_bitboard[42] = 42949672960LU;
bpawn_capture_bitboard[43] = 85899345920LU;
bpawn_capture_bitboard[44] = 171798691840LU;
bpawn_capture_bitboard[45] = 343597383680LU;
bpawn_capture_bitboard[46] = 687194767360LU;
bpawn_capture_bitboard[47] = 274877906944LU;
bpawn_capture_bitboard[48] = 2199023255552LU;
bpawn_capture_bitboard[49] = 5497558138880LU;
bpawn_capture_bitboard[50] = 10995116277760LU;
bpawn_capture_bitboard[51] = 21990232555520LU;
bpawn_capture_bitboard[52] = 43980465111040LU;
bpawn_capture_bitboard[53] = 87960930222080LU;
bpawn_capture_bitboard[54] = 175921860444160LU;
bpawn_capture_bitboard[55] = 70368744177664LU;
bpawn_capture_bitboard[56] = 562949953421312LU;
bpawn_capture_bitboard[57] = 1407374883553280LU;
bpawn_capture_bitboard[58] = 2814749767106560LU;
bpawn_capture_bitboard[59] = 5629499534213120LU;
bpawn_capture_bitboard[60] = 11258999068426240LU;
bpawn_capture_bitboard[61] = 22517998136852480LU;
bpawn_capture_bitboard[62] = 45035996273704960LU;
bpawn_capture_bitboard[63] = 18014398509481984LU;
vectorm9[0] = 0LU;
vectorm9[1] = 0LU;
vectorm9[2] = 0LU;
vectorm9[3] = 0LU;
vectorm9[4] = 0LU;
vectorm9[5] = 0LU;
vectorm9[6] = 0LU;
vectorm9[7] = 0LU;
vectorm9[8] = 0LU;
vectorm9[9] = 1LU;
vectorm9[10] = 2LU;
vectorm9[11] = 4LU;
vectorm9[12] = 8LU;
vectorm9[13] = 16LU;
vectorm9[14] = 32LU;
vectorm9[15] = 64LU;
vectorm9[16] = 0LU;
vectorm9[17] = 256LU;
vectorm9[18] = 513LU;
vectorm9[19] = 1026LU;
vectorm9[20] = 2052LU;
vectorm9[21] = 4104LU;
vectorm9[22] = 8208LU;
vectorm9[23] = 16416LU;
vectorm9[24] = 0LU;
vectorm9[25] = 65536LU;
vectorm9[26] = 131328LU;
vectorm9[27] = 262657LU;
vectorm9[28] = 525314LU;
vectorm9[29] = 1050628LU;
vectorm9[30] = 2101256LU;
vectorm9[31] = 4202512LU;
vectorm9[32] = 0LU;
vectorm9[33] = 16777216LU;
vectorm9[34] = 33619968LU;
vectorm9[35] = 67240192LU;
vectorm9[36] = 134480385LU;
vectorm9[37] = 268960770LU;
vectorm9[38] = 537921540LU;
vectorm9[39] = 1075843080LU;
vectorm9[40] = 0LU;
vectorm9[41] = 4294967296LU;
vectorm9[42] = 8606711808LU;
vectorm9[43] = 17213489152LU;
vectorm9[44] = 34426978560LU;
vectorm9[45] = 68853957121LU;
vectorm9[46] = 137707914242LU;
vectorm9[47] = 275415828484LU;
vectorm9[48] = 0LU;
vectorm9[49] = 1099511627776LU;
vectorm9[50] = 2203318222848LU;
vectorm9[51] = 4406653222912LU;
vectorm9[52] = 8813306511360LU;
vectorm9[53] = 17626613022976LU;
vectorm9[54] = 35253226045953LU;
vectorm9[55] = 70506452091906LU;
vectorm9[56] = 0LU;
vectorm9[57] = 281474976710656LU;
vectorm9[58] = 564049465049088LU;
vectorm9[59] = 1128103225065472LU;
vectorm9[60] = 2256206466908160LU;
vectorm9[61] = 4512412933881856LU;
vectorm9[62] = 9024825867763968LU;
vectorm9[63] = 18049651735527937LU;
vectorm8[0] = 0LU;
vectorm8[1] = 0LU;
vectorm8[2] = 0LU;
vectorm8[3] = 0LU;
vectorm8[4] = 0LU;
vectorm8[5] = 0LU;
vectorm8[6] = 0LU;
vectorm8[7] = 0LU;
vectorm8[8] = 1LU;
vectorm8[9] = 2LU;
vectorm8[10] = 4LU;
vectorm8[11] = 8LU;
vectorm8[12] = 16LU;
vectorm8[13] = 32LU;
vectorm8[14] = 64LU;
vectorm8[15] = 128LU;
vectorm8[16] = 257LU;
vectorm8[17] = 514LU;
vectorm8[18] = 1028LU;
vectorm8[19] = 2056LU;
vectorm8[20] = 4112LU;
vectorm8[21] = 8224LU;
vectorm8[22] = 16448LU;
vectorm8[23] = 32896LU;
vectorm8[24] = 65793LU;
vectorm8[25] = 131586LU;
vectorm8[26] = 263172LU;
vectorm8[27] = 526344LU;
vectorm8[28] = 1052688LU;
vectorm8[29] = 2105376LU;
vectorm8[30] = 4210752LU;
vectorm8[31] = 8421504LU;
vectorm8[32] = 16843009LU;
vectorm8[33] = 33686018LU;
vectorm8[34] = 67372036LU;
vectorm8[35] = 134744072LU;
vectorm8[36] = 269488144LU;
vectorm8[37] = 538976288LU;
vectorm8[38] = 1077952576LU;
vectorm8[39] = 2155905152LU;
vectorm8[40] = 4311810305LU;
vectorm8[41] = 8623620610LU;
vectorm8[42] = 17247241220LU;
vectorm8[43] = 34494482440LU;
vectorm8[44] = 68988964880LU;
vectorm8[45] = 137977929760LU;
vectorm8[46] = 275955859520LU;
vectorm8[47] = 551911719040LU;
vectorm8[48] = 1103823438081LU;
vectorm8[49] = 2207646876162LU;
vectorm8[50] = 4415293752324LU;
vectorm8[51] = 8830587504648LU;
vectorm8[52] = 17661175009296LU;
vectorm8[53] = 35322350018592LU;
vectorm8[54] = 70644700037184LU;
vectorm8[55] = 141289400074368LU;
vectorm8[56] = 282578800148737LU;
vectorm8[57] = 565157600297474LU;
vectorm8[58] = 1130315200594948LU;
vectorm8[59] = 2260630401189896LU;
vectorm8[60] = 4521260802379792LU;
vectorm8[61] = 9042521604759584LU;
vectorm8[62] = 18085043209519168LU;
vectorm8[63] = 36170086419038336LU;
vectorm7[0] = 0LU;
vectorm7[1] = 0LU;
vectorm7[2] = 0LU;
vectorm7[3] = 0LU;
vectorm7[4] = 0LU;
vectorm7[5] = 0LU;
vectorm7[6] = 0LU;
vectorm7[7] = 0LU;
vectorm7[8] = 2LU;
vectorm7[9] = 4LU;
vectorm7[10] = 8LU;
vectorm7[11] = 16LU;
vectorm7[12] = 32LU;
vectorm7[13] = 64LU;
vectorm7[14] = 128LU;
vectorm7[15] = 0LU;
vectorm7[16] = 516LU;
vectorm7[17] = 1032LU;
vectorm7[18] = 2064LU;
vectorm7[19] = 4128LU;
vectorm7[20] = 8256LU;
vectorm7[21] = 16512LU;
vectorm7[22] = 32768LU;
vectorm7[23] = 0LU;
vectorm7[24] = 132104LU;
vectorm7[25] = 264208LU;
vectorm7[26] = 528416LU;
vectorm7[27] = 1056832LU;
vectorm7[28] = 2113664LU;
vectorm7[29] = 4227072LU;
vectorm7[30] = 8388608LU;
vectorm7[31] = 0LU;
vectorm7[32] = 33818640LU;
vectorm7[33] = 67637280LU;
vectorm7[34] = 135274560LU;
vectorm7[35] = 270549120LU;
vectorm7[36] = 541097984LU;
vectorm7[37] = 1082130432LU;
vectorm7[38] = 2147483648LU;
vectorm7[39] = 0LU;
vectorm7[40] = 8657571872LU;
vectorm7[41] = 17315143744LU;
vectorm7[42] = 34630287488LU;
vectorm7[43] = 69260574720LU;
vectorm7[44] = 138521083904LU;
vectorm7[45] = 277025390592LU;
vectorm7[46] = 549755813888LU;
vectorm7[47] = 0LU;
vectorm7[48] = 2216338399296LU;
vectorm7[49] = 4432676798592LU;
vectorm7[50] = 8865353596928LU;
vectorm7[51] = 17730707128320LU;
vectorm7[52] = 35461397479424LU;
vectorm7[53] = 70918499991552LU;
vectorm7[54] = 140737488355328LU;
vectorm7[55] = 0LU;
vectorm7[56] = 567382630219904LU;
vectorm7[57] = 1134765260439552LU;
vectorm7[58] = 2269530520813568LU;
vectorm7[59] = 4539061024849920LU;
vectorm7[60] = 9078117754732544LU;
vectorm7[61] = 18155135997837312LU;
vectorm7[62] = 36028797018963968LU;
vectorm7[63] = 0LU;
vectorm1[0] = 0LU;
vectorm1[1] = 1LU;
vectorm1[2] = 3LU;
vectorm1[3] = 7LU;
vectorm1[4] = 15LU;
vectorm1[5] = 31LU;
vectorm1[6] = 63LU;
vectorm1[7] = 127LU;
vectorm1[8] = 0LU;
vectorm1[9] = 256LU;
vectorm1[10] = 768LU;
vectorm1[11] = 1792LU;
vectorm1[12] = 3840LU;
vectorm1[13] = 7936LU;
vectorm1[14] = 16128LU;
vectorm1[15] = 32512LU;
vectorm1[16] = 0LU;
vectorm1[17] = 65536LU;
vectorm1[18] = 196608LU;
vectorm1[19] = 458752LU;
vectorm1[20] = 983040LU;
vectorm1[21] = 2031616LU;
vectorm1[22] = 4128768LU;
vectorm1[23] = 8323072LU;
vectorm1[24] = 0LU;
vectorm1[25] = 16777216LU;
vectorm1[26] = 50331648LU;
vectorm1[27] = 117440512LU;
vectorm1[28] = 251658240LU;
vectorm1[29] = 520093696LU;
vectorm1[30] = 1056964608LU;
vectorm1[31] = 2130706432LU;
vectorm1[32] = 0LU;
vectorm1[33] = 4294967296LU;
vectorm1[34] = 12884901888LU;
vectorm1[35] = 30064771072LU;
vectorm1[36] = 64424509440LU;
vectorm1[37] = 133143986176LU;
vectorm1[38] = 270582939648LU;
vectorm1[39] = 545460846592LU;
vectorm1[40] = 0LU;
vectorm1[41] = 1099511627776LU;
vectorm1[42] = 3298534883328LU;
vectorm1[43] = 7696581394432LU;
vectorm1[44] = 16492674416640LU;
vectorm1[45] = 34084860461056LU;
vectorm1[46] = 69269232549888LU;
vectorm1[47] = 139637976727552LU;
vectorm1[48] = 0LU;
vectorm1[49] = 281474976710656LU;
vectorm1[50] = 844424930131968LU;
vectorm1[51] = 1970324836974592LU;
vectorm1[52] = 4222124650659840LU;
vectorm1[53] = 8725724278030336LU;
vectorm1[54] = 17732923532771328LU;
vectorm1[55] = 35747322042253312LU;
vectorm1[56] = 0LU;
vectorm1[57] = 72057594037927936LU;
vectorm1[58] = 216172782113783808LU;
vectorm1[59] = 504403158265495552LU;
vectorm1[60] = 1080863910568919040LU;
vectorm1[61] = 2233785415175766016LU;
vectorm1[62] = 4539628424389459968LU;
vectorm1[63] = 9151314442816847872LU;
vectorp9[0] = 9241421688590303744LU;
vectorp9[1] = 36099303471055872LU;
vectorp9[2] = 141012904183808LU;
vectorp9[3] = 550831656960LU;
vectorp9[4] = 2151686144LU;
vectorp9[5] = 8404992LU;
vectorp9[6] = 32768LU;
vectorp9[7] = 0LU;
vectorp9[8] = 4620710844295151616LU;
vectorp9[9] = 9241421688590303232LU;
vectorp9[10] = 36099303471054848LU;
vectorp9[11] = 141012904181760LU;
vectorp9[12] = 550831652864LU;
vectorp9[13] = 2151677952LU;
vectorp9[14] = 8388608LU;
vectorp9[15] = 0LU;
vectorp9[16] = 2310355422147510272LU;
vectorp9[17] = 4620710844295020544LU;
vectorp9[18] = 9241421688590041088LU;
vectorp9[19] = 36099303470530560LU;
vectorp9[20] = 141012903133184LU;
vectorp9[21] = 550829555712LU;
vectorp9[22] = 2147483648LU;
vectorp9[23] = 0LU;
vectorp9[24] = 1155177711056977920LU;
vectorp9[25] = 2310355422113955840LU;
vectorp9[26] = 4620710844227911680LU;
vectorp9[27] = 9241421688455823360LU;
vectorp9[28] = 36099303202095104LU;
vectorp9[29] = 141012366262272LU;
vectorp9[30] = 549755813888LU;
vectorp9[31] = 0LU;
vectorp9[32] = 577588851233521664LU;
vectorp9[33] = 1155177702467043328LU;
vectorp9[34] = 2310355404934086656LU;
vectorp9[35] = 4620710809868173312LU;
vectorp9[36] = 9241421619736346624LU;
vectorp9[37] = 36099165763141632LU;
vectorp9[38] = 140737488355328LU;
vectorp9[39] = 0LU;
vectorp9[40] = 288793326105133056LU;
vectorp9[41] = 577586652210266112LU;
vectorp9[42] = 1155173304420532224LU;
vectorp9[43] = 2310346608841064448LU;
vectorp9[44] = 4620693217682128896LU;
vectorp9[45] = 9241386435364257792LU;
vectorp9[46] = 36028797018963968LU;
vectorp9[47] = 0LU;
vectorp9[48] = 144115188075855872LU;
vectorp9[49] = 288230376151711744LU;
vectorp9[50] = 576460752303423488LU;
vectorp9[51] = 1152921504606846976LU;
vectorp9[52] = 2305843009213693952LU;
vectorp9[53] = 4611686018427387904LU;
vectorp9[54] = 9223372036854775808LU;
vectorp9[55] = 0LU;
vectorp9[56] = 0LU;
vectorp9[57] = 0LU;
vectorp9[58] = 0LU;
vectorp9[59] = 0LU;
vectorp9[60] = 0LU;
vectorp9[61] = 0LU;
vectorp9[62] = 0LU;
vectorp9[63] = 0LU;
vectorp8[0] = 72340172838076672LU;
vectorp8[1] = 144680345676153344LU;
vectorp8[2] = 289360691352306688LU;
vectorp8[3] = 578721382704613376LU;
vectorp8[4] = 1157442765409226752LU;
vectorp8[5] = 2314885530818453504LU;
vectorp8[6] = 4629771061636907008LU;
vectorp8[7] = 9259542123273814016LU;
vectorp8[8] = 72340172838076416LU;
vectorp8[9] = 144680345676152832LU;
vectorp8[10] = 289360691352305664LU;
vectorp8[11] = 578721382704611328LU;
vectorp8[12] = 1157442765409222656LU;
vectorp8[13] = 2314885530818445312LU;
vectorp8[14] = 4629771061636890624LU;
vectorp8[15] = 9259542123273781248LU;
vectorp8[16] = 72340172838010880LU;
vectorp8[17] = 144680345676021760LU;
vectorp8[18] = 289360691352043520LU;
vectorp8[19] = 578721382704087040LU;
vectorp8[20] = 1157442765408174080LU;
vectorp8[21] = 2314885530816348160LU;
vectorp8[22] = 4629771061632696320LU;
vectorp8[23] = 9259542123265392640LU;
vectorp8[24] = 72340172821233664LU;
vectorp8[25] = 144680345642467328LU;
vectorp8[26] = 289360691284934656LU;
vectorp8[27] = 578721382569869312LU;
vectorp8[28] = 1157442765139738624LU;
vectorp8[29] = 2314885530279477248LU;
vectorp8[30] = 4629771060558954496LU;
vectorp8[31] = 9259542121117908992LU;
vectorp8[32] = 72340168526266368LU;
vectorp8[33] = 144680337052532736LU;
vectorp8[34] = 289360674105065472LU;
vectorp8[35] = 578721348210130944LU;
vectorp8[36] = 1157442696420261888LU;
vectorp8[37] = 2314885392840523776LU;
vectorp8[38] = 4629770785681047552LU;
vectorp8[39] = 9259541571362095104LU;
vectorp8[40] = 72339069014638592LU;
vectorp8[41] = 144678138029277184LU;
vectorp8[42] = 289356276058554368LU;
vectorp8[43] = 578712552117108736LU;
vectorp8[44] = 1157425104234217472LU;
vectorp8[45] = 2314850208468434944LU;
vectorp8[46] = 4629700416936869888LU;
vectorp8[47] = 9259400833873739776LU;
vectorp8[48] = 72057594037927936LU;
vectorp8[49] = 144115188075855872LU;
vectorp8[50] = 288230376151711744LU;
vectorp8[51] = 576460752303423488LU;
vectorp8[52] = 1152921504606846976LU;
vectorp8[53] = 2305843009213693952LU;
vectorp8[54] = 4611686018427387904LU;
vectorp8[55] = 9223372036854775808LU;
vectorp8[56] = 0LU;
vectorp8[57] = 0LU;
vectorp8[58] = 0LU;
vectorp8[59] = 0LU;
vectorp8[60] = 0LU;
vectorp8[61] = 0LU;
vectorp8[62] = 0LU;
vectorp8[63] = 0LU;
vectorp7[0] = 0LU;
vectorp7[1] = 256LU;
vectorp7[2] = 66048LU;
vectorp7[3] = 16909312LU;
vectorp7[4] = 4328785920LU;
vectorp7[5] = 1108169199616LU;
vectorp7[6] = 283691315109888LU;
vectorp7[7] = 72624976668147712LU;
vectorp7[8] = 0LU;
vectorp7[9] = 65536LU;
vectorp7[10] = 16908288LU;
vectorp7[11] = 4328783872LU;
vectorp7[12] = 1108169195520LU;
vectorp7[13] = 283691315101696LU;
vectorp7[14] = 72624976668131328LU;
vectorp7[15] = 145249953336262656LU;
vectorp7[16] = 0LU;
vectorp7[17] = 16777216LU;
vectorp7[18] = 4328521728LU;
vectorp7[19] = 1108168671232LU;
vectorp7[20] = 283691314053120LU;
vectorp7[21] = 72624976666034176LU;
vectorp7[22] = 145249953332068352LU;
vectorp7[23] = 290499906664136704LU;
vectorp7[24] = 0LU;
vectorp7[25] = 4294967296LU;
vectorp7[26] = 1108101562368LU;
vectorp7[27] = 283691179835392LU;
vectorp7[28] = 72624976397598720LU;
vectorp7[29] = 145249952795197440LU;
vectorp7[30] = 290499905590394880LU;
vectorp7[31] = 580999811180789760LU;
vectorp7[32] = 0LU;
vectorp7[33] = 1099511627776LU;
vectorp7[34] = 283673999966208LU;
vectorp7[35] = 72624942037860352LU;
vectorp7[36] = 145249884075720704LU;
vectorp7[37] = 290499768151441408LU;
vectorp7[38] = 580999536302882816LU;
vectorp7[39] = 1161999072605765632LU;
vectorp7[40] = 0LU;
vectorp7[41] = 281474976710656LU;
vectorp7[42] = 72620543991349248LU;
vectorp7[43] = 145241087982698496LU;
vectorp7[44] = 290482175965396992LU;
vectorp7[45] = 580964351930793984LU;
vectorp7[46] = 1161928703861587968LU;
vectorp7[47] = 2323857407723175936LU;
vectorp7[48] = 0LU;
vectorp7[49] = 72057594037927936LU;
vectorp7[50] = 144115188075855872LU;
vectorp7[51] = 288230376151711744LU;
vectorp7[52] = 576460752303423488LU;
vectorp7[53] = 1152921504606846976LU;
vectorp7[54] = 2305843009213693952LU;
vectorp7[55] = 4611686018427387904LU;
vectorp7[56] = 0LU;
vectorp7[57] = 0LU;
vectorp7[58] = 0LU;
vectorp7[59] = 0LU;
vectorp7[60] = 0LU;
vectorp7[61] = 0LU;
vectorp7[62] = 0LU;
vectorp7[63] = 0LU;
vectorp1[0] = 254LU;
vectorp1[1] = 252LU;
vectorp1[2] = 248LU;
vectorp1[3] = 240LU;
vectorp1[4] = 224LU;
vectorp1[5] = 192LU;
vectorp1[6] = 128LU;
vectorp1[7] = 0LU;
vectorp1[8] = 65024LU;
vectorp1[9] = 64512LU;
vectorp1[10] = 63488LU;
vectorp1[11] = 61440LU;
vectorp1[12] = 57344LU;
vectorp1[13] = 49152LU;
vectorp1[14] = 32768LU;
vectorp1[15] = 0LU;
vectorp1[16] = 16646144LU;
vectorp1[17] = 16515072LU;
vectorp1[18] = 16252928LU;
vectorp1[19] = 15728640LU;
vectorp1[20] = 14680064LU;
vectorp1[21] = 12582912LU;
vectorp1[22] = 8388608LU;
vectorp1[23] = 0LU;
vectorp1[24] = 4261412864LU;
vectorp1[25] = 4227858432LU;
vectorp1[26] = 4160749568LU;
vectorp1[27] = 4026531840LU;
vectorp1[28] = 3758096384LU;
vectorp1[29] = 3221225472LU;
vectorp1[30] = 2147483648LU;
vectorp1[31] = 0LU;
vectorp1[32] = 1090921693184LU;
vectorp1[33] = 1082331758592LU;
vectorp1[34] = 1065151889408LU;
vectorp1[35] = 1030792151040LU;
vectorp1[36] = 962072674304LU;
vectorp1[37] = 824633720832LU;
vectorp1[38] = 549755813888LU;
vectorp1[39] = 0LU;
vectorp1[40] = 279275953455104LU;
vectorp1[41] = 277076930199552LU;
vectorp1[42] = 272678883688448LU;
vectorp1[43] = 263882790666240LU;
vectorp1[44] = 246290604621824LU;
vectorp1[45] = 211106232532992LU;
vectorp1[46] = 140737488355328LU;
vectorp1[47] = 0LU;
vectorp1[48] = 71494644084506624LU;
vectorp1[49] = 70931694131085312LU;
vectorp1[50] = 69805794224242688LU;
vectorp1[51] = 67553994410557440LU;
vectorp1[52] = 63050394783186944LU;
vectorp1[53] = 54043195528445952LU;
vectorp1[54] = 36028797018963968LU;
vectorp1[55] = 0LU;
vectorp1[56] = 18302628885633695744LU;
vectorp1[57] = 18158513697557839872LU;
vectorp1[58] = 17870283321406128128LU;
vectorp1[59] = 17293822569102704640LU;
vectorp1[60] = 16140901064495857664LU;
vectorp1[61] = 13835058055282163712LU;
vectorp1[62] = 9223372036854775808LU;
vectorp1[63] = 0LU;

bbline[0] =0x101010101010101;
bbline[1] =0x202020202020202;
bbline[2] =0x404040404040404;
bbline[3] =0x808080808080808;
bbline[4] =0x1010101010101010;
bbline[5] =0x2020202020202020;
bbline[6] =0x4040404040404040;
bbline[7] =0x8080808080808080;


	fillVectors();
}
