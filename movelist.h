#ifndef MOVELIST
#define MOVELIST

class MoveList {
  public:
    int moves[500];
    short movescores[500];
    int captures[500];
    short capturescores[500];
    int checks[500];
    short checkscores[500];
    int macs[1000];
    short macscores[1000];
    int totalmoves, currentmove;
    int totalcaptures, currentcapture;
    int totalchecks, currentcheck;
    int totalmacs, currentmac;
    char color;
    bool mateflag;

  public:
    void init(char clr);
    void addCapture(int capture);
    void addCapture(int capture, short score);
    void addMove(int move);
    void addMove(int capture, short score);
    void addCheck(int check);
    bool isStaleMate();
    bool isMate();
    void setMateFlag();
    void showMoveListDebug();
    void showTabMoveAndCheckListDebug(char curlevel);
    void clear();
    void sortMoveList(int killer1, int killer2,int hashmove);
    int getNextmac();
    bool ableTomac();
    void addScore(short scr);
    //short getMobilityBonus();

};
#endif
