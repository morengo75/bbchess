/*
 * engine.cpp
 *
 *  Created on: 19 ���. 2017 �.
 *      Author: Ponomarev.N
 */

#include <string.h>
#include <iostream>
#include <stdio.h>

#include <string>
//using namespace std;


#include "defenitions.h"
#include "engine.h"
#include "utils.h"
#include "tree.h"
#include "hash.h"

//#ifdef WITHHASH
//	extern Hash HSH;
//#endif

bool BbcEngine::_initialized = false;
int BbcEngine::_optHash =32;

	const char* BbcEngine::GetEngineName() const{
		return (char*)"Vtchess beta";
	}

	const char* BbcEngine::GetEngineVersion() const{
	  return (char*)"0.1 b";
	}

	const char* BbcEngine::GetAuthorName() const
	{	return (char*)"Nick"; }

	const char* BbcEngine::GetEmailAddress() const
	{	return (char*)"morengo75@mail.ru"; }

	const char* BbcEngine::GetCountryName() const
	{	return (char*)"RU"; }

	char* BbcEngine::GetOptions() const
	{
		return (char*)"option name Hash type spin default 32 min 1 max 32\noption name Threads type spin default 1 min 1 max 2\n";
	}
	bool BbcEngine::SetEngineOption(const char* optionName, const char* optionValue)
	{
	  if (strcmp(optionName, "hash")) {
	      return true;
	    }

	  return false;
	}

void BbcEngine::Initialize(char color)
{
	tr.initDefault(color);
	game_history.init();
	zerolevelcolor=color;
	_initialized = true;
}

bool BbcEngine::IsInitialized() const
{
	return _initialized;
}

	bool BbcEngine::WhiteToMove() const
	{
	  return ( WHITE & zerolevelcolor);
	}

bool BbcEngine::SetEPD(const char* epd)
	{

		if (!epd || !*epd) {
			cout << "NULL or empty epd string" << endl;
			return NULL;
		}
		tr.brd.initClear();

		string sepd(epd),sub_sepd;

		splitStringSpace(&sepd,&sub_sepd);

	    char figure;
	    char pole[2];
	    std::string::iterator it;

	    it = sub_sepd.begin();
	    for (char y = '8'; y > '0'; y--){
			for (char x = 'a'; x < 'i'; x++) {
				if ((*it > '0') && (*it < '9'))
					x = x + (*it - '0' - 1);
				if (((*it > 'a') && (*it < 'z')) || ((*it > 'A') && (*it < 'Z'))) {
					figure = *it;
					pole[0] = x;
					pole[1] = y;
					tr.brd.setFigure(figure, pole);
				}
				if (*it == '/')
					x--;
				it++;
				if(it==sub_sepd.end()) break;
			}
	    }

//move side
	    if(!splitStringSpace(&sepd,&sub_sepd))
	    	return false;
	    switch (*sub_sepd.begin()) {
			case 'b': zerolevelcolor=BLACK; break;
	    	case 'w': zerolevelcolor=WHITE; break;
	    	default: cout << "Bad EPD " << epd << endl; return false;
	    }
	    tr.zerolevelcolor=zerolevelcolor;

//castling
	    if(!splitStringSpace(&sepd,&sub_sepd))
	    	return false;
	    tr.brd.wOOrokirovka=true;
	    tr.brd.wOOOrokirovka=true;
	    tr.brd.bOOrokirovka=true;
	    tr.brd.bOOOrokirovka=true;
	    for(it=sub_sepd.begin(); it!=sub_sepd.end(); it++){
	        switch (*it) {
	        	case '-': break;
	        	case 'K': tr.brd.wOOrokirovka=false; continue;
	        	case 'Q': tr.brd.wOOOrokirovka=false;  continue;
	        	case 'k': tr.brd.bOOrokirovka=false; continue;
	        	case 'q': tr.brd.bOOOrokirovka=false;  continue;
	        	default:
	        		cout << "Bad EPD " << epd << endl; return false;
	        }
	        break;
	    }

//en-passants
	    char enpos=0;
	    if(!splitStringSpace(&sepd,&sub_sepd))
	    	return false;
	    for(it=sub_sepd.begin(); it!=sub_sepd.end(); it++){
	        if(*it =='-') break;
	        else if((*it>='a') && (*it<='h')) enpos+= *it-'a';
	        else if((*it>='1') && (*it<='8')) enpos+= (*it-'1')*8;
	        else {cout << "Bad EPD " << epd << endl; return false;}
	    }
	    if(enpos>=40)
	    	tr.brd.b_enpassant=positionToBITBoard(enpos);
	    else if(enpos>=16)
	    	tr.brd.w_enpassant=positionToBITBoard(enpos);

//options. i miss python
	    string option,value,ov;

	    while(sepd.begin()!=sepd.end()){
	    	if(!splitStringChar(&sepd,&ov,';'))
	    		return false;
	    	if(!splitStringSpace(&ov,&option))
	    		return false;
	    	value=ov;
	    	if(option.substr(0,2)=="bm") expected_bm=tr.brd.halfmnemToMove(value,zerolevelcolor);
	    	if(option.substr(0,2)=="id") epd_id=value;
	    }
	    _initialized=true;
		return true;
	}

bool BbcEngine::SetFEN(const char* fen)
{
	if (!fen || !*fen) {
		cout << "NULL or empty fen string" << endl;
		return NULL;
	}
	tr.brd.initClear();

	string sepd(fen),sub_sepd;

	splitStringSpace(&sepd,&sub_sepd);

	char figure;
	char pole[2];
	std::string::iterator it;

	    it = sub_sepd.begin();
	    for (char y = '8'; y > '0'; y--){
			for (char x = 'a'; x < 'i'; x++) {
				if ((*it > '0') && (*it < '9'))
					x = x + (*it - '0' - 1);
				if (((*it > 'a') && (*it < 'z')) || ((*it > 'A') && (*it < 'Z'))) {
					figure = *it;
					pole[0] = x;
					pole[1] = y;
					tr.brd.setFigure(figure, pole);
				}
				if (*it == '/')
					x--;
				it++;
				if(it==sub_sepd.end()) break;
			}
	    }

//move side
	    if(!splitStringSpace(&sepd,&sub_sepd))
	    	return false;
	    switch (*sub_sepd.begin()) {
			case 'b': zerolevelcolor=BLACK; break;
	    	case 'w': zerolevelcolor=WHITE; break;
	    	default: cout << "Bad FEN " << fen << endl; return false;
	    }
	    tr.zerolevelcolor=zerolevelcolor;

//castling
	    if(!splitStringSpace(&sepd,&sub_sepd))
	    	return false;
	    tr.brd.wOOrokirovka=true;
	    tr.brd.wOOOrokirovka=true;
	    tr.brd.bOOrokirovka=true;
	    tr.brd.bOOOrokirovka=true;
	    for(it=sub_sepd.begin(); it!=sub_sepd.end(); it++){
	        switch (*it) {
	        	case '-': break;
	        	case 'K': tr.brd.wOOrokirovka=false; continue;
	        	case 'Q': tr.brd.wOOOrokirovka=false;  continue;
	        	case 'k': tr.brd.bOOrokirovka=false; continue;
	        	case 'q': tr.brd.bOOOrokirovka=false;  continue;
	        	default:
	        		cout << "Bad FEN " << fen << endl; return false;
	        }
	        break;
	    }

//en-passants
	    char enpos=0;
	    if(!splitStringSpace(&sepd,&sub_sepd))
	    	return false;
	    for(it=sub_sepd.begin(); it!=sub_sepd.end(); it++){
	        if(*it =='-') break;
	        else if((*it>='a') && (*it<='h')) enpos+= *it-'a';
	        else if((*it>='1') && (*it<='8')) enpos+= (*it-'1')*8;
	        else {cout << "Bad FEN " << fen << endl; return false;}
	    }
	    if(enpos>=40)
	    	tr.brd.b_enpassant=positionToBITBoard(enpos);
	    else if(enpos>=16)
	    	tr.brd.w_enpassant=positionToBITBoard(enpos);

	    /// need add num of moves since last capture and num of moves   "0 1"

		return true;
	}


//	"r1bqkb1r/4npp1/p1p4p/1p1pP1B1/8/1B6/PPPN1PPP/R2Q1RK1 w kq - bm Ne4; id \"test1\"";

	bool BbcEngine::MakeMove(const char* str)
	{
		string smove=str;

		if(smove=="O-O"){
			if(tr.zerolevelcolor==WHITE){
				smove="e1g1";
			} else {
				smove="e8g8";
			}
		}
		if(smove=="O-O-O"){
			if(tr.zerolevelcolor==WHITE){
				smove="e1c1";
			} else {
				smove="e8c8";
			}
		}

		int move=tr.brd.mnemToMove(smove.c_str());

		char pos=poleToPosition(smove.c_str());
		char color=0;
		if(positionToBITBoard(pos) & tr.brd.w_figures)
			color=WHITE;
		else
			if(positionToBITBoard(pos) & tr.brd.b_figures)
				color=BLACK;
		if(!color)
			return false;

		tr.brd.makeCaptureOrMove(move,color);

		game_history.setMoveHashToDepth(move,tr.brd.z_hash,game_history.getDepth());
		game_history.setDepth(game_history.getDepth()+1);

//		if(move==2135455)
//			tr.brd.showBoardDebug();

		tr.zerolevelcolor=3-tr.zerolevelcolor;
		zerolevelcolor=3-zerolevelcolor;

		return true;
	}

	void BbcEngine::PrintBoard() const
	{
	 tr.brd.showBoardDebug();

	}

	void BbcEngine::PonderHit()
	{
	  // ponder not supported
	}

	//----------------------------------------------------------------------------
	void BbcEngine::Quit() {
	  // stop searching and exit the timer thread
//	  ChessEngine::Quit();

	  // free up memory allocated for the transposition table
	  //SetHashSize(0);
	}
	//----------------------------------------------------------------------------

	void BbcEngine::GetStats(int* depth,
	                        int* seldepth,
	                        uint64_t* nodes,
	                        uint64_t* qnodes,
	                        uint64_t* msecs,
	                        int* movenum,
	                        char* move) const
	{
	  if (depth) {
	    *depth = 1;
	  }
	  if (seldepth) {
	    *seldepth = 1;
	  }
	  if (nodes) {
	    *nodes = (tr.searchednodes);
	  }
	  if (qnodes) {
	    *qnodes = 1;
	  }
//	  if (msecs) {
//	    *msecs = (Now() - tr.starttime);
//	  }
	  if (movenum) {
	    *movenum = 1;
	  }
	  //if (move && movelen) {
	  //  snprintf(move, movelen, "%s", "e2e4");
	  //}
	}

void* BbcEngine::MyGo(BbcEngineArgs *ea)

{
	const int depth=ea->depth;
	//const int movestogo=ea->movestogo;
	const uint64_t movetime=ea->movetime;
	//const uint64_t wtime=ea->wtime;
	//const uint64_t winc=ea->winc;
	//const uint64_t btime=ea->btime;
	//const uint64_t binc=ea->binc;

	if (!_initialized) {
		printf("Engine not initialized");
		return((char*)"Engine not initialized");
	}

	if(!movetime){
		best_mc=tr.iterate(MAXLEVEL, depth);
	}else{
		best_mc=tr.iterate(MAXTIME, movetime);
	}
	tr.showStats(best_mc.getDepth(),best_mc.getDepth());
	//tr.brd.makeCaptureOrMove(best_mc.moves[0],zerolevelcolor); // possibly we have corrupted board after time break. so no need to make move
	//uci will reinitialize anyway. may be need correct in future.

	cout << "bestmove "<< showMoveUCI(best_mc.moves[0]);
	if(best_mc.getDepth()>1)
		cout << " ponder "<< showMoveUCI(best_mc.moves[1]);
	cout << endl <<flush;
	return NULL;
}


char* BbcEngine::GetFEN() const	{

	static char fen[256];
	char* p = fen;
	int boardpos;
	char space;
	BITBOARD bitboardpos;

	  // piece positions
	for (int y = 7; y >= 0; --y) {
	  space=0;
	  for (int x = 0; x < 8; ++x) {
			  boardpos = x + y * 8;
			  bitboardpos = positionToBITBoard(boardpos);
			  if (tr.brd.w_figures & bitboardpos) {
				  if(space){
					  *p++= '0' + space;
					  space=0;
				  }
	    		  					if (tr.brd.w_pawns & bitboardpos)
	    		  						*p++ = 'P';
	    		  					if (tr.brd.w_rooks & bitboardpos)
	    		  						*p++ = 'R';
	    		  					if (tr.brd.w_knights & bitboardpos)
	    		  						*p++ = 'N';
	    		  					if (tr.brd.w_bishops & bitboardpos)
	    		  						*p++ = 'B';
	    		  					if (tr.brd.w_kings & bitboardpos)
	    		  						*p++ = 'K';
	    		  					if (tr.brd.w_queens & bitboardpos)
	    		  						*p++ = 'Q';
	    		  				}
			  else if (tr.brd.b_figures & bitboardpos) {
				  if(space){
				  	  *p++= '0' + space;
				  	  space=0;
				  }
	    		  					if (tr.brd.b_pawns & bitboardpos)
	    		  						*p++ = 'p';
	    		  					if (tr.brd.b_rooks & bitboardpos)
	    		  						*p++ = 'r';
	    		  					if (tr.brd.b_knights & bitboardpos)
	    		  						*p++ = 'n';
	    		  					if (tr.brd.b_bishops & bitboardpos)
	    		  						*p++ = 'b';
	    		  					if (tr.brd.b_kings & bitboardpos)
	    		  						*p++ = 'k';
	    		  					if (tr.brd.b_queens & bitboardpos)
	    		  						*p++ = 'q';
	    		  				}
			  else{
				  space++;
			  }

		  }
		  if(space){
			*p++= '0' + space;
			space=0;
		  }
		  if (y > 0) {
			  *p++ = '/';
		  }
	  }
	  // color to move
	  *p++ = ' ';
	  *p++ = (WhiteToMove() ? 'w' : 'b');

	  // castling
	  *p++ = ' ';
	  if(!(tr.brd.wOOrokirovka|tr.brd.wOOOrokirovka)){
	  	if (!tr.brd.wOOrokirovka) *p++ = 'K';
	  	if (!tr.brd.wOOOrokirovka)  *p++ = 'Q';
	  }
	  if(!(tr.brd.bOOrokirovka|tr.brd.bOOOrokirovka)){
	   if (!tr.brd.bOOrokirovka) *p++ = 'k';
	   if (!tr.brd.bOOOrokirovka)  *p++ = 'q';
	  }
	  if(tr.brd.wOOrokirovka|tr.brd.wOOOrokirovka|tr.brd.bOOrokirovka|tr.brd.bOOOrokirovka) *p++ = '-';


	  // en passant square
	  *p++ = ' ';
	  if (tr.brd.w_enpassant|tr.brd.b_enpassant) {
		positionToPole(63-FirstOne(tr.brd.w_enpassant|tr.brd.b_enpassant) , p);
		p+=2;  
	  }
	  else {
	    *p++ = '-';
	  }
/*
	  // reversible half-move count and full move count
	  snprintf(p, (sizeof(fen) - strlen(fen)), " %d %d",
	           rcount, ((mcount + 1) / 2));
	  */
	  return fen;
	}

/*	void BbcEngine::ResetStatsTotals() {
	  tr.searchednodes=0;
	}
*/
	long BbcEngine::CalculateMoveTime(long movetime,long enemymovetime,long inct, long einct){
		//max movetime when 26 figures on board
		float coef;
		 if(tr.brd.number_of_total_figures>26) //steep lift side
		 	coef = 1.0 + float(32.0 - tr.brd.number_of_total_figures)/6.0;
		else
			coef = 1.0 + float(tr.brd.number_of_total_figures/26.0); //slope right side
		//our time = increment + max coef at 26figs *(remain time+slow opponent correction)/40 moves 
		long mvt = float(inct)*0.9 + coef*(float(movetime) +float(max(movetime-enemymovetime,0L))/10.0)/40.0;
		//long mvt=inct+(movetime)/(tr.brd.number_of_total_figures+10)+m ax(movetime-enemymovetime,0L)/(tr.brd.number_of_total_figures*10);
		return mvt;
	}



void BbcEngineArgs::Set(int dp, int mtg, uint64_t mt, uint64_t wt, uint64_t wi, uint64_t bt, uint64_t bi)
{
	depth=dp;
	movestogo=mtg;
	movetime=mt;
	wtime=wt;
	winc=wi;
	btime=bt;
	binc=bi;
}
