#ifndef UTILS
#define UTILS

#include "defenitions.h"
#include "bitboards.h"
#include "board.h"
#include <string>
using namespace std;

extern BITBoards BB;

void showBITBOARD(BITBOARD f);
char typeColorToName(char type, char color);
char nameToColor(char f);
unsigned char nameToType(char f);
char poleToPosition(const char *pole);
void positionToPole(char pos, char *pole);
BITBOARD positionToBITBoard(unsigned char pos);
//void Clear(char to, BITBOARD * ourmoves);
//int FirstOne(BITBOARD a); //via macro
//int LastOne(BITBOARD a); //via macro
//int PopCnt(BITBOARD a); //via macro
void showMove(int move, char color);
char* showMoveUCI(int move);
bool splitStringSpace(string* str, string* substr);
bool splitStringChar(string* str, string* substr, char chr);

extern inline void Clear(char to, BITBOARD * ourmoves){ *ourmoves ^= (BITBOARD) 1LU << to;}

extern inline unsigned char BeatenFiguere(int move){ return (move >> 15) & 7; }
extern inline unsigned char FromPos(int move){ return move & 63;}
extern inline unsigned char ToPos(int move){ return (move >> 6) & 63;}

BITBOARD myTime();

#endif
