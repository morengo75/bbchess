/*
 * Opening.h
 *
 *  Created on: 9 ����. 2018 �.
 *      Author: Ponomarev.N
 */
#include "defenitions.h"

#ifndef OPENING_H_
#define OPENING_H_

class Opening {
public:
//	Opening();
//	virtual ~Opening();
	void init(const char* filename);
	int getRandomMove(BITBOARD hash);
private:
	int moves[MAX_OPENING_SIZE];
	BITBOARD hashes[MAX_OPENING_SIZE];
	int loaded_positions;
};

#endif /* OPENING_H_ */
