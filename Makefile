# Source, Executable, Includes, Library Defines
INCL   = data.h defenitions.h bitboards.h utils.h movelist.h hash.h board.h tree.h movechain.h engine.h opening.h main.h
SRC    = data.cpp bitboards.cpp utils.cpp movelist.cpp hash.cpp board.cpp tree.cpp movechain.cpp engine.cpp opening.cpp main.cpp
OBJ    = $(SRC:.cpp=.o)
#LIBS   = -lgen
LIBS   = -lpthread
EXE    = bbc

# Compiler, Linker Defines
#CC      = g++
CC      = /usr/bin/clang++
CFLAGS  = -ansi -pedantic -Wall -O3 -std=c++14
LIBPATH = -L.
LDFLAGS = -o $(EXE) $(LIBPATH) $(LIBS)
CFDEBUG = -ansi -pedantic -Wall -g -DDEBUG -std=c++14 $(LDFLAGS)
CFPROFILE = -ansi -pedantic -Wall -g -DDEBUG -std=c++14 $(LDFLAGS) -lprofiler
RM      = rm -f

# Compile and Assemble C Source Files into Object Files
%.o: %.cpp
	$(CC) -c $(CFLAGS) $*.cpp

# Link all Object Files with external Libraries into Binaries
$(EXE): $(OBJ)
	$(CC) $(LDFLAGS) $(OBJ)

# Objects depend on these Libraries
$(OBJ): $(INCL)

# Create a gdb/dbx Capable Executable with DEBUG flags turned on
debug:
	$(CC) $(CFDEBUG) $(SRC)

profile: 
	$(CC) $(CFPROFILE) $(SRC)

# Clean Up Objects, Exectuables, Dumps out of source directory
clean:
	$(RM) $(OBJ) $(EXE) core a.out
