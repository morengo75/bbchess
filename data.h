#ifndef DATA
#define DATA
//#include "bitboards.h"
//#include "hash.h"
//extern BITBoards BB;
//extern Hash      HSH;
//http://www.sbor.net/~e_k/text/eval.htm
extern short wpawnbonus[64];
extern short bpawnbonus[64];

extern short w_pawn_connected[64];
extern short b_pawn_connected[64];
//extern short wrookbonus[64];
//extern short brookbonus[64];
extern short knightbonus[64];
extern short bishopbonus[64];
extern short kinstartgbonus[64];
extern short kingendbonus[64];

extern short wknightoutpost[64];
extern short bknightoutpost[64];
extern short wbishopoutpost[64];
extern short bbishopoutpost[64];

extern short KnightMobilityBonus_mg[9];
extern short BishopMobilityBonus_mg[14];
extern short RookMobilityBonus_mg[15];
extern short QueenMobilityBonus_mg[28];
extern short KingMobilityBonus_mg[9];

extern short KnightMobilityBonus_eg[9];
extern short BishopMobilityBonus_eg[14];
extern short RookMobilityBonus_eg[15];
extern short QueenMobilityBonus_eg[28];
extern short KingMobilityBonus_eg[9];

extern int MVV_LVA[7][7];

extern short king_tropism_knight[6];
extern short king_tropism_bishop[6];
extern short king_tropism_rook[6];
extern short king_tropism_queen[6];

extern short tropism_vector[16];

#endif
