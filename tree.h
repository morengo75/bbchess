#ifndef TREE
#define TREE
#include "defenitions.h"
#include "board.h"
#include "movechain.h"
#include "opening.h"

#include "treedebug.h"

extern GameHistory game_history;
extern Opening opening;


class Tree
{
public:
  BITBOARD starttime, curtime,stoptime;
  int maxtime;
  double nexttimecheckpoint;

  long searchednodes;
  long searchednodescheckpoint;
  long betacutoffs;
  long betacutoffsQ;

  char maxcheckply;

  char zerolevelcolor;
#ifdef RANDOMIZER
  int randomizer;
#endif
  Board brd;

  MoveChain pv;
  MoveChain killers1;
  MoveChain killers2;

	public:
  	  bool timebreakflag;
  private:

    bool matebreakflag;
    bool legal0levelmovecalculatted;
    MoveList zerolevelmovelist;

public:
  void init(Board *b, char color);
  void initDefault(char color);
  MoveChain ABSearch(char maxlevel, char curlevel, char curlevelcolor, short alpha, short beta, bool null_move);
  MoveChain ABCaptureSearch(char maxlevel, char curlevel, char curlevelcolor, short alpha, short beta);
  MoveChain iterate(char type, uint64_t value);
  bool loadFEN(const char *FEN);
  void showStats(char depth, char seldepth);
  void prepareZeroLevelMoveList(char color);

private:
  void sortFromPrevItterate(MoveList* ml,MoveList* to);
  void incSearchNodes(char depth, char seldepth);

  bool checktrdbg(unsigned char level, int move);

  bool check3FoldRepetition(BITBOARD hash);
  bool checkEnemy3FoldRepetition(BITBOARD hash);


};
#endif
