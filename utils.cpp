#include "defenitions.h"
#include "utils.h"
#include "board.h"
#include <stdio.h>
#include <iostream>

#include <string>
using namespace std;

#include <chrono>

BITBOARD myTime()
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}


void showBITBOARD(BITBOARD f)
{
    char pos;
    for (int i = 7; i >= 0; i--) {
	for (int j = 0; j < 8; j++) {
	    pos = j + i * 8;
	    if (f & ((BITBOARD) 1 << pos))
	    	cout <<"x ";
	    else
	    	cout <<". ";
	}
	cout << endl << std::flush;
    };
}

char typeColorToName(char type, char color)
{
    char name;

    switch (type) {
    case PAWN:
	{
	    name = 'p';
	    break;
	}
    case ROOK:
	{
	    name = 'r';
	    break;
	}
    case KNIGHT:
	{
	    name = 'n';
	    break;
	}
    case BISHOP:
	{
	    name = 'b';
	    break;
	}
    case KING:
	{
	    name = 'k';
	    break;
	}
    case QUEEN:
	{
	    name = 'q';
	    break;
	}
    default:
	name = 'X';
    }
    if (color == 1)
	name = (char) (name - 'a' + 'A');
    return name;
}

char nameToColor(char f)
{
    if (f == 'P' || f == 'R' || f == 'N' || f == 'B' || f == 'K'|| f == 'Q')
	return 1;
    if (f == 'p' || f == 'r' || f == 'n' || f == 'b' || f == 'k'|| f == 'q')
	return 2;
    return 0;
}

unsigned char nameToType(char f)
{
    int N;
    if (f == 'P' || f == 'R' || f == 'N' || f == 'B' || f == 'K'
	|| f == 'Q' || f == 'p' || f == 'r' || f == 'n' || f == 'b'
	|| f == 'k' || f == 'q') {
	if (f > 'A' && f < 'Z')
	    N = f - 'A' + 'a';
	else
	    N = f;
	switch (N) {
	case 'p':
	    return PAWN;
	case 'r':
	    return ROOK;
	case 'n':
	    return KNIGHT;
	case 'b':
	    return BISHOP;
	case 'k':
	    return KING;
	case 'q':
	    return QUEEN;
	}
    }
    return 0;
}

char poleToPosition(const char *pole)
{
    int x, y;
    x = pole[0];
    y = pole[1];
    if (x >= 'a' && x <= 'h')
    	x = x - 'a';
    else {
    	if (x >= 'A' && x <= 'H')
    		x = x - 'A';
	else
	    return 0;
    }
    y = (y - '1') << 3;
    return x + y;
}

void positionToPole(char pos, char *pole)
{
    pole[0] = (char) ((pos & 7) + 'a');
    pole[1] = (char) ((pos >> 3) + '1');
    pole[2] = 0;
}

BITBOARD positionToBITBoard(unsigned char pos)
{
    return BB.bitpos[pos];
}

/*
void Clear(char to, BITBOARD * ourmoves)
{
    *ourmoves ^= (BITBOARD) 1L << to;
}
*/

/*int FirstOne(BITBOARD a)
{
    return __builtin_clzll(a);
}*/
//  asm (
//        "bsr     edx, dword ptr a+4\n\t"
//        "mov     eax, 31\n\t"
//        "jnz     l1\n\t"
//        "bsr     edx, dword ptr a\n\t"
//        "mov     eax, 63\n\t"
//        "jnz     l1\n\t"
//        "mov     edx, -1\n\t"
//"l1:   sub     eax, edx\n\t"
//   );


//FORCEINLINE int LastOne(BITBOARD a) {
/*int LastOne(BITBOARD a)
{
    return __builtin_ctzll(a);
}*/

// asm (
//    "bsf     edx, dword ptr a"
//    "mov     eax, 63"
//    "jnz     l1"
//    "bsf     edx, dword ptr a+4"
//    "mov     eax, 31"
//    "jnz     l1"
//    "mov     edx, -33"
//"l1:   sub     eax, edx"

//  );

/*int PopCnt(BITBOARD a)
{
    return __builtin_popcountll(a);
}*/
//  asm (
//        "mov     ecx, dword ptr a"
//        "xor     eax, eax"
//        "test    ecx, ecx"
//        "jz      l1"
//    "l0: lea     edx, [ecx-1]"
//        "inc     eax"
//        "and     ecx, edx"
//        "jnz     l0"
//    "l1: mov     ecx, dword ptr a+4"
//        "test    ecx, ecx"
//        "jz      l3"
//    "l2: lea     edx, [ecx-1]"
//        "inc     eax"
//        "and     ecx, edx"
//        "jnz     l2"
//    "l3:"
// );

void showMove(int move, char color)
{
    char pole[10];

    if (move == 1) {
		cout <<"O-O ";
		return;
    }
    if (move == 2) {
    	cout <<"O-O-O ";
		return;
    }
    if (move == 3) {
    	cout <<"o-o ";
    	return;
    }
    if (move == 4) {
    	cout <<"o-o-o ";
    	return;
    }

	if (move == 5) {
    	cout <<"mat from hash ";
    	return;
    }

    cout <<typeColorToName((move >> 12) & 7, color);
    if ((move >> 15) & 7) {
    	cout <<"x";
    	cout <<typeColorToName(((move >> 15) & 7), 3 - color);
    }
    cout <<":";
    positionToPole(move & 63, pole);
    cout <<pole;
    cout <<"-";
    positionToPole((move >> 6) & 63, pole);
    cout <<pole;
    cout <<" ";

}

char* showMoveUCI(int move)
{
    static char pole[10];

    if (move == 1) {
		sprintf( pole, "e1g1");
		return pole;
    }
    if (move == 2) {
		sprintf(pole, "e1c1");
		return pole;
    }
    if (move == 3) {
   		sprintf( pole, "e8g8");
   		return pole;
    }
    if (move == 4) {
   		sprintf(pole, "e8c8");
   		return pole;
    }
	if (move == 5) { //mat in hash - null move
		return "";
	}
    positionToPole(move & 63, pole);
    positionToPole((move >> 6) & 63, pole+2);
	char promotion = ((move >> 18) & 7);
	if(promotion)
		pole[4]=typeColorToName(promotion,BLACK);
    return pole;

}

bool splitStringSpace(string* str, string* substr){
	if(!str->length())
		return false;
	while((str->at(0) == ' ') && (str->length()>0))
		*str=str->substr(1);
	if (str->find(" ") != string::npos){
		*substr=str->substr(0,str->find(" "));
		*str=str->substr(str->find(" ")+1);
	}else{
	    *substr=str->substr(0);
	    str->clear();
	}
	return true;
}

bool splitStringChar(string* str, string* substr, char chr){
	if(!str->length())
		return false;
	while((str->at(0) == chr) && (str->length()>0))
			*str=str->substr(1);
	if (str->find(chr) != string::npos){
		*substr=str->substr(0,str->find(chr));
		*str=str->substr(str->find(chr)+1);
	}else{
	    *substr=str->substr(0);
	    str->clear();
	}
	return true;
}
char* GetFEN(Board brd,char curlevelcolor) {

	static char fen[256];
	char* p = fen;
	int boardpos;
	char space;
	BITBOARD bitboardpos;

	  // piece positions
	for (int y = 7; y >= 0; --y) {
	  space=0;
	  for (int x = 0; x < 8; ++x) {
			  boardpos = x + y * 8;
			  bitboardpos = positionToBITBoard(boardpos);
			  if (brd.w_figures & bitboardpos) {
				  if(space){
					  *p++= '0' + space;
					  space=0;
				  }
	    		  					if (brd.w_pawns & bitboardpos)
	    		  						*p++ = 'P';
	    		  					if (brd.w_rooks & bitboardpos)
	    		  						*p++ = 'R';
	    		  					if (brd.w_knights & bitboardpos)
	    		  						*p++ = 'N';
	    		  					if (brd.w_bishops & bitboardpos)
	    		  						*p++ = 'B';
	    		  					if (brd.w_kings & bitboardpos)
	    		  						*p++ = 'K';
	    		  					if (brd.w_queens & bitboardpos)
	    		  						*p++ = 'Q';
	    		  				}
			  else if (brd.b_figures & bitboardpos) {
				  if(space){
				  	  *p++= '0' + space;
				  	  space=0;
				  }
	    		  					if (brd.b_pawns & bitboardpos)
	    		  						*p++ = 'p';
	    		  					if (brd.b_rooks & bitboardpos)
	    		  						*p++ = 'r';
	    		  					if (brd.b_knights & bitboardpos)
	    		  						*p++ = 'n';
	    		  					if (brd.b_bishops & bitboardpos)
	    		  						*p++ = 'b';
	    		  					if (brd.b_kings & bitboardpos)
	    		  						*p++ = 'k';
	    		  					if (brd.b_queens & bitboardpos)
	    		  						*p++ = 'q';
	    		  				}
			  else{
				  space++;
			  }

		  }
		  if(space){
			*p++= '0' + space;
			space=0;
		  }
		  if (y > 0) {
			  *p++ = '/';
		  }
	  }
	  // color to move
	  *p++ = ' ';
	  *p++ = (1-curlevelcolor ? 'w' : 'b');

	  // castling
	  *p++ = ' ';
	  if(!(brd.wOOrokirovka|brd.wOOOrokirovka)){
	  	if (!brd.wOOrokirovka) *p++ = 'K';
	  	if (!brd.wOOOrokirovka)  *p++ = 'Q';
	  }
	  if(!(brd.bOOrokirovka|brd.bOOOrokirovka)){
	   if (!brd.bOOrokirovka) *p++ = 'k';
	   if (!brd.bOOOrokirovka)  *p++ = 'q';
	  }
	  if(brd.wOOrokirovka|brd.wOOOrokirovka|brd.bOOrokirovka|brd.bOOOrokirovka) *p++ = '-';


	  // en passant square
	  *p++ = ' ';
	  if (brd.w_enpassant|brd.b_enpassant) {
		positionToPole(63-FirstOne(brd.w_enpassant|brd.b_enpassant) , p);
		p+=2;  
	  }
	  else {
	    *p++ = '-';
	  }
/*
	  // reversible half-move count and full move count
	  snprintf(p, (sizeof(fen) - strlen(fen)), " %d %d",
	           rcount, ((mcount + 1) / 2));
	  */
	  return fen;
	}
