/*
 * engine.h
 *
 *  Created on: 19 ���. 2017 �.
 *      Author: Ponomarev.N
 */

#ifndef ENGINE_H_
#define ENGINE_H_


#include "board.h"
#include "tree.h"
#include "movechain.h"
#include <string>

class BbcEngineArgs
{
	public:
		int depth;
		int movestogo;
		uint64_t movetime;
		uint64_t wtime;
		uint64_t winc;
		uint64_t btime;
		uint64_t binc;
	public:
		void Set(int dp, int mtg, uint64_t mt, uint64_t wt, uint64_t wi, uint64_t bt, uint64_t bi);
};


class BbcEngine
{
	public: //testing only
		Tree tr;
		char zerolevelcolor;
		static bool _initialized;
		int expected_bm;
		string epd_id;
		MoveChain best_mc;

	private:

	public:
		static int _optHash;

	public:
		const char* GetEngineName() const;
		const char* GetEngineVersion() const;
		const char* GetEmailAddress() const;
		const char* GetAuthorName() const;
		const char* GetCountryName() const;
		char* GetFEN() const;
		char* GetOptions() const;
		bool SetEngineOption(const char* name, const char* value);
		bool IsInitialized() const;
		bool WhiteToMove() const;
		bool SetEPD(const char* epd);
		bool SetFEN(const char* fen);
		bool MakeMove(const char* str);
		void PrintBoard() const;
		void Initialize(char color);
		void PonderHit();
		void Quit();
		long CalculateMoveTime(long movetime,long enemymovetime, long inct, long einct);
		void GetStats(int* depth,
		                int* seldepth,
		                uint64_t* nodes,
		                uint64_t* qnodes,
		                uint64_t* msecs,
		                int* movenum,
		                char* move) const;
		void * MyGo(BbcEngineArgs *ea);
	protected:

};

#endif /* ENGINE_H_ */
