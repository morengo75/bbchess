#ifndef MOVECHAIN
#define MOVECHAIN

#include "movelist.h"

class MoveChain
{

private:
  short boardscore;
  unsigned char depth;
  bool stalemateflag;
  bool mateflag;
  bool checkAtTail;

public:
  int moves[MAXPLY];
  void init();
  void initErase();
  //int getKiller();
  void setStaleMateFlag();
  void setMateFlag();
  short getScore();
  short getDepth();
  int getMove(short dpth);
  void setScore(short sc);
  void setDepth(short dpth);
  void copyFrom(MoveChain *mc);
  void setMoveToHead(int move);
  void setMoveToDepth(int move, short dpth, short scr);
  void addToTail(MoveChain mc);
  void showMoveChainDebug();
  void showMoveChainDebugCurrmove(BITBOARD time);
  void showMoveChainDebugCurrmoveWOCP(BITBOARD time);
  void showMoveChainDebugWOCP();
  void changeMATScoreClimbingUP();
  bool compare(MoveChain *mc);
};


class GameHistory {
   private:
	BITBOARD hashes[MAXHISTORY];
	int moves[MAXHISTORY];
	unsigned short depth;
   public:
	void init();
	BITBOARD getHash(short dpth);
	void setMoveHashToDepth(int move, BITBOARD hash, short dpth);
	short getDepth();
	void setDepth(short dpth);

};




#endif
