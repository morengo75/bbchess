#include <stdio.h>
#include <iostream>
using namespace std;
#include "utils.h"
#include "movechain.h"

void MoveChain::init()
{
  depth = 0;
  stalemateflag = false;
  mateflag=false;
  checkAtTail=false;
  moves[0] = 0;
}

void MoveChain::initErase()
{
  depth = 0;
  stalemateflag = false;
  mateflag=false;
  checkAtTail=false;
  for(int i =0; i<MAXPLY; i++){
	  moves[i]=0;
  }
}

/*
int  MoveChain::getKiller() {
#ifdef WITHKILLER
  if (depth>0)
      return moves[1];
#endif
  return 0;
}
*/

void MoveChain::setStaleMateFlag()
{
  stalemateflag=true;
}

void MoveChain::setMateFlag()
{
  mateflag=true;
}

short MoveChain::getScore()
{
  return boardscore;
}

short MoveChain::getDepth()
{
  return depth;
}

void MoveChain::setScore(short sc)
{
  boardscore=sc;
}

void MoveChain::setDepth(short dpth)
{
  depth=dpth;
}

void MoveChain::copyFrom(MoveChain *mc)
{
  stalemateflag = mc->stalemateflag;
  mateflag = mc->mateflag;
  depth = mc->depth;
  for (int i = 0; i < depth; i++)
  {
    moves[i] = mc->moves[i];
   }
  boardscore=mc->boardscore;
  checkAtTail=mc->checkAtTail;
}

bool MoveChain::compare(MoveChain *mc)
{
  for (int i = 0; i < depth; i++)
  {
    if(moves[i] != mc->moves[i])
      return false;
  }
  return true;
}

void MoveChain::setMoveToHead(int move)
{
  moves[0] = move;
  depth=1;
}

void MoveChain::setMoveToDepth(int move, short dpth, short scr)
{
	if(dpth<MAXPLY){
		moves[dpth] = move;
		depth=dpth;
		boardscore=scr;
	}
	else
		cout << "info string Warning: maxply > " << MAXPLY << endl;
}

void MoveChain::addToTail(MoveChain mc)
{
  for (int i = 0; i < mc.depth; i++)
    moves[i + depth] = mc.moves[i];
  depth = mc.depth+1;
  boardscore = -mc.boardscore;
  stalemateflag = mc.stalemateflag;
  mateflag = mc.mateflag;
  checkAtTail = mc.checkAtTail;
}

void MoveChain::showMoveChainDebug()
{
   if(mateflag)
	   cout << "info string Mat" << endl;
   if(stalemateflag)
	   cout << "info string Stalemat" << endl;
   cout << "info depth " << (short)depth << " score cp "<< (short)boardscore << " pv ";
   for (int i = 0; i < depth; i++) {
      if(moves[i]!=5) //not mat from hash
	      cout << showMoveUCI(moves[i]) << " ";
   }
   cout << endl << flush;
   #ifdef MOVECHAINDEBUG
   for (int i = 0; i < depth; i++) {
      if(moves[i]!=5) //not mat from hash
	      cout << moves[i] << " ";
   }
   cout << endl << flush;
   #endif
}

void MoveChain::showMoveChainDebugCurrmove(BITBOARD time)
{
   if(mateflag)
	   cout << "info string Mat" << endl;
   if(stalemateflag)
	   cout << "info string Stalemat" << endl;
   cout << "info currmove " << showMoveUCI(moves[0]) << " score cp "<< (short)boardscore << " curtime " << time ;
   cout << endl << flush;
}
void MoveChain::showMoveChainDebugCurrmoveWOCP(BITBOARD time)
{
   if(mateflag)
	   cout << "info string Mat" << endl;
   if(stalemateflag)
	   cout << "info string Stalemat" << endl;
   cout << "info currmove " << showMoveUCI(moves[0]) << " curtime " << time ;
   cout << endl << flush;
}

void MoveChain::showMoveChainDebugWOCP()
{
   if(mateflag)
     cout << "info string Mat" << endl;
   if(stalemateflag)
     cout << "info string Stalemat" << endl;
   cout << "info depth " << (short)depth << " pv ";
   for (int i = 0; i < depth; i++) {
        if(moves[i]!=5) //not mat from hash
     	      cout << showMoveUCI(moves[i]) << " ";
   }
   cout << endl << flush;
}


int MoveChain::getMove(short dpth)
{
	return moves[dpth];
}

void MoveChain::changeMATScoreClimbingUP()
{
  if (boardscore > MAT - 50)
    boardscore--;
  if (boardscore < -MAT + 50)
    boardscore++;
}

BITBOARD GameHistory::getHash(short dpth)
{
	return hashes[dpth];
}

void GameHistory::init()
{
  depth = 0;
}

short GameHistory::getDepth()
{
  return depth;
}


void GameHistory::setDepth(short dpth)
{
  depth=dpth;
}

void GameHistory::setMoveHashToDepth(int move, BITBOARD hash, short dpth)
{
	if(dpth<MAXHISTORY){
		moves[dpth] = move;
		hashes[dpth]=hash;
		depth=dpth;
	}
	else
		cout << "info string Warning: maxhistory > " << MAXHISTORY << endl;
}
