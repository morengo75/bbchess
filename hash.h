#ifndef HASH
#define HASH

#include "defenitions.h"
#include "board.h"

class Hash {
    friend class Board;

  public:

    BITBOARD zobrist[3][7][64];

    BITBOARD * checksum;
    char *depth;
    char *entrytype;
    char *color;
    int *score;
    int *bestmove;
    BITBOARD whiteZ; //leave checksumm unmodified if color == black
    BITBOARD mask;

    unsigned int htsize;
    long hashProbes;
    long hashHit;
    long hashCollisions;
    long hash_full;

    bool init(int hs);
    void flush(void);
    void flushNotExact(void);
    BITBOARD getHashChecksum(Board * brd);
    unsigned int getHashIndex(Board * brd);
    unsigned int getHashIndex(BITBOARD hsh);
    bool hashProbe(char clr, char dpth, int *scr, int *bstmv, int *hashdepth, char *foundflag, BITBOARD z_hash);
    int hashProbeQuick(char clr, BITBOARD z_hash, char dpth);
    int hashProbeMove(char clr, BITBOARD z_hash, char dpth);
    void hashStore(char clr, char dpth, char entrtp, int scr,int bstmv, BITBOARD z_hash);
    BITBOARD computeHash(Board * brd);

};
#endif
