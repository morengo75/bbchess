#include "movelist.h"
#include <stdio.h>
#include "utils.h"
#include "data.h"

#include <iostream>
#include <algorithm>
#include <string.h>
using namespace std;

void MoveList::init(char clr)
{
    color = clr;
    clear();
}

/*short MoveList::getMobilityBonus()
{
	//if (totalchecks+totalcaptures)
	//	cout << "!";
    return 3*MOBILITY_MULT * totalchecks + 2*MOBILITY_MULT * totalcaptures + MOBILITY_MULT*totalmoves;
}*/

void MoveList::clear()
{
    totalmoves = currentmove = totalcaptures = currentcapture = 0;
    totalmacs = currentmac = totalchecks = currentcheck = 0;
    mateflag = false;
}

void MoveList::addCapture(int capture)
{
    captures[totalcaptures++] = capture;
    macs[totalmacs++] = capture;
}
void MoveList::addCapture(int capture, short score)
{
	capturescores[totalcaptures] = score;
    captures[totalcaptures++] = capture;
    macs[totalmacs++] = capture;
}

void MoveList::addMove(int move)
{
    moves[totalmoves++] = move;
    macs[totalmacs++] = move;
}

void MoveList::addMove(int move, short score)
{
	movescores[totalmoves] =score;
    moves[totalmoves++] = move;
    macs[totalmacs++] = move;
}

void MoveList::addCheck(int check)
{
    checks[totalchecks++] = check;
    macs[totalmacs++] = check;
}

bool MoveList::isStaleMate()
{
    if (totalmacs)
    	return false;
    return true;
}

void MoveList::showMoveListDebug()
{
    int i;
    cout << "Captures: ";
    for (i = 0; i < totalcaptures; i++)
    	showMove(captures[i], color);
    cout << "\nMoves: ";
    for (i = 0; i < totalmoves; i++)
    	showMove(moves[i], color);
    cout << "\nMoves and captures: ";
    for (i = 0; i < totalmacs; i++)
    	showMove(macs[i], color);
    cout << endl;
}

void MoveList::showTabMoveAndCheckListDebug(char curlevel)
{
    for(int i=0;i<curlevel;i++)cout << "\t";
    if(totalcaptures){
		cout << "Captures: ";
		for(int i = 0; i < totalcaptures; i++)
			showMove(captures[i], color);
		cout << endl;
	}
    if(totalchecks){
		for(int i=0;i<curlevel;i++) cout << "\t";
		cout << "Checks: ";
		for(int i = 0; i < totalchecks; i++)
			showMove(checks[i], color);
		cout << endl;
    }
    if(totalmoves){
	for(int i=0;i<curlevel;i++) cout << "\t";
		cout << "Moves: ";
		for(int i = 0; i < totalmoves; i++)
			showMove(moves[i], color);
		cout << endl;
    }
}


int MoveList::getNextmac()
{
    if (totalmacs != currentmac)
	return macs[currentmac++];
    return 0;
}

bool MoveList::ableTomac()
{
    if (totalmacs == currentmac)
	return false;
    return true;
}

bool MoveList::isMate()
{
    if (mateflag && (totalmacs == 0))
	return true;
    return false;
}

void MoveList::setMateFlag()
{
    mateflag = true;
}

void MoveList::addScore(short scr)
{
    macscores[currentmac++] = scr;
}

// We need this function to define how to sort
// the vector. We will pass this function into the
// third parameter and it will tell it to sort descendingly.
bool reverseSort(BITBOARD i, BITBOARD j) { return i > j; }

/*
mvv-lva score 		unused		check	en-passant		promo		beatenfigure	figure		to	  		from
	  	  111| 	  	 0| 	  		1|			1|		111|		111|			111|		111111|		111111
   	   	 	24 	 	 23	     		22			21       18  		   15   		   12     	      6          0

*/

void MoveList::sortMoveList(int killer1,int killer2,int hashmove)
{
    int n = 0;
    int i;


#ifdef WITHHASHSORT // place first moves from the hash
    BITBOARD hashed_moves[500];
    int hn=0;

    for (i = 0; i < totalcaptures; i++){
		if(capturescores[i]!=INFINITY_SCORE){
			hashed_moves[hn++]=(BITBOARD)((BITBOARD)capturescores[i]<<32)+captures[i];
			captures[i]=0;
		}
    }

    for (i = 0; i < totalchecks; i++){
		if(checkscores[i]!=INFINITY_SCORE){
			hashed_moves[hn++]=(BITBOARD)((BITBOARD)checkscores[i]<<32)+checks[i];
			checks[i]=0;
		}
    }

    for (i = 0; i < totalmoves; i++){
		if(movescores[i]!=INFINITY_SCORE){
			hashed_moves[hn++]=(BITBOARD)((BITBOARD)movescores[i]<<32)+moves[i];
			moves[i]=0;
		}
    }
    if(hn>1){ //why  it doesn't work ?
    	sort(hashed_moves, hashed_moves+hn,reverseSort); //not to much speedup. looks like waste of time because sorting hashed moves takes more time than hash check.
    	//sort(hashed_moves, hashed_moves+hn); //not to much speedup. looks like waste of time because sorting hashed moves takes more time than hash check.
    }

    for (i = 0; i < hn; i++){
    	macs[n++]=hashed_moves[i] & 0b11111111111111111111111;
    }

#endif

#ifdef MVVLVA   //need to recode because hashed captures[] ==0
    int atackingfigure,capturedfigure;
    for (i = 0; i < totalcaptures; i++){
    	atackingfigure = (captures[i] >> 12) & 7;
    	capturedfigure = (captures[i] >> 15) & 7;
    	captures[i]+= MVV_LVA[atackingfigure][capturedfigure]; //add mvvlva bonus for sorting
    }
    if(totalcaptures>1)
    	sort(captures, captures+totalcaptures,reverseSort);
    for (i = 0; i < totalcaptures; i++){
    	captures[i]&=0b11111111111111111111111;  //cut off bonus
    }
#endif

#ifdef WITHHASHSORT1
/*	
    if (hashmove){
        for (i = 1; i < totalmoves; i++)
			if (moves[i] == hashmove) {
				//memcpy(&moves[1],moves,i*sizeof(int));
				moves[i] = moves[0];
				moves[0] = hashmove;
				break;
			}
    }
    if (hashmove){
        for (i = 1; i < totalcaptures; i++)
			if (captures[i] == hashmove) {
				//memcpy(&moves[1],moves,i*sizeof(int));
				captures[i] = captures[0];
				captures[0] = hashmove;
				break;
			}
    }
	if (hashmove){
        for (i = 1; i < totalchecks; i++)
			if (checks[i] == hashmove) {
				//memcpy(&moves[1],moves,i*sizeof(int));
				checks[i] = checks[0];
				checks[0] = hashmove;
				break;
			}
    }
*/	

	//if(0)
    if (hashmove){
        for (i = 0; i < totalmoves; i++)
			if (moves[i] == hashmove) {
				moves[i] = 0;
				macs[n++] = hashmove;
				break;
			}
    }
	//if(0)
    if (hashmove){
        for (i = 0; i < totalcaptures; i++)
			if (captures[i] == hashmove) {
				captures[i] = 0;
				macs[n++] = hashmove;
				break;
			}
    }
	//if(0)
	if (hashmove){ //waste of time
        for (i = 0; i < totalchecks; i++)
			if (checks[i] == hashmove) {
				checks[i] = 0;
				macs[n++] = hashmove;
				break;
			}
    }
	
#endif

#ifdef WITHKILLER // looks like killer heuristic should be quiet move
/*
    if (killer1){
 		for (i = 1; i < totalmoves; i++)
 			if (moves[i] == killer1) {
 				//memcpy(&moves[1],moves,i*sizeof(int));
 				moves[i] = moves[0];
 				moves[0] = killer1;
 				break;
 			}
     }

    if (killer2){
        for (i = 2; i < totalmoves; i++)
			if (moves[i] == killer2) {
				//memcpy(&moves[1],moves,i*sizeof(int));
				moves[i] = moves[1];
				moves[1] = killer2;
				break;
			}
    }
*/	

	//if(0)
    if (killer1){
 		for (i = 0; i < totalmoves; i++)
 			if (moves[i] == killer1) {
 				moves[i] = 0;
 				macs[n++] = killer1;
 				break;
 			}
     }
	if(0)
    if (killer2){
        for (i = 0; i < totalmoves; i++)
			if (moves[i] == killer2) {
				moves[i] = 0;
				macs[n++] = killer2;
				break;
			}
    }

#endif


    for (i = 0; i < totalcaptures; i++){
    	if(captures[i])
    		macs[n++] = captures[i];
    }

    for(i=0;i<totalchecks; i++)
    	if(checks[i])
    		macs[n++]=checks[i];

    for (i = 0; i < totalmoves; i++)
    	if(moves[i])
    		macs[n++] = moves[i];

}
