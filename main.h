#ifndef MAIN
#define MAIN

#include "hash.h"
#include "utils.h"
#include "bitboards.h"
#include "board.h"
#include "data.h"
#include "tree.h"

//#include "UCI/ChessEngine.h"

void play2comp(void);
void playVsComp(void);
void solve(const char* epd, int testtype, uint64_t value);
void *uci(void *uci_parm);
void test(void);
void bestMoveTest(const char* filename, int testtype, uint64_t value);
void cutOffTest(const char* filename, int testtype, uint64_t value); //bullet
void loadOpeninigBook(const char* filename);
void perft(void);
void perft2(void);

#endif
