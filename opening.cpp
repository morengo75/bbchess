/*
 * Opening.cpp
 *
 *  Created on: 9 ����. 2018 �.
 *      Author: Ponomarev.N
 */

#include "opening.h"
#include "defenitions.h"
#include "engine.h"
#include "utils.h"

#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <fstream>

/*Opening::Opening() {
	// TODO Auto-generated constructor stub
}

Opening::~Opening() {
	// TODO Auto-generated destructor stub
}
*/
void Opening::init(const char* filename){

	BbcEngine engine;

	std::vector<std::string> opens;

	std::ifstream fens_file(filename);
	std::string line;

	while(std::getline(fens_file, line)){
		std::string new_line;
		new_line = line + "\n";
		opens.push_back(new_line);
	}

	int n=0;

	for ( unsigned int i = 0; (i < opens.size())&&(n<MAX_OPENING_SIZE); i++) {
		engine.Initialize(WHITE);
		string Line,move;
		int mv;

		Line=opens[i];
		int color=WHITE;

		while(Line.length()&&(n<MAX_OPENING_SIZE)){
			if(!splitStringSpace(&Line,&move))
				break;

			hashes[n]=engine.tr.brd.z_hash;

			mv=engine.tr.brd.halfmnemToMove(move, color);
			if(!mv) //check move validity
				break;
			engine.tr.brd.makeCaptureOrMove(mv,color);

			moves[n]=mv;

			color=3-color;
			n++;

		}

	}
	//remove duplicates
	int matched_pos=0;
	while(matched_pos<MAX_OPENING_SIZE){
		if(!hashes[matched_pos]){
			matched_pos++;
			continue;
		}
		for ( int i = matched_pos+1; i < MAX_OPENING_SIZE; i++) {
			if(hashes[i]){
				if((hashes[i]==hashes[matched_pos]) && (moves[i]==moves[matched_pos])){
					//cout << i << " duplicate " << hashes[i] << ":" << moves[i] << endl;
					hashes[i]=0;
					moves[i]=0;
				}
			}

		}
		matched_pos++;
	}
	//squize
	matched_pos=0;
	for ( int i = matched_pos+1; i < MAX_OPENING_SIZE; i++) {
		if(hashes[i]){
			hashes[matched_pos]=hashes[i];
			moves[matched_pos]=moves[i];
			matched_pos++;
		}
	}
	loaded_positions=matched_pos;
}

int Opening::getRandomMove(BITBOARD hash)
{
	int found=0;
	srand(myTime());
	for ( int i = 0; i < loaded_positions; i++) {
			if(hashes[i]==hash){
				found++;
			}
	}
	if(!found)
		return 0;
	else{
		int rnd=rand()%found+1;
		for ( int i = 0; i < loaded_positions; i++) {
			if(hashes[i]==hash){
				rnd--;
				if(!rnd)
					return moves[i];
			}

		}
	}

	return 0;
}
