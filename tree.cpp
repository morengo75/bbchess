#include <time.h>
#include <ctime>
#include <string.h>
#include <stdio.h>

#include "tree.h"
#include "hash.h"
#include "movechain.h"
#include "utils.h"

#include <iostream>

using namespace std;

#ifdef WITHHASH
extern Hash HSH;
#endif

void Tree::init(Board *b, char color)
{
	memcpy(&brd, b, sizeof(Board));
	zerolevelcolor = color;
	timebreakflag = false;
	matebreakflag = false;
	legal0levelmovecalculatted = false;
	searchednodes = 0;
	betacutoffs = 0;
	betacutoffsQ = 0;
	zerolevelmovelist.init(zerolevelcolor);
	brd.generateMoveList(&zerolevelmovelist, zerolevelcolor, 0, 0, 127);
	pv.init();
	killers1.init();
	killers2.init();
#ifdef RANDOMIZER
	randomizer = 0;
#endif
}

void Tree::initDefault(char color)
{
	brd.initDefault();
	zerolevelcolor = color;
	timebreakflag = false;
	matebreakflag = false;
	legal0levelmovecalculatted = false;
	searchednodes = 0;
	zerolevelmovelist.init(zerolevelcolor);
	brd.generateMoveList(&zerolevelmovelist, zerolevelcolor, 0, 0, 127);
}

MoveChain Tree::ABCaptureSearch(char maxlevel, char curlevel, char curlevelcolor, short alpha, short beta)
{

	MoveChain curchain;
	MoveChain bestchain;

	curchain.init();
	bestchain.init();
	//    int nextLevelKiller = 0;

	bestchain.setScore(-INFINITY_SCORE);

	/////////////////////////////////////////////////////////////////////////////
	// if our king is eaten on prev level
/*
	if (brd.badQuickMatCheck(curlevelcolor))
	{
		curchain.setScore(-MAT);
		curchain.setMateFlag();
		//	    if (curchain.getScore() >= beta) { //need to return immideately. null move would be better by estimate than quick check
		return curchain;
		//	    }
		//	    if (curchain.getScore() > alpha){
		//	        	alpha=curchain.getScore();
		//	    }
		//	    bestchain.copyFrom(&curchain); // even mate better than infinity
	}
*/
/////////////////////////////////////////////////////////////////////
// check if position in hash with depth >=0
char hashf = hashfALPHA; //just in case we use WITHHASHSTOREQ or WITHHASHINQ
//#ifdef WITHHASHSTOREQ
//	char hashf = hashfALPHA;
//#endif
#ifdef WITHHASHINQ
//	char hashf = hashfALPHA;

	int hashscore;
	int hashbestmove;
	int hashdepth;
	char foundflag;
	if (curlevel != 0) //��� �� ���������� ���� �� 1 ������
		if(maxlevel-curlevel==-1)
		if (HSH.hashProbe(curlevelcolor, -1, &hashscore, &hashbestmove, &hashdepth, &foundflag, brd.z_hash))
		{
			bestchain.setMoveToHead(hashbestmove);
//score from hash may be no equl to 3 fold repetition draw
#ifdef WITHHASH
			if (curlevel == 0)
				if (check3FoldRepetition(brd.z_hash))
					bestchain.setScore(STALEMAT);
			if (curlevel == 1)
				if (checkEnemy3FoldRepetition(brd.z_hash))
					bestchain.setScore(STALEMAT); // dont know what is stale mate from enemy point of view. Possibly need to set this value dynamicaly.
#endif
			//bestchain.showMoveChainDebugCurrmove(hashbestmove);

			if ((hashscore > MAT - 30) && ((foundflag == hashfEXACT) || (foundflag == hashfBETA)))
			{
				bestchain.setScore(hashscore);
				bestchain.setMateFlag();
				//return bestchain;
			}
			if ((hashscore < -MAT + 30) && ((foundflag == hashfEXACT) || (foundflag == hashfALPHA)))
			{
				bestchain.setScore(hashscore);
				bestchain.setMateFlag();
				//return bestchain;
			}
			if (foundflag == hashfEXACT)
			{ //return exact value
				//if (curlevel == 0)
				bestchain.setScore(hashscore);
				return bestchain;
			}
			else if (foundflag == hashfBETA)
			{ //estimate more than score
				bestchain.setScore(hashscore);
				if (hashscore > alpha)
					alpha = hashscore; //raise minimal estimate but it can be more - up to beta
			}
			else if (foundflag == hashfALPHA)
			{ //estimate less than score
				if (hashscore < alpha)
				{
					//test
					bestchain.setScore(hashscore);
					return bestchain; //bad move even worse than alpha (return -INFINITY)
					//else try to find value between alpha and score
				}
			}
			if (alpha >= beta)
			{
				betacutoffsQ++;
				return bestchain;
			}
		}
#endif

	MoveList ml;
	int move;

	ml.init(curlevelcolor);

#ifdef EVASIVE
	int evasion = 0;

	if (curlevel < maxlevel + 3)
	{
		if (brd.underCheck(curlevelcolor))
		{
			brd.generateMoveList(&ml, curlevelcolor, killers1.getMove(curlevel), killers2.getMove(curlevel), 127);
			if (ml.isMate())
			{
				bestchain.setMateFlag();
				short score = -MAT;
				bestchain.setScore(score);
				brd.clearEnPassant(3 - curlevelcolor);
#ifdef WITHHASHSTOREQ
				HSH.hashStore(curlevelcolor, -1, hashfEXACT, -MAT, 5, brd.z_hash);
#endif
				return bestchain;
			}
			evasion = 1;
		}
	}

	if (!evasion)
	{ //esli korol ne ubegaet

		if (curlevel < maxcheckply) // if level higher than full scan we try to generate shortened movelist
			brd.generateCaptureAndCheckList(&ml, curlevelcolor, killers1.getMove(curlevel), killers2.getMove(curlevel));
		else // and final leap - only exchanges
			brd.generateCaptureList(&ml, curlevelcolor, killers1.getMove(curlevel), killers2.getMove(curlevel));

#ifdef FULLBOARDSCORE
		curchain.setScore(brd.getFullBoardScore(curlevelcolor, alpha, beta));
#endif
#ifndef FULLBOARDSCORE
		curchain.setScore(brd.getQuickBoardScore(curlevelcolor));
#endif
		incSearchNodes(maxlevel, curlevel);
#ifdef WITHHASHSTOREBOARDSCORE
		if(ml.totalmacs==0){
			HSH.hashStore(curlevelcolor, -1, hashfEXACT, curchain.getScore(), 5, brd.z_hash);
		}
#endif
		if (curchain.getScore() >= beta)  //https://www.chessprogramming.org/Quiescence_Search st-pat
		{
#ifdef WITHHASHSTOREQ
			if(maxlevel-curlevel==-1)
			HSH.hashStore(curlevelcolor, -1, hashfBETA, curchain.getScore(), 5, brd.z_hash);
#endif
			//curchain.setScore(curchain.getScore()+1);
			return curchain;
		}
		if (curchain.getScore() > bestchain.getScore())
		{
			bestchain.copyFrom(&curchain);
			if (curchain.getScore() > alpha)
			{
				alpha = curchain.getScore(); 
#ifdef WITHHASHSTOREQ
				hashf = hashfEXACT;
#endif
			}
		}
	}
#endif

#ifndef EVASIVE
	if (curlevel < maxcheckply) // if level higher than full scan we try to generate shortened movelist
		brd.generateCaptureAndCheckList(&ml, curlevelcolor, killers1.getMove(curlevel), killers2.getMove(curlevel));
	else // and final leap - only exchanges
		brd.generateCaptureList(&ml, curlevelcolor, killers1.getMove(curlevel), killers2.getMove(curlevel));
#endif

	//////////////////////////////////////////////////////////////////////////////////////
	/// cycle through possible captures and checks

	brd.clearEnPassant(3 - curlevelcolor); //clear enpassant after wi finishen ml generations
	while (ml.ableTomac())
	{
		move = ml.getNextmac();

		brd.makeCaptureOrMove(move, curlevelcolor);
		curchain.setMoveToHead(move);
		pv.setMoveToDepth(move, curlevel, -INFINITY_SCORE);

		curchain.addToTail(ABCaptureSearch(maxlevel, curlevel + 1, (3 - curlevelcolor), -beta, -alpha));
		curchain.changeMATScoreClimbingUP();

		brd.rollBackCaptureOrMove(move, curlevelcolor);

#ifdef WITHALPHABETA
		if (curchain.getScore() >= beta)
		{
#ifdef WITHHASHSTOREQ
			if(maxlevel-curlevel==-1)
			HSH.hashStore(curlevelcolor, -1, hashfBETA, curchain.getScore(), move, brd.z_hash);
#endif
			betacutoffsQ++;
			return curchain;
		}
#endif
		if (curchain.getScore() > bestchain.getScore())
		{
			bestchain.copyFrom(&curchain);
#ifdef WITHALPHABETA
			if (bestchain.getScore() > alpha)
			{ //soft
				alpha = bestchain.getScore();
#ifdef WITHHASHSTOREQ
				hashf = hashfEXACT;
#endif
			}
#endif
		}
	} //endwhile:

#ifdef WITHHASHSTOREQ
	//if(hashf == hashfEXACT){
	if(maxlevel-curlevel==-1){
	if (bestchain.moves[0])
	{
		HSH.hashStore(curlevelcolor, -1, hashf, bestchain.getScore(), bestchain.moves[0], brd.z_hash);
	}
	else
	{
		HSH.hashStore(curlevelcolor, -1, hashf, bestchain.getScore(), 5, brd.z_hash);
	}
	}
#endif

	return bestchain;
}

MoveChain Tree::ABSearch(char maxlevel, char curlevel, char curlevelcolor, short alpha, short beta, bool null_move)
{
	MoveChain curchain;
	MoveChain bestchain;

	bestchain.setScore(-INFINITY_SCORE);

	curchain.init();
	bestchain.init();

#ifdef WITHHASH
	char hashf = hashfALPHA;

	int hashscore;
	int hashbestmove;
	int hashdepth;
	char foundflag;
	if (curlevel != 0) //��� �� ���������� ���� �� 1 ������
					   //if (1)
		if (HSH.hashProbe(curlevelcolor, maxlevel - curlevel, &hashscore, &hashbestmove, &hashdepth, &foundflag, brd.z_hash))
		{
			bestchain.setMoveToHead(hashbestmove);
//score from hash may be no equl to 3 fold repetition draw
#ifdef WITHHASH
			if (curlevel == 0)
				if (check3FoldRepetition(brd.z_hash))
					bestchain.setScore(STALEMAT);
			if (curlevel == 1)
				if (checkEnemy3FoldRepetition(brd.z_hash))
					bestchain.setScore(STALEMAT); // dont know what is stale mate from enemy point of view. Possibly need to set this value dynamicaly.
#endif

			if ((hashscore > MAT - 30) && ((foundflag == hashfEXACT) || (foundflag == hashfBETA)))
			{
				bestchain.setScore(hashscore);
				bestchain.setMateFlag();
				//return bestchain;
			}
			if ((hashscore < -MAT + 30) && ((foundflag == hashfEXACT) || (foundflag == hashfALPHA)))
			{
				bestchain.setScore(hashscore);
				bestchain.setMateFlag();
				//return bestchain;
			}
			if (foundflag == hashfEXACT)
			{ //return exact value
				bestchain.setScore(hashscore);
				return bestchain;
			}
			else if (foundflag == hashfBETA)
			{ //estimate more than score
				bestchain.setScore(hashscore);
				if (hashscore > alpha)
					alpha = hashscore; //raise minimal estimate but it can be more - up to beta
			}
			else if (foundflag == hashfALPHA)
			{ //estimate less than score
				if (hashscore < alpha)
				{
					//test
					bestchain.setScore(hashscore);
					return bestchain; //bad move even worse than alpha (return -INFINITY)
					//else try to find value between alpha and score
				}
			}
			if (alpha >= beta)
			{
				betacutoffs++;
				return bestchain;
			}
		}
#endif

	MoveList ml;
	int move;

	ml.init(curlevelcolor);

	if (curlevel == 0)
		sortFromPrevItterate(&zerolevelmovelist, &ml);
	else
	{
		brd.generateMoveList(&ml, curlevelcolor, killers1.getMove(curlevel), killers2.getMove(curlevel), maxlevel - curlevel);
		brd.clearEnPassant(3 - curlevelcolor);
	}

	if (ml.isMate())
	{
		bestchain.setMateFlag();
		short score = -MAT;
		bestchain.setScore(score);
#ifdef WITHHASH
		HSH.hashStore(curlevelcolor, 0, hashfEXACT, -MAT, 5, brd.z_hash);
#endif
		return bestchain;
	}
	if (ml.isStaleMate())
	{
		bestchain.setStaleMateFlag();
		short score = STALEMAT;
		bestchain.setScore(score);
		return bestchain;
	}

#ifdef NULLMOVEP
	//just pruning when in midgame to avoid zugzvang
	/*
  if(curlevel<=maxlevel-4){
		if(!ml.mateflag){
			if(brd.number_of_total_figures>10){
            	curchain.addToTail(ABSearch(curlevel+1, curlevel + 1, (3 - curlevelcolor), -beta, 1-beta));
                	if(curchain.getScore()>beta){
                    	return curchain;
                	}
			}
		}
	}
  */
	if (curlevel <= maxlevel - 4)
	{
		if (!null_move)
		{
			if (!ml.mateflag)
			{
				if (brd.number_of_total_figures > 10)
				{
					int reduction = NULLMOVEP_R;
					if ((maxlevel - curlevel) < 6)
						reduction--;

					curchain.addToTail(ABSearch(curlevel + 1, curlevel + 1, (3 - curlevelcolor), -beta, 1 - beta, true));
					curchain.changeMATScoreClimbingUP();
					if (curchain.getScore() >= beta)
					{
						maxlevel = maxlevel - reduction;
					}
				}
			}
		}
	}
	// extended null move reduction
	/*if ( nullMoveAllowed && ...) {
                   R = depth > 6 ? 4 : 3;
                   makeNullMove()
                   score = -zwSearch(1-beta, depth-R-1) // -AlphaBeta (0-beta, 1-beta, depth-R-1)
                   unmakeNullMove();
                   if (score >= beta ) {
                      depth -= 4; // reduce search
                      if ( depth <= 0 )
                          return Evaluate(); // Quiescence
                   }
                }
                // continue search
                */

#endif

	while (ml.ableTomac())
	{
		move = ml.getNextmac();
		brd.makeCaptureOrMove(move, curlevelcolor);
		curchain.setMoveToHead(move);
		pv.setMoveToDepth(move, curlevel, -INFINITY_SCORE);
#ifdef MOVECHAINDEBUG
		//debug remove
		MoveChain pvtest;
		//info depth 5 score cp 56 pv e1c1 f5f8 f3e1 e7d7 b3b7
		//16516 20325 8469 23796 56401
		pvtest.setMoveToDepth(18813, 0, -INFINITY_SCORE);
		pvtest.setMoveToDepth(23508, 1, -INFINITY_SCORE);
		pvtest.setMoveToDepth(18277, 2, -INFINITY_SCORE);
		pvtest.setMoveToDepth(153455, 3, -INFINITY_SCORE);
		pvtest.setMoveToDepth(28607, 4, -INFINITY_SCORE);
		pvtest.setDepth(5);
		if (curlevel == 2)
		{
			if (pv.compare(&pvtest))
			{
				brd.showBoardDebug();
				cout << brd.GetFEN(curlevelcolor) <<endl <<flush;
			}
		}
		//debug end remove
#endif
		if (curlevel < maxlevel)
		{
			curchain.addToTail(ABSearch(maxlevel, curlevel + 1, (3 - curlevelcolor), -beta, -alpha, null_move));
			curchain.changeMATScoreClimbingUP();
		}
		else
		{

#ifdef EXCHANGESEARCH
			curchain.addToTail(ABCaptureSearch(maxlevel, curlevel + 1, (3 - curlevelcolor), -beta, -alpha));
			curchain.changeMATScoreClimbingUP();
#endif
#ifndef EXCHANGESEARCH
#ifdef FULLBOARDSCORE
			curchain.setScore(brd.getFullBoardScore(curlevelcolor, alpha, beta));
#endif
#ifndef FULLBOARDSCORE
			curchain.setScore(brd.getQuickBoardScore(curlevelcolor));
#endif
			incSearchNodes(curlevel, curlevel);
#endif
		}
#ifdef WITHHASH
		if (curlevel == 0)
			if (check3FoldRepetition(brd.z_hash))
				curchain.setScore(STALEMAT);
		if (curlevel == 1)
			if (checkEnemy3FoldRepetition(brd.z_hash))
				curchain.setScore(STALEMAT); // dont know what is stale mate from enemy point of view. Possibly need to set this value dynamicaly.
#endif
		brd.rollBackCaptureOrMove(move, curlevelcolor);

		if (timebreakflag)
			return bestchain;

		if(curlevel==0)
			legal0levelmovecalculatted=true;

		if (curlevel == 0)
		{
			zerolevelmovelist.addScore(curchain.getScore());
			if (curchain.getScore() > MAT - 30)
				matebreakflag = true;
			//if(maxlevel>5){
			curtime = myTime();
			curchain.showMoveChainDebugCurrmoveWOCP(curtime - starttime); //no sense to show cp because it is possible beta pruned
			//curchain.showMoveChainDebugCurrmove(curtime - starttime); //less output - more stable "scid to mac"
			//curchain.showMoveChainDebug();
			//}
		}
#ifdef WITHALPHABETA
		if (curchain.getScore() >= beta)
		{

#ifdef WITHKILLER

			int killer = curchain.moves[0];
			if (!(killer & 0b111000000000000000))
			{
				if (killers1.getMove(curlevel) != killer)
				{
					if (killers2.getMove(curlevel) != killer)
					{
						killers2.setMoveToDepth(killers1.moves[(unsigned char)curlevel], curlevel, killers1.getScore());
						killers1.setMoveToDepth(killer, curlevel, curchain.getScore());
					}
				}
			}
#endif
#ifdef WITHHASH
			HSH.hashStore(curlevelcolor, maxlevel - curlevel, hashfBETA, curchain.getScore(), move, brd.z_hash);
#endif
			betacutoffs++;
			return curchain;
		}
#endif
#ifdef RANDOMIZER //doesn't work because we recieve only first move with exact score. all next moves are beta cut offed
		if ((curchain.getScore() > bestchain.getScore()) || (randomizer && (curchain.getScore() == bestchain.getScore())))
		{
#endif
#ifndef RANDOMIZER
			if (curchain.getScore() > bestchain.getScore())
			{
#endif
				bestchain.copyFrom(&curchain);
			}
			if (bestchain.getScore() > alpha)
			{
				//if(curlevel!=0)// if we want exact scores - not beta pruned we have to disable alpha rise. but it costs to much :(
				alpha = bestchain.getScore();

#ifdef WITHHASH
				hashf = hashfEXACT;
#endif
				if (curlevel == 0)
				{
					curchain.showMoveChainDebug();
					//	    		if (bestchain.getScore() > MAT - 30)
					//		   			return bestchain; //shall we break on first find mat? Possibly not. We could fall on 3fold repetiiton.
				}
			}

		} //endwhile:

#ifdef WITHHASH
		HSH.hashStore(curlevelcolor, maxlevel - curlevel, hashf, bestchain.getScore(), bestchain.moves[0], brd.z_hash);
#endif

		if (curlevel == 0)
		{
			if (bestchain.getScore() < -MAT + 30)
			{
				matebreakflag = true;
			}
		}
#ifdef WITHKILLER

		int killer = bestchain.moves[0];
		if (bestchain.getScore() > alpha)
			if (!(killer & 0b111000000000000000))
			{
				if (killers1.getMove(curlevel) != killer)
				{
					if (killers2.getMove(curlevel) != killer)
					{
						killers2.setMoveToDepth(killers1.moves[(unsigned char)curlevel], curlevel, killers1.getScore());
						killers1.setMoveToDepth(killer, curlevel, bestchain.getScore());
					}
				}
			}
#endif
		return bestchain;
	}

	void Tree::sortFromPrevItterate(MoveList * ml, MoveList * to)
	{
		bool swapflag;
		int pos;
		int tmp;
		while (1)
		{
			pos = 0;
			swapflag = false;
			while (1)
			{
				if (pos > ml->totalmacs - 2)
					break;
				if (ml->macscores[pos] < ml->macscores[pos + 1])
				{
					swapflag = true;
					tmp = ml->macscores[pos];
					ml->macscores[pos] = ml->macscores[pos + 1];
					ml->macscores[pos + 1] = tmp;

					tmp = ml->macs[pos];
					ml->macs[pos] = ml->macs[pos + 1];
					ml->macs[pos + 1] = tmp;
				}
				pos++;
			}
			if (!swapflag)
				break;
		}

		memcpy(to, ml, sizeof(MoveList));
		ml->currentmove = 0;
		ml->currentcapture = 0;
		ml->currentmac = 0;
		ml->currentcheck = 0;
		to->currentmove = 0;
		to->currentcapture = 0;
		to->currentmac = 0;
		to->currentcheck = 0;
	}

	MoveChain Tree::iterate(char type, uint64_t value)
	{

		unsigned int iteratelevel;
		timebreakflag = false;
		matebreakflag = false;

		searchednodes = 0;
		searchednodescheckpoint = CHECKPOINTSTEP;
		starttime = myTime();

		char curlevelcolor = zerolevelcolor;

		MoveChain mc;
		mc.init();
		MoveChain bestmc;
		bestmc.init();

		killers1.initErase();
		killers2.initErase();

		MoveList prevgoodmovelist;
		prevgoodmovelist.init(curlevelcolor);

		prepareZeroLevelMoveList(curlevelcolor);

#ifdef WINDOWSSCALING
		int exp_value, w_high, w_low;
		w_high = 100;
		w_low = 100;
		exp_value = 0;

#endif

		iteratelevel = 0;
#ifdef WITHOPENINGS
		int openmove = opening.getRandomMove(brd.z_hash);
		if (openmove)
		{
			bestmc.setMoveToHead(openmove);
			bestmc.setScore(0);
			cout << endl;
			cout << "info string from opening book" << endl;
			return bestmc;
		}
#endif

		///////////////////////////////////////// maxtime
		if (type == MAXTIME)
		{
			maxtime = value;
			while (!timebreakflag)
			{
				//while (!timebreakflag) {
				legal0levelmovecalculatted = false;
				cout << "info depth " << iteratelevel << " nodes " << searchednodes << endl;
				maxcheckply = iteratelevel + CHECHSEARCHDEPTH + 1;
#ifdef MATESEARCH
				mc = ABSearch(iteratelevel++, 0, curlevelcolor, MAT - 30, MAT + 1, false); // maxlev curlev  color alpha beta
#endif
#ifndef MATESEARCH
#ifdef WINDOWSSCALING
				if (iteratelevel == 0)
				{
					mc = ABSearch(iteratelevel, 0, curlevelcolor, -INFINITY_SCORE, INFINITY_SCORE, false); // maxlev curlev  color alpha beta killer
					exp_value = mc.getScore();
					sortFromPrevItterate(&zerolevelmovelist, &prevgoodmovelist); //save good movelist at prev, in case of window fail
				}
				else
				{
					mc = ABSearch(iteratelevel, 0, curlevelcolor, exp_value - w_low, exp_value + w_high, false); // maxlev curlev  color alpha beta killer
				}
				if (mc.getScore() >= (exp_value + w_high))
				{
					w_high += w_high;
					cout << "info string fail high window. increasing " << exp_value << ":" << mc.getScore() << ":" << w_high << endl;
#ifdef WITHHASH
					HSH.flushNotExact();
#endif
					sortFromPrevItterate(&prevgoodmovelist, &zerolevelmovelist);
				}
				else if (mc.getScore() <= (exp_value - w_low))
				{
					//if(!false1level){ // false positive window decrease if we have not finished first move at new level and though have -INFINiTY SCORE
					w_low += w_low;
					cout << "info exp value:" << exp_value << " string fail low window. increasing " << w_low << endl;
					//}
#ifdef WITHHASH
					HSH.flushNotExact();
#endif
					sortFromPrevItterate(&prevgoodmovelist, &zerolevelmovelist);
				}
				else
				{
					iteratelevel++;
					sortFromPrevItterate(&zerolevelmovelist, &prevgoodmovelist);
				}
#endif
#ifndef WINDOWSSCALING
				mc = ABSearch(iteratelevel++, 0, curlevelcolor, -INFINITY_SCORE, INFINITY_SCORE, false); // maxlev curlev  color alpha beta
#endif

#endif //matesearch
				if (legal0levelmovecalculatted)
					bestmc.copyFrom(&mc);
				cout << "info string Final at level: " << iteratelevel << endl;
				bestmc.showMoveChainDebug();
			}

			//////////////////////// maxlevel
		}
		else if (type == MAXLEVEL)
		{
			maxtime = 100000000;
			while ((iteratelevel <= value) && (!matebreakflag))
			{ //find first mate. depth could be not minimal
				legal0levelmovecalculatted = false;
				cout << "info depth " << iteratelevel << " nodes " << searchednodes << endl;
				maxcheckply = iteratelevel + CHECHSEARCHDEPTH + 1;
#ifdef MATESEARCH
				mc = ABSearch(iteratelevel++, 0, curlevelcolor, MAT - 30, MAT + 1, false); // maxlev curlev  color alpha beta
#endif
#ifndef MATESEARCH
#ifdef WINDOWSSCALING
				if (iteratelevel == 0)
				{
					mc = ABSearch(iteratelevel, 0, curlevelcolor, -INFINITY_SCORE, INFINITY_SCORE, false); // maxlev curlev  color alpha beta
					exp_value = mc.getScore();
					sortFromPrevItterate(&zerolevelmovelist, &prevgoodmovelist); //save good movelist at prev, in case of window fail
				}
				else
				{
					mc = ABSearch(iteratelevel, 0, curlevelcolor, exp_value - w_low, exp_value + w_high, false); // maxlev curlev  color alpha beta
				}
				if (mc.getScore() >= (exp_value + w_high))
				{
					w_high += w_high;
					cout << "info string fail high window. increasing " << exp_value << ":" << mc.getScore() << ":" << w_high << endl;
#ifdef WITHHASH
					HSH.flushNotExact();
#endif
					sortFromPrevItterate(&prevgoodmovelist, &zerolevelmovelist);
				}
				else if (mc.getScore() <= (exp_value - w_low))
				{
					w_low += w_low;
					cout << "info exp value:" << exp_value << " string fail low window. increasing " << w_low << endl;
#ifdef WITHHASH
					HSH.flushNotExact();
#endif
					sortFromPrevItterate(&prevgoodmovelist, &zerolevelmovelist);
				}
				else
				{
					iteratelevel++;
					sortFromPrevItterate(&zerolevelmovelist, &prevgoodmovelist);
				}
#endif
#ifndef WINDOWSSCALING
				mc = ABSearch(iteratelevel++, 0, curlevelcolor, -INFINITY_SCORE, INFINITY_SCORE, false); // maxlev curlev  color alpha beta
#endif

#endif
				if (legal0levelmovecalculatted || matebreakflag)
					bestmc.copyFrom(&mc);
				cout << "info string Final at level: " << iteratelevel - 1 << endl;
				bestmc.showMoveChainDebug();
			}
		}
		stoptime = myTime();
		return bestmc;
	}

	bool Tree::loadFEN(const char *FEN)
	{
		brd.initClear();

		int n = 0;
		char chr;
		char figure;
		char pole[2];
		for (char y = '8'; y > '0'; y--)
			for (char x = 'a'; x < 'i'; x++)
			{
				chr = FEN[n++];
				if ((chr > '0') && (chr < '9'))
					x = x + (chr - '0' - 1);
				if (((chr > 'a') && (chr < 'z')) || ((chr > 'A') && (chr < 'Z')))
				{
					figure = chr;
					pole[0] = x;
					pole[1] = y;
					brd.setFigure(figure, pole);
				}
				if (chr == '/')
					x--;
			}
		n++;
		switch (FEN[n++])
		{
		case 'b':
			zerolevelcolor = BLACK;
			break;
		case 'w':
			zerolevelcolor = WHITE;
			break;
		default:
			cout << "Bad FEN " << FEN << endl;
			return false;
		}
		n++;
		brd.wOOrokirovka = true;
		brd.wOOOrokirovka = true;
		brd.bOOrokirovka = true;
		brd.bOOOrokirovka = true;
		while (FEN[n])
		{
			switch (FEN[n++])
			{
			case '-':
				break;
			case ' ':
				break;
			case 'K':
				brd.wOOrokirovka = false;
				continue;
			case 'Q':
				brd.wOOOrokirovka = false;
				continue;
			case 'k':
				brd.bOOrokirovka = false;
				continue;
			case 'q':
				brd.bOOOrokirovka = false;
				continue;
			default:
				cout << "Bad FEN " << FEN << endl;
				return false;
			}
			break;
		}
		return true;
	}

	void Tree::showStats(char depth, char seldepth)
	{
		curtime = myTime();
		if (curtime == starttime)
			curtime++;
#ifdef WITHHASH
		cout << "info depth " << (int)depth << " seldepth " << (int)seldepth << " nodes " << searchednodes << " nps " << (BITBOARD)1000 * searchednodes / (curtime - starttime) << " hashfull " << (BITBOARD)1000 * (HSH.hash_full >> 10) / (HSH.htsize << 10) << " time " << (BITBOARD)curtime - (BITBOARD)starttime << endl  << flush;
#endif
#ifndef WITHHASH
		cout << "info depth " << (int)depth << " seldepth " << (int)seldepth << " nodes " << searchednodes << " nps " << (long long)1000 * searchednodes / (curtime - starttime) << " hashfull "
			 << "0"
			 << " time " << (long)curtime - starttime << endl << flush;
#endif
	}

	void Tree::incSearchNodes(char depth, char seldepth)
	{
		searchednodes++;
		if (!(searchednodes % CHECKPOINTSTEPINFO))
		{
			showStats(depth, seldepth);
		}
		if (searchednodes == searchednodescheckpoint)
		{
			searchednodescheckpoint += CHECKPOINTSTEP;
			//time(&curtime);
			curtime = myTime();
#ifdef RANDOMIZER
			randomizer = 1 - randomizer;
			randomizer ^= curtime & 1UL;
#endif
			BITBOARD tcheck = curtime - starttime;
			if ((tcheck >= maxtime) && !timebreakflag)
			{ // -10 msec - time to finish break loop
				timebreakflag = true;
				cout << "info string tbrf set" << endl;
			}

		}
	}

	bool Tree::check3FoldRepetition(BITBOARD hash)
	{
#ifndef FOLD3REPEAT
		return false;
#endif

		int gh_dpth = game_history.getDepth();

		if (gh_dpth < 8)
			return false;

		int repeats = 0;

		for (int i = gh_dpth - 1; i >= 0; i--)
		{
			if (hash == game_history.getHash(i))
			{
				repeats++;
				if (repeats == 2)
					return true;
			}
		}
		return false;
	}

	bool Tree::checkEnemy3FoldRepetition(BITBOARD hash)
	{
#ifndef FOLD3REPEAT
		return false;
#endif

		int gh_dpth = game_history.getDepth();

		if (gh_dpth < 7)
			return false;

		int repeats = 0;

		for (int i = gh_dpth - 1; i >= 0; i--)
		{
			if (hash == game_history.getHash(i))
			{
				repeats++;
				if (repeats == 2)
					return true;
			}
		}
		return false;
	}

	void Tree::prepareZeroLevelMoveList(char color)
	{
		zerolevelmovelist.init(color);
		brd.generateMoveList(&zerolevelmovelist, color, 0, 0, 0);
		for (int i = 0; i < 500; i++)
			zerolevelmovelist.macscores[i] = -INFINITY_SCORE;
	}