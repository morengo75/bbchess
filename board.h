#ifndef BOARD
#define BOARD

#include <string>
using namespace std;

#include "utils.h"
#include "defenitions.h"
#include "movelist.h"
#include "data.h"
#include "bitboards.h"
#include "movechain.h"

extern BITBoards BB;

class Board {
    friend class Hash;

  public:

     Board();

    BITBOARD wb_figures;
    BITBOARD w_figures;
    BITBOARD b_figures;
    BITBOARD w_pawns;
    BITBOARD w_knights;
    BITBOARD w_bishops;
    BITBOARD w_rooks;
    BITBOARD w_queens;
    BITBOARD w_kings;
    BITBOARD b_pawns;
    BITBOARD b_knights;
    BITBOARD b_bishops;
    BITBOARD b_rooks;
    BITBOARD b_queens;
    BITBOARD b_kings;

    BITBOARD w_enpassant;
    BITBOARD b_enpassant;

    BITBOARD z_hash;

  private:
    int number_of_white_figures;

    int number_of_white_pawns;
    int number_of_white_rooks;
    int number_of_white_knights;
    int number_of_white_bishops;
    int number_of_white_queens;

    int number_of_black_figures;

    int number_of_black_pawns;
    int number_of_black_rooks;
    int number_of_black_knights;
    int number_of_black_bishops;
    int number_of_black_queens;
  public:
    int number_of_total_figures;

  public:
    bool wOOrokirovka;
    bool bOOrokirovka;
    bool wOOOrokirovka;
    bool bOOOrokirovka;

    int wkingmoved;
    int bkingmoved;

  public:
    void initClear();
    void initDefault();
    bool setFigure(char figure, const char *pole);
    void showBoardDebug() const;
    void generateMoveList(MoveList * ml, char color, int killer1, int killer2, char req_hashdepth);
    void generatePLMoveList(MoveList * ml, char color);
    void generatePLMoveCount(MoveList * ml, char color);
    void generatePLPLMoveCount(MoveList * ml, char color); //extra light version
    void generateCaptureList(MoveList * ml, char color, int killer1, int killer2);
    void generateCaptureAndCheckList(MoveList * ml, char color, int killer1, int killer2);
    short getQuickBoardScore(char color);
    short getFullBoardScore(char color, int alpha, int beta);
    void testing_clone(Board *brd);
    bool testing_compare(Board *brd);

    BITBOARD slonyachafunk(unsigned char pos);
    BITBOARD ladyachafunk(unsigned char pos);
    BITBOARD kvinskafunk(unsigned char pos);

    BITBOARD RooksConnectedV(unsigned char pos);


  private:
    char positionOfBlackFigureOn(unsigned char to);
    char positionOfWhiteFigureOn(unsigned char to);

    BITBOARD pawnMoveFuncFrom2Line(unsigned char pos);
    BITBOARD pawnMoveFuncFrom7Line(unsigned char pos);
  public: //test only
    void makeMove(int move, char color);
    void rollBackMove(int move, char color);
    void makeCapture(int move, char color);
    void rollBackCapture(int move, char color);
  private:
    short getQueenBonus(char color);
    short getKingBonus(char color);
    short getPawnsBonus(char color);
    short getKnightsBonus(char color);
    short getRooksBonus(char color);
    short getBishopsBonus(char color);
    short getMobilityBonus(MoveList * ml);
    short getMobilityBonus2(char color);

    void addFigureCapturesAndMoves(char color,BITBOARD bb, BITBOARD ourfrom, MoveList *ml, char req_hashdepth);
    void addFigureCaptures(char color,BITBOARD bb, BITBOARD ourfrom, MoveList *ml, char req_hashdepth);
    void addFigureCapturesAndChecks(char color,BITBOARD bb, BITBOARD ourfrom, MoveList *ml, char req_hashdepth);

  public:
    void makeCaptureOrMove(int move, char color);
    void rollBackCaptureOrMove(int move, char color);
    bool underCheck(char color);
    bool isSQUnderAtack(char color,unsigned char pos);
    bool badQuickMatCheck(char color);
    char loadFEN(const char *FEN);
    bool compareToFENDebug(char *FEN);
    int mnemToMove(const char *mnem);
    int halfmnemToMove(string mnem,char color);
    void clearEnPassant(char color);
    char* GetFEN(char curlevelcolor) const;

};
#endif
