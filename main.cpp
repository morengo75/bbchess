#include <stdio.h>
#include <ctime>

#include "main.h"
#include "utils.h"
#include "movelist.h"
#include "engine.h"
#include "opening.h"

#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <fstream>

//#include <pthread.h>
#ifdef THREADS
#include <thread>
#endif

using namespace std;

BITBoards BB;
#ifdef WITHHASH
Hash HSH;
#endif

GameHistory game_history;
Tree tr;
MoveChain mc;
BbcEngine engine;
Opening opening;

int lazytest = 0;

int main()
{

	BB.init();

#ifdef WITHHASH
	HSH.init(4);
#ifdef WITHOPENINGS
	opening.init("openings500.book");
#endif
#endif

#ifdef THREADS
	int uci_parm;
	pthread_t t_uci;
	pthread_create(&t_uci, NULL, uci, &uci_parm);

	if (pthread_join(t_uci, NULL))
	{
		cout << "Error joining thread" << endl;
		return 2;
	}
	cout << "thread joined";
	return 1;
#endif

	uci(0);
	//solve("5k2/ppp2r1p/2p2ppP/8/2Q5/2P1bN2/PP4P1/1K1R4 w - - 0 1 ",MAXLEVEL,16); //hard one
	//solve("r7/3b1pk1/6p1/3Pp2p/2P4P/p4B2/5PP1/2R3K1 b - -",MAXTIME,60000);
	//  info depth 6 score cp -9992 pv f8f5 e3h6 f5f4 ->>h6f4<<- h8g8 f4f7 problem
	//solve("rnbqk1nr/pppp1ppp/4p3/8/1bPP4/2N5/PP2PPPP/R1BQKBNR b KQkq - 2 3",MAXTIME,1000);
	//test();
	//perft(); //several mate searches
	//perft2();
	//bestMoveTest("epds/bk.test.epd",MAXTIME,20*1000);
	//bestMoveTest("epds/sbd.test.epd",MAXTIME, 0.2*1000);
	//bestMoveTest("epds/sbd.test.epd",MAXTIME, 0.5*1000);
	//bestMoveTest("epds/rooks-open-files-diagonals.epd",MAXLEVEL, 5);
	//bestMoveTest("epds/STS5.0BishopvsKnight.epd",MAXTIME,0.2*1000);
	//bestMoveTest("epds/STS3.1-KnightOutposts-Centralization-Repositioning.epd",MAXTIME, 0.5*1000);
	//bestMoveTest("epds/markov.endgame.epd",MAXTIME, 5*1000);
	//bestMoveTest("epds/markov.endgame.hard.epd",MAXLEVEL,9);
	//bestMoveTest("epds/STS9.0Advancementofa-b-cPawns.epd",MAXTIME,0.2*1000);
	//cutOffTest("epds/STS5.0BishopvsKnight.epd",MAXLEVEL,6); //bullet

	return 1;
}

void solve(const char *epd, int testtype, uint64_t value)
{

	BITBOARD curtime, starttime;
	long nodes = 0;

	engine.SetEPD(epd);
	//engine.SetEPD("8/p7/1p6/2q5/8/5b2/1k6/5K2 b - - 2 2");

	engine.PrintBoard();

	BbcEngineArgs ea;
	if (testtype == MAXLEVEL)
		ea.Set(value, 0, 0, 0, 0, 0, 0);
	if (testtype == MAXTIME)
		ea.Set(0, 0, value, 0, 0, 0, 0);
	starttime = myTime();
	engine.MyGo(&ea);
	nodes = engine.tr.searchednodes;

	curtime = myTime();
	cout << "info string time " << (BITBOARD)curtime - starttime << endl;
	cout << "searched nodes " << nodes << endl;

	//tr.brd.loadFEN("7k/2p3p1/r1q1p2p/5p2/2PP4/2Q5/1P3PPP/4R1K1 w - -");
	//tr.loadFEN("4k2r/2pn1p2/5n2/1P1p1bp1/3Pp2p/B1N1P2P/2P1BPP1/5RK1 w K");
	//tr.loadFEN("r1b2rk1/p1p1qp1p/1p2p2n/4p1p1/1PP5/P1NQ1N1P/5PP1/R4RK1 w - - 0 16  "); //exchange search problem at 10000
	//tr.loadFEN("position startpos moves e2e4 b8c6 d2d4 e7e6 f1d3 c6d4 c2c3 d4c6 g1f3 g7g6 e1g1 f8h6 c1h6 g8h6 c3c4 e8g8 b1c3 d7d6 d1e2 c6b4 a2a3 b4d3 e2d3 g6g5 h2h3 d8e7 b2b4 b7b6 e4e5");
	//tr.loadFEN("6k1/pp1b3p/1n1Pr2q/B1p2p2/2P2Pp1/P3r3/3QB1K1/4RR2 b - - 1 32 "); // problem slow mate with exchange search
	//tr.loadFEN("5k2/6pp/p1qN4/1p1p4/3P4/2PKP2Q/PP3r2/3R4 b - - bm Qc4+;"); //quick
	/////tr.loadFEN("8/8/2B1N3/3rp2K/4k3/7Q/2r3Pn/1b1N4 w - - bm ;"); //mat 7 22 sec
	//tr.loadFEN("k7/1P6/8/8/8/8/8/8 w - -"); //queice test. mate
	//tr.loadFEN("rk5r/ppp1Q1pp/2n5/3b4/5PP1/1N3n2/PP1p3P/RK3BBR w K"); //white dont see mate
	//tr.loadFEN("r6r/kpp1Q1pp/2n5/3b4/5PP1/1N3n2/PP1p3P/RK3B1R w - - 0 2");
	//position fen qrbbknnr/pppppppp/8/8/8/8/PPPPPPPP/QRBBKNNR w KQkq - 0 1 moves b2b4 g8f6 f2f4  - false mate movetime 6000 //solved - wrong hash sort rokirowka
	//tr.loadFEN("qrbbkn1r/pppppppp/5n2/8/1P3P2/8/P1PPP1PP/QRBBKNNR b KQkq");
	//position fen brkbqrnn/pppppppp/8/8/8/8/PPPPPPPP/BRKBQRNN w KQkq - 0 1 moves g1f3 b7b5 d2d3 a8f3 e2f3 e7e5 e1e3 b8b6 b2b4 h8g6 h1g3 f7f5 g3h1 g8e7 b1b3 e7d5 e3d2 h7h6 c1b1 d8g5
	//tr.loadFEN("2q1rr1k/3bbnnp/p2p1pp1/2pPp3/PpP1P1P1/1P2BNNP/2BQ1PRK/7R b - -"); //premature end with -INFINITY
	//tr.loadFEN("2b1k2r/2p2ppp/1qp4n/7B/1p2P3/5Q2/PPPr2PP/R2N1R1K b k - bm O-O; id \"sbd.003\";");

	//tr.loadFEN("k7/7R/6R1/8/8/8/8/K7 w - - bm ;"); //test evasive mate

	//tr.loadFEN("r2qnrnk/p2b2b1/1p1p2pp/2pPpp2/1PP1P3/PRNBB3/3QNPPP/5RK1 w - - bm f4; id \"BK.24\";"); //bc problem

	//tr.brd.showBoardDebug();

	//int maxtime=10000;
	//int maxlevel=20;

	//mc=tr.iterate(MAXTIME,maxtime);
	//mc=tr.iterate(MAXLEVEL,maxlevel);

	//  tr.showStats(mc.getDepth(), mc.getDepth());
	//#ifdef WITHHASH
	//	    cout << "Hash Hits:" << HSH.hashHit << " Hash Collisions:" << HSH.hashCollisions << " Hash Probes:" << HSH.hashProbes << endl;
	//#endif
}

void perft(void)
{
	std::vector<std::string> fens{
		//23.9 mln nodes. 59 secs.
		//final: 1mln nodes. subsecond time
		"6k1/pp2p2p/3b2p1/7q/3p4/1P1P3P/P1N2rP1/1Q3R1K b - - 0 1",
		"Q7/5pk1/4q1p1/3p1p2/2nP1Q1P/5P1K/8/8 w - - 1 0",
		"4r1r1/6R1/p1n3b1/3R1p1p/kpp1P2P/4BP2/PPP1N1P1/2K5 w - - 1 0",
		"4r2k/r4p1p/3R1p2/p1q1P3/2p3Q1/Pn3NP1/5P1P/5RK1 w - - 1 0",
		"2k1r3/5pp1/p2r3p/1p3Pb1/6P1/PPP1q3/R3N1QP/4K2R b - - 0 1",
	};
	int maxlevel = 16;
	BITBOARD curtime, starttime;
	starttime = myTime();
	long nodes = 0;
	long betacutoffsQ = 0;
	long betacutoffs = 0;

	for (unsigned int i = 0; i < fens.size(); i++)
	{
		engine.SetEPD(fens[i].c_str());
		engine.PrintBoard();
		cout << fens[i] << endl;

		BbcEngineArgs ea;
		ea.Set(maxlevel, 0, 0, 0, 0, 0, 0);
		engine.MyGo(&ea);
		nodes += engine.tr.searchednodes;
		betacutoffsQ += engine.tr.betacutoffsQ;
		betacutoffs += engine.tr.betacutoffs;

		curtime = myTime();
		cout << "info string time " << (BITBOARD)curtime - starttime << endl;
		cout << "searched nodes " << nodes << endl;
		cout << "beta cut offs Q search " << betacutoffsQ << endl;
		cout << "beta cut offs AB search " << betacutoffs << endl;
#ifdef WITHHASH
		cout << "hash probes " << HSH.hashProbes << endl;
#endif
	}
}

void perft2(void)
{
	bestMoveTest("epds/markov.endgame.hard.epd", MAXLEVEL, 9);
}

void bestMoveTest(const char *filename, int testtype, uint64_t value)
{
	std::vector<std::string> fens;

	std::ifstream fens_file(filename);
	std::string line;

	while (std::getline(fens_file, line))
	{
		std::string new_line;
		new_line = line + "\n";
		fens.push_back(new_line);
	}

	bool fails[fens.size()];

	int found = 0;
	int total = 0;
	MoveChain best_mc;

	BITBOARD curtime, starttime;
	starttime = myTime();
	long nodes = 0;

	long betacutoffsQ = 0;
	long betacutoffs = 0;

	for (unsigned int i = 0; i < fens.size(); i++)
	{
		cout << fens[i].c_str();
		engine.SetEPD(fens[i].c_str());
		engine.PrintBoard();
		//cout << fens[i] << endl;
		//int maxlevel=20;

		if (testtype == MAXTIME)
		{
			BbcEngineArgs ea;
			ea.Set(0, 0, value, 0, 0, 0, 0);
			engine.MyGo(&ea);
		}
		else if (testtype == MAXLEVEL)
		{
			BbcEngineArgs ea;
			ea.Set(value, 0, 0, 0, 0, 0, 0);
			engine.MyGo(&ea);
		}

		nodes += engine.tr.searchednodes;
		betacutoffsQ += engine.tr.betacutoffsQ;
		betacutoffs += engine.tr.betacutoffs;

		best_mc.copyFrom(&engine.best_mc);

		if (engine.expected_bm == best_mc.moves[0])
		{
			cout << "correct " << engine.epd_id << endl;
			found++;
			fails[i] = false;
		}
		else
		{
			cout << "fail " << engine.epd_id << endl;
			fails[i] = true;
		}
		total++;
	}
	for (unsigned int i = 0; i < fens.size(); i++)
	{
		if (fails[i])
			cout << fens[i].c_str();
	}

	cout << "total: " << total << " correct " << found << endl;

	curtime = myTime();
	cout << "info string time " << (BITBOARD)curtime - starttime << endl;
	cout << "searched nodes " << nodes << endl;

	cout << "beta cut offs Q search " << betacutoffsQ << endl;
	cout << "beta cut offs AB search " << betacutoffs << endl;
#ifdef WITHHASH
	cout << "hash probes " << HSH.hashProbes << endl;
	cout << "hash hits " << HSH.hashHit << endl;
#endif
}

void cutOffTest(const char *filename, int testtype, uint64_t value)
{
	std::vector<std::string> fens;

	std::ifstream fens_file(filename);
	std::string line;

	while (std::getline(fens_file, line))
	{
		std::string new_line;
		new_line = line + "\n";
		fens.push_back(new_line);
	}

	bool fails[fens.size()];

	int lazystart = 50;
	int lazyend = 400;
	int lazystep = 25;

	int correct[lazyend + 1];
	int time[lazyend + 1];
	int nds[lazyend + 1];

	MoveChain best_mc;

	for (lazytest = lazystart; lazytest <= lazyend; lazytest += lazystep)
	{

		BITBOARD curtime, starttime;
		starttime = myTime();
		long nodes = 0;
		int found = 0;
		int total = 0;
#ifdef WITHHASH
		HSH.flush();
#endif

		for (unsigned int i = 0; i < fens.size(); i++)
		{
			cout << fens[i].c_str();
			engine.SetEPD(fens[i].c_str());
			engine.PrintBoard();
			//cout << fens[i] << endl;
			//int maxlevel=20;

			if (testtype == MAXTIME)
			{
				BbcEngineArgs ea;
				ea.Set(0, 0, value, 0, 0, 0, 0);
				engine.MyGo(&ea);
			}
			else if (testtype == MAXLEVEL)
			{
				BbcEngineArgs ea;
				ea.Set(value, 0, 0, 0, 0, 0, 0);
				engine.MyGo(&ea);
			}

			nodes += engine.tr.searchednodes;

			best_mc.copyFrom(&engine.best_mc);

			if (engine.expected_bm == best_mc.moves[0])
			{
				cout << "correct " << engine.epd_id << endl;
				found++;
				fails[i] = false;
			}
			else
			{
				cout << "fail " << engine.epd_id << endl;
				fails[i] = true;
			}
			total++;
		}
		for (unsigned int i = 0; i < fens.size(); i++)
		{
			if (fails[i])
				cout << fens[i].c_str();
		}

		curtime = myTime();

		correct[lazytest] = found;
		time[lazytest] = (BITBOARD)curtime - starttime;
		nds[lazytest] = nodes;
	}
	for (lazytest = lazystart; lazytest <= lazyend; lazytest += lazystep)
	{
		cout << "lazy margine " << lazytest << endl;
		cout << "correct " << correct[lazytest] << endl;
		cout << "info string time " << time[lazytest] << endl;
		cout << "searched nodes " << nds[lazytest] << endl
			 << endl;
	}
}

void play2comp(void)
{
	int score = 0;
	char color = WHITE;

	tr.initDefault(color);

	int maxtime = 5;
	while (score < MAT - 30)
	{
		tr.brd.showBoardDebug();
		mc = tr.iterate(MAXTIME, maxtime);
		score = mc.getScore();
		tr.brd.makeCaptureOrMove(mc.moves[0], color);
		color = 3 - color;
		tr.zerolevelcolor = color;
	}
}
void playVsComp(void)
{
	char mnem[100];
	int mv;
	char color = BLACK;
	int maxtime = 5;

	tr.initDefault(BLACK);

	while (1)
	{
		tr.brd.showBoardDebug();
		do
		{
			scanf("%s", mnem);
			mv = tr.brd.mnemToMove(mnem);
			if (!mv)
				cout << "illegal move" << endl;
		}

		while (!mv);
		tr.brd.makeCaptureOrMove(mv, 3 - color);
		tr.brd.showBoardDebug();
		mc = tr.iterate(MAXTIME, maxtime);
		tr.brd.makeCaptureOrMove(mc.moves[0], color);
		tr.showStats(mc.getDepth(), mc.getDepth());
	}
}

void *uci(void *uci_parm)
{
	engine.Initialize(BLACK);

	string Line; //to read the command given by the GUI
	unsigned int ps;

	cout.setf(ios::unitbuf); // Make sure that the outputs are sent straight away to the GUI

	//cin.rdbuf()->setbuf(NULL, 0);
	//cin.setf(ios::unitbuf);
	//std::ios::sync_with_stdio(false);
	cin.sync_with_stdio(false);

	while (getline(cin, Line))
	{
		if (Line == "uci")
		{
			cout << "id name " << engine.GetEngineName() << endl;
			cout << "id author " << engine.GetAuthorName() << endl;
			cout << "id version " << engine.GetEngineVersion() << endl;
			cout << "id country " << engine.GetCountryName() << endl;
#ifdef MATESEARCH
			cout << "info string matesearch " << endl;
#endif
#ifdef WITHHASH
			cout << "info string with hash " << endl;
#endif
#ifdef WITHHASHSORT
			cout << "info string with hashsort " << endl;
#endif
#ifdef WITHALPHABETA
			cout << "info string with alphabeta " << endl;
#endif
#ifdef WITHKILLER
			cout << "info string with killer " << endl;
#endif
#ifdef FULLBOARDSCORE
			cout << "info string with fullboardscore " << endl;
#endif
#ifdef EXCHANGESEARCH
			cout << "info string with qsearch " << endl;
#endif
#ifdef MVVLVA
			cout << "info string with mvvlva " << endl;
#endif
#ifdef MOBILITYBONUS
			cout << "info string with mobylitybonus " << endl;
#endif
#ifdef LAZYCUTOFF
			cout << "info string with lazycutoff " << endl;
#endif
#ifdef WINDOWSSCALING
			cout << "info string with windowsscaling " << endl;
#endif
#ifdef EVASIVE
			cout << "info string with evasing " << endl;
#endif

#ifdef NULLMOVEP
			cout << "info string with null move pruning " << endl;
#endif

#ifdef HEAVY_EVAL
			cout << "info string with heavy eval " << endl;
#endif

#ifdef FOLD3REPEAT
			cout << "info string with 3 fold repetition " << endl;
#endif

#ifdef RANDOMIZER
			cout << "info string with move randomizer " << endl;
#endif

#ifdef WITHOPENINGS
			cout << "info string with opening book " << endl;
#endif

			cout << "uciok" << endl
				 << flush;
		}
		else if (Line == "quit")
		{
			cout << "Bye Bye" << endl
				 << flush;
			engine.tr.timebreakflag = true;
			break;
		}
		else if (Line == "isready")
		{
			cout << "readyok" << endl
				 << flush;
		}
		else if (Line == "ucinewgame")
		{
			engine.Initialize(BLACK);
		}
		else if (Line == "showboard")
		{
			engine.PrintBoard();
		}
		else if (Line == "showfen")
		{
			cout << engine.GetFEN() << endl
				 << flush;
		}

		if (Line.substr(0, 17) == "position startpos")
		{ //position startpos moves e2e4 e7e5 g1f3 b8c6 f1c4 f8c5
			engine.Initialize(WHITE);
			if (Line.find("moves ") != string::npos)
			{
				Line = Line.substr(Line.find("moves ") + 6);
				string move;
				while (Line.length())
				{
					if (!splitStringSpace(&Line, &move))
						break;
					engine.MakeMove(move.c_str());

					///////////////////////////////// illegal moves test
					//engine.PrintBoard();
					//cout << Line[ps-5] <<Line[ps-4] <<Line[ps-3] <<Line[ps-2] <<" ";
					//showBITBOARD(engine.tr.brd.w_enpassant);
					//getchar();
					///////////////////////////////////////////////////
				}
			}
		}
		else if (Line.substr(0, 13) == "position fen ")
		{ //position fen qrkrbbnn/pppppppp/8/8/8/8/PPPPPPPP/QRKRBBNN w KQkq - 0 1 moves d2d4 h8g6 g1f3 g8f6 e2e3 d7d6 f1d3 e8c6 f3g5 c6g2 g5f7 g2h1 f7d8 c8d8 e1b4 h1e4 d3e4 f6e4 f2f3 e4g5 f3f4 g5f3 h2h3 e7e6 b2b3 b7b5 a1c3 f8e7 b4a5 b8c8 d4d5 e6e5 c3d3 e5f4 e3f4 f3h4 d3e4 d8e8 c2c4 b5c4 b3c4 c7c6 c1b2 c6d5 c4d5 a8b7 b2a1 b7d7 b1b3 h4f5 b3c3 c8c3 a5c3 d7c8 a1b2 c8d7 d1c1 a7a5 c3a5 f5g3 e4d3 d7h3 d3f3 g6h4 f3b3 h4g2 b3f3 g2h4 f3d3 h4g2 d3f3 e7f6 a5c3 g2h4 f3e3 e8f7 c3f6 f7f6 c1g1 h4g2 e3f3 g2f4 f3f4 g3f5 f4g5 f6f7 g1c1 h3h2 b2b1 g7g6 c1c7 f7e8 c7c8 e8f7 c8c7 f7e8 c7c8 e8f7
			engine.Initialize(WHITE);
			ps = 13;
			engine.SetFEN(&Line[ps]); //fen
			if (Line.find("moves ") != string::npos)
			{
				Line = Line.substr(Line.find("moves ") + 6);
				string move;
				while (Line.length())
				{
					if (!splitStringSpace(&Line, &move))
						break;
					engine.MakeMove(move.c_str());
					//cout << move <<endl;
					//engine.PrintBoard();
					//getchar();
				}
			}
			//temp arena patch. need to change to rook_moved
			engine.tr.brd.bOOOrokirovka = true;
			engine.tr.brd.bOOrokirovka = true;
			engine.tr.brd.wOOOrokirovka = true;
			engine.tr.brd.wOOrokirovka = true;
		}
		else if (Line == "stop")
		{
			engine.tr.timebreakflag = true;
		}
		else if (Line.substr(0, 3) == "go ")
		{
			if (Line.substr(0, 12) == "go movetime ")
			{
				int movetime;
				sscanf(&Line[12], "%d", &movetime);
				cout << "info string start with movetime " << movetime << endl;
				BbcEngineArgs ea;
				ea.Set(0, 0, movetime * 0.995, 0, 0, 0, 0);
#ifdef THREADS
				std::thread engn(&BbcEngine::MyGo, &engine, &ea);
				engn.detach();
#endif
#ifndef THREADS
				engine.MyGo(&ea);
#endif
			}
			else if ((Line.find("wtime") != string::npos) && (engine.WhiteToMove()))
			{
				long movetime, enemymovetime, inct = 0, einct = 0;

				ps = Line.find("wtime") + 6;
				sscanf(&Line[ps], "%lu", &movetime);
				ps = Line.find("btime") + 6;
				sscanf(&Line[ps], "%lu", &enemymovetime);

				ps = Line.find("winc");
				if (ps != string::npos)
					sscanf(&Line[ps + 5], "%lu", &inct);
				ps = Line.find("binc");
				if (ps != string::npos)
					sscanf(&Line[ps + 5], "%lu", &einct);

				cout << "info string start with movetime " << engine.CalculateMoveTime(movetime, enemymovetime, inct, einct) << endl;
				BbcEngineArgs ea;
				ea.Set(0, 0, engine.CalculateMoveTime(movetime, enemymovetime, inct, einct), 0, 0, 0, 0);
#ifdef THREADS
				//pthread_t t_eng;
				//typedef void * (*THREADFUNCPTR)(void *);
				//pthread_create(&t_eng, NULL, (THREADFUNCPTR) &engine.MyGo, &ea);
				std::thread engn(&BbcEngine::MyGo, &engine, &ea);
				engn.detach();
//first.join();
#endif
#ifndef THREADS
				engine.MyGo(&ea);
#endif
			}
			else if ((Line.find("btime") != string::npos) && (!engine.WhiteToMove()))
			{
				long movetime, enemymovetime, inct = 0, einct = 0;

				ps = Line.find("btime") + 6;
				sscanf(&Line[ps], "%lu", &movetime);
				ps = Line.find("wtime") + 6;
				sscanf(&Line[ps], "%lu", &enemymovetime);

				ps = Line.find("binc");
				if (ps != string::npos)
					sscanf(&Line[ps + 5], "%lu", &inct);
				ps = Line.find("winc");
				if (ps != string::npos)
					sscanf(&Line[ps + 5], "%lu", &einct);

				cout << "info string start with movetime " << engine.CalculateMoveTime(movetime, enemymovetime, inct, einct) << endl;
				BbcEngineArgs ea;
				ea.Set(0, 0, engine.CalculateMoveTime(movetime, enemymovetime, inct, einct), 0, 0, 0, 0);
#ifdef THREADS
				std::thread engn(&BbcEngine::MyGo, &engine, &ea);
				engn.detach();
#endif
#ifndef THREADS
				engine.MyGo(&ea);
#endif
			}

			else if (Line == "go infinite")
			{
				cout << "info string start with depth 30 " << endl;
				BbcEngineArgs ea;
				ea.Set(30, 0, 0, 0, 0, 0, 0);
#ifdef THREADS
				std::thread engn(&BbcEngine::MyGo, &engine, &ea);
				engn.detach();
#endif
#ifndef THREADS
				engine.MyGo(&ea);
#endif
			}
			// Received a command like: "go wtime 300000 btime 300000 winc 0 binc 0"
			//Output like: "bestmove h7h5"
		}
	}
	return NULL;
}

void test()
{
	//tr.loadFEN("k7/8/8/8/8/8/8/K7 w - - bm ;");
	/*
	tr.loadFEN("7k/p2R3p/6p1/1P6/5Q2/8/P5PP/6K1 b - -");
	tr.brd.showBoardDebug();
	MoveList ml;
	ml.init(WHITE);
	tr.brd.generateMoveList(&ml, WHITE, 0, 0, 0);
	//cout << tr.brd.getFullBoardScore(1, -INFINITY_SCORE, +INFINITY_SCORE);
	return;
	tr.brd.showBoardDebug();
	tr.prepareZeroLevelMoveList(WHITE);
	char maxdepth = 0;
	tr.maxcheckply = maxdepth + CHECHSEARCHDEPTH + 1;
	mc = tr.ABSearch(maxdepth, 0, WHITE, -INFINITY_SCORE, INFINITY_SCORE, false);
	//mc=tr.ABCaptureSearch(0, 0, WHITE, -INFINITY_SCORE, INFINITY_SCORE);
	mc.showMoveChainDebug();
	*/
	//mc=tr.iterate(MAXLEVEL,3);

	//tr.showStats(mc.getDepth(), mc.getDepth());
	//BB.dumpBITBoards(); //passed
	/* passed - but add srand initialization
    srand (time(NULL));
    BITBOARD rnd=
	    (((BITBOARD) rand() <<  0) & 0x00000000000000FFull) |
		(((BITBOARD) rand() <<  8) & 0x000000000000FF00ull) |
	    (((BITBOARD) rand() << 16) & 0x0000000000FF0000ull) |
	    (((BITBOARD) rand() << 24) & 0x00000000FF000000ull) |
		(((BITBOARD) rand() << 32) & 0x000000FF00000000ull) |
		(((BITBOARD) rand() << 40) & 0x0000FF0000000000ull) |
		(((BITBOARD) rand() << 48) & 0x00FF000000000000ull) |
	    (((BITBOARD) rand() << 56) & 0xFF00000000000000ull);
	showBITBOARD(rnd);
	*/
	//return;
	/*
	Board brd;
	MoveList ml;


	BITBOARD starttime, stoptime;

	ml.init(WHITE);
	starttime=myTime();

	tr.loadFEN("8/8/2B1N3/3rp2K/4k3/7Q/2r3Pn/1b1N4 w - - bm ;"); //mat 7 22 sec

	MoveChain pv;
	pv.setMoveToDepth(5, 0, 0);
	pv.setMoveToDepth(1, 1, 0);
	pv.setMoveToDepth(2, 2, 0);
	pv.setMoveToDepth(1, 3, 0);
	pv.setMoveToDepth(2, 4, 0);
	pv.setMoveToDepth(3, 5, 0);
	pv.setMoveToDepth(2, 6, 0);


	cout << tr.brd.getQuickBoardScore(1);

//#ifdef WITHHASH
//	cout << tr.brd.z_hash <<endl;
//	cout << HSH.getHashChecksum(&tr.brd)<<endl;
//#endif
	return;
*/
	int hashscore;
	int hashbestmove;
	int hashdepth;
	char foundflag;
	MoveList ml;
	tr.loadFEN("7k/p2R3p/6p1/1P6/5Q2/8/P5PP/6K1 b - -");
	tr.brd.showBoardDebug();
	//BITBOARD x=(BITBOARD)65536*65536;
	HSH.hashStore(WHITE,-1,hashfEXACT,10,5,tr.brd.z_hash);
	BITBOARD starttime = myTime();
	for (int i = 0; i < 100000000; i++)
	{
		//tr.brd.generatePLMoveCount(&ml,WHITE);
		//tr.brd.generatePLMoveList(&ml,WHITE);
		//tr.brd.generateMoveList(&ml,WHITE,0,0,0);
		//tr.brd.generateCaptureAndCheckList(&ml, WHITE, 0, 0)
		//tr.brd.generateCaptureList(&ml,WHITE,0,0);
		//tr.brd.getFullBoardScore(WHITE,-INFINITY_SCORE,INFINITY_SCORE);
		//FirstOne(x);
		//tr.brd.underCheck(WHITE);
		//int hashscore;
		//int hashbestmove;
		//int hashdepth;
		//char foundflag;
		//HSH.hashStore(WHITE,-1,hashfEXACT,10,5,tr.brd.z_hash);
		//HSH.hashProbe(WHITE, -1, &hashscore, &hashbestmove, &hashdepth, &foundflag, tr.brd.z_hash);
		HSH.hashProbeQuick(WHITE,tr.brd.z_hash,-1);
		//HSH.hashProbe(&tr.brd, 1, 0, &hashscore, &hashbestmove, &hashdepth, &foundflag);
		//		int a=__builtin_popcountll(12345678);
		//PopCnt(12345678);
	}
	BITBOARD stoptime = myTime();
	cout << "info string time " << (long)stoptime - starttime << endl;

	/*
	brd.initClear();
	brd.setFigure('p', "b7");
	brd.setFigure('P', "a5");
	brd.showBoardDebug();
	brd.generateMoveList(&ml, BLACK,0);
	ml.showMoveListDebug();
	showBITBOARD(brd.b_enpassant);
	brd.makeMove(ml.moves[0],BLACK);
	brd.showBoardDebug();
	getchar();
	ml.init(WHITE);
	brd.generateMoveList(&ml, WHITE,0);
	ml.showMoveListDebug();
	brd.makeCapture(ml.captures[0],WHITE);
	brd.showBoardDebug();
	getchar();
	brd.rollBackCapture(ml.captures[0],WHITE);
	brd.showBoardDebug();
*/
	//}

	/*
    	uint64_t B = (uint64_t)1LU;
    	for( int i=0;i<64;i++){
    		printf("%llu ",B);
    		printf("%d %d %d\n",LastOne(B),FirstOne(B),PopCnt(B));
    		B=(B <<1)+1LL;
    		getchar();
    	}

*/

	//    printf("%lu\n",BB.vectorp8[55]);
	//    printf("%lu\n",(BITBOARD) 1 << 63);
	//    getchar();

	//    for (int i = 0; i < 64; i++) {
	//	showBITBOARD(BB.bpawn_capture_bitboard[i]);
	//	printf("%d\n",i);
	//	getchar();
	//    };
	//    return 1;

	//    brd.initClear();
	//    brd.initDefault();

	//    brd.showBoardDebug();

	/*for(int i=0;i<64; i++)
{
 brd.showBoardDebug();
// showBITBOARD(brd.slonyachafunk(i));
 showBITBOARD(brd.kvinskafunk(i));
 printf("pos %d\n ",i);
 getchar();
 printf("\n\n\n");
}*/

	//  brd.initDefault();
	// brd.setFigure('K', "h8");
	// brd.setFigure('p', "h5");
	// brd.setFigure('k', "a6");
	// brd.setFigure('N', "a1");
	// brd.loadFEN("8/8/2B1N3/3rp2K/4k3/7Q/2r3Pn/1b1N4");//mat 7 22sec
	//  brd.loadFEN("6rk/1p1br2p/pqp5/3pNP2/1P1Pp3/P5PR/5PKR/Q7");//test1 h3-h7 test1
	//  brd.loadFEN("r4rk1/pb1q1ppp/2N1p3/2Rn4/3P4/3BPQ2/5PPP/2R3K1");// b h7 test 2
	//  brd.loadFEN("6rk/2nbNq1p/3p1PpR/2pPp1P1/1nBbP3/2N3K1/1P6/2B4Q");// r h6 h7 test 3
	//  brd.loadFEN("4k3/7K/8/8/8/8/7R/5R2"); //exchange test
	//  brd.loadFEN("7k/1R6/8/8/8/P7/8/K7"); //promotion test
	//  brd.loadFEN("R3k2r/1ppppppp/8/8/8/8/8/r3K2R"); //rokirovka test

	// brd.loadFEN("r2q1rk1/ppp3pp/2nb1n2/5b2/4pP2/2P3PB/PP1P3P/RNBQK2R"); //test

	//brd.showBoardDebug();
	//tr.init(&brd);

	//MoveList ml, oppml;

	//int color=1;
	//ml.init(color);
	//brd.generateMoveList(&ml, color, 0);
	//ml.showMoveListDebug();

	//  short x =brd.getQuickBoardScore(1);
	//  short x =brd.getFullBoardScore(1);
	//  printf("%d",x);
	//  getchar();
	//  return(0);
}
