#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include "hash.h"
#include "utils.h"

static BITBOARD get64rand()
{
	BITBOARD rnd =
		(((BITBOARD)rand() << 0) & 0x00000000000000FFull) |
		(((BITBOARD)rand() << 8) & 0x000000000000FF00ull) |
		(((BITBOARD)rand() << 16) & 0x0000000000FF0000ull) |
		(((BITBOARD)rand() << 24) & 0x00000000FF000000ull) |
		(((BITBOARD)rand() << 32) & 0x000000FF00000000ull) |
		(((BITBOARD)rand() << 40) & 0x0000FF0000000000ull) |
		(((BITBOARD)rand() << 48) & 0x00FF000000000000ull) |
		(((BITBOARD)rand() << 56) & 0xFF00000000000000ull);
	return rnd;
}

BITBOARD Hash::computeHash(Board *brd)
{
	// slow compute. Just for testing

	unsigned char from;
	BITBOARD hash = 0;
	BITBOARD ourfigures;

	ourfigures = brd->w_pawns;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[WHITE][PAWN][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->w_knights;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[WHITE][KNIGHT][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->w_rooks;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[WHITE][ROOK][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->w_bishops;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[WHITE][BISHOP][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->w_kings;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[WHITE][KING][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->w_queens;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[WHITE][QUEEN][from];
		Clear(from, &ourfigures);
	};

	ourfigures = brd->b_pawns;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[BLACK][PAWN][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->b_knights;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[BLACK][KNIGHT][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->b_rooks;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[BLACK][ROOK][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->b_bishops;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[BLACK][BISHOP][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->b_kings;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[BLACK][KING][from];
		Clear(from, &ourfigures);
	};
	ourfigures = brd->b_queens;
	while (ourfigures)
	{
		from = LastOne(ourfigures);
		hash = hash ^ zobrist[BLACK][QUEEN][from];
		Clear(from, &ourfigures);
	};

	return hash;
}

bool Hash::init(int hs)
{
#ifdef TRUERANDOM
	srand(time(NULL));
#endif
	for (unsigned char c = WHITE; c <= BLACK; ++c)
	{
		for (unsigned char pt = PAWN; pt <= KING; pt++)
		{
			for (unsigned char sq = 0; sq <= 63; sq++)
			{
				zobrist[c][pt][sq] = get64rand();
			}
		}
	}

	whiteZ = get64rand();

	hashProbes = 0;
	hashHit = 0;
	hashCollisions = 0;
	htsize = hs;
	hash_full = 0;

	mask = (htsize << 20) - 1;

	checksum = (BITBOARD *)calloc(htsize << 20, sizeof(BITBOARD));
	depth = (char *)calloc(htsize << 20, sizeof(char));
	entrytype = (char *)calloc(htsize << 20, sizeof(char));
	color = (char *)calloc(htsize << 20, sizeof(char));
	score = (int *)calloc(htsize << 20, sizeof(int));
	bestmove = (int *)calloc(htsize << 20, sizeof(int));

	if ((!checksum) || (!depth) || (!entrytype) || (!color) || (!score) || (!bestmove))
	{
		printf("Memori allocation hashtable failed");
		getchar();
		exit(0);
	}

	//flush();
	for (unsigned int i = 0; (unsigned int)i < (htsize << 20); i++)
	{
		checksum[i] = 0;
		depth[i] = 0;
		entrytype[i] = 0;
		color[i] = 0;
		score[i] = 0;
		bestmove[i] = 0;

	}

	return true;
}

void Hash::flush(void)
{

	free(checksum);
	free(depth);
	free(entrytype);
	free(color);
	free(score);
	free(bestmove);

	checksum = (BITBOARD *)calloc(htsize << 20, sizeof(BITBOARD));
	depth = (char *)calloc(htsize << 20, sizeof(char));
	entrytype = (char *)calloc(htsize << 20, sizeof(char));
	color = (char *)calloc(htsize << 20, sizeof(char));
	score = (int *)calloc(htsize << 20, sizeof(int));
	bestmove = (int *)calloc(htsize << 20, sizeof(int));

	if ((!checksum) || (!depth) || (!entrytype) || (!color) || (!score) || (!bestmove))
	{
		printf("Memori allocation hashtable failed");
		getchar();
		exit(0);
	}

	hash_full = 0;
}

void Hash::flushNotExact(void)
{
	return;
	char et;
	hash_full = 0;
	for (unsigned int i = 0; (unsigned int)i < (htsize << 20); i++)
	{
		et = entrytype[i];
		if (et != hashfEXACT)
		{
			checksum[i] = (BITBOARD)0;
		}
		if (et == hashfEXACT)
			hash_full++;
	}
}

BITBOARD Hash::getHashChecksum(Board *brd)
{
	return computeHash(brd);
}

unsigned int Hash::getHashIndex(Board *brd)
{
	BITBOARD test = (htsize << 20) - 1;
	BITBOARD index = computeHash(brd);

	return (unsigned int)index & test;
}

unsigned int Hash::getHashIndex(BITBOARD hsh)
{
	//BITBOARD mask = (htsize << 20) - 1;
	return (unsigned int)hsh & mask;
}

bool Hash::hashProbe(char clr, char dpth, int *scr, int *bstmv, int *hashdepth, char *foundflag, BITBOARD z_hash)
{
	hashProbes++;
	if (clr == WHITE)
		z_hash ^= whiteZ;
	unsigned int index = getHashIndex(z_hash);
	if ((checksum[index] == z_hash) && (clr == color[index]) && (dpth <= depth[index]))
	{
		*foundflag = entrytype[index];
		*scr = score[index];
		*bstmv = bestmove[index];
		*hashdepth = depth[index];
		hashHit++;
		if (bestmove[index] == 0)
		{
			printf("hash probe problem\n");
			exit(0);
		}
		return true;
	}
	return false;
}

int Hash::hashProbeQuick(char clr, BITBOARD z_hash, char dpth)
{
	hashProbes++;
	if (clr == WHITE)
		z_hash ^= whiteZ;
	unsigned int index = getHashIndex(z_hash);
	if (checksum[index] == z_hash)
	{
		//if (clr == color[index])
			if (entrytype[index] == hashfEXACT)
				//if (entrytype[index]==hashfBETA||entrytype[index]==hashfEXACT)
				if(dpth <= depth[index]) // what the heck ? without depth even faster!!
				return score[index];
		//return entrytype[index];
	}
	return -INFINITY_SCORE;
}

int Hash::hashProbeMove(char clr, BITBOARD z_hash, char dpth)
{
	hashProbes++;
	if (clr == WHITE)
		z_hash ^= whiteZ;
	unsigned int index = getHashIndex(z_hash);
	if (checksum[index] == z_hash)
	{
		//if (clr == color[index])
			//if (entrytype[index] == hashfEXACT)
				//if (entrytype[index]==hashfBETA||entrytype[index]==hashfEXACT)
				//if(dpth <= depth[index]) // what the heck ? without depth even faster!!
				return bestmove[index];
		//return entrytype[index];
	}
	return 0;
}

void Hash::hashStore(char clr, char dpth, char entrtp, int scr, int bstmv, BITBOARD z_hash)
{
	if (clr == WHITE)
		z_hash ^= whiteZ;

	unsigned int index = getHashIndex(z_hash);

	if (!checksum[index])
		hash_full++;

	if (checksum[index] == z_hash)
	{
		if (dpth >depth[index])
		{
			hashCollisions++;
			checksum[index] = z_hash;
			entrytype[index] = entrtp;
			score[index] = scr;
			depth[index] = dpth;
			color[index] = clr;
			bestmove[index] = bstmv;
			if (bstmv == 0)
			{
				printf("hash store problem\n");
			}
		}
	}
	else
	{
		checksum[index] = z_hash;
		entrytype[index] = entrtp;
		score[index] = scr;
		depth[index] = dpth;
		color[index] = clr;
		bestmove[index] = bstmv;
		if (bstmv == 0)
		{
			printf("hash store problem\n");
		}
	}
}
