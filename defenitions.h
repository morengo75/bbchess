#include <inttypes.h>

#define PopCnt __builtin_popcountll
#define LastOne __builtin_ctzll
#define FirstOne __builtin_clzll

#ifndef DEFENITIONS
#define DEFENITIONS
#define BITBOARD uint64_t

#define PAWN   1
#define KNIGHT 2
#define BISHOP 3
#define ROOK   4
#define QUEEN  5
#define KING   6

#define MAT 10000
#define INFINITY_SCORE 32767
#define STALEMAT -100

#define WHITE 1
#define BLACK 2

#define hashfEXACT 1
#define hashfALPHA 3
#define hashfBETA 2


//#define MATESEARCH // narrow alpha beta with MAT-30<>MAT

#ifdef MATESEARCH
	#define WITHHASH  //zobrist
	#define WITHHASHSORT1  //lightwieghted hash sort - only 1 move from curent level - not all moves from level+1
	////#define WITHHASHSORT  // not so fast as could be. but will be more efficient when evaluate function becomes more heavy
	#define WITHHASHSTOREQ   //store hash score in q-search
	////#define WITHHASHSTOREBOARDSCORE  //store hash in q - only boardscore
    #define WITHHASHINQ		// use hash score in q-search
	#define WITHALPHABETA
	#define WITHKILLER
	#define FULLBOARDSCORE
	#define EXCHANGESEARCH  //q-search
	#define MVVLVA
	#define MOBILITYBONUS // very bad with silent but deadly test!
	#define LAZYCUTOFF // if static score evaluation> beta dont calculate heavy mobility bonus
	//#define WINDOWSSCALING
	#define EVASIVE  //check evasions
	#define NULLMOVEP
#endif

#ifndef MATESEARCH
	#define WITHHASH  //zobrist
	#define WITHHASHSORT1  //lightwieghted hash sort - only 1 move from curent level - not all moves from level+1
	////#define WITHHASHSORT  // not so fast as could be. but will be more efficient when evaluate function becomes more heavy
	#define WITHHASHSTOREQ   //store hash score in q-search
	////#define WITHHASHSTOREBOARDSCORE  //store hash in q - only boardscore
    #define WITHHASHINQ		// use hash score in q-search
	#define WITHALPHABETA
	#define WITHKILLER
	#define FULLBOARDSCORE
	#define EXCHANGESEARCH  //q-search
	#define MVVLVA
	#define MOBILITYBONUS // very bad with silent but deadly test!
	#define LAZYCUTOFF // if static score evaluation> beta dont calculate heavy mobility bonus
	#define WINDOWSSCALING
	#define EVASIVE  //check evasions
	#define NULLMOVEP

	#define HEAVY_EVAL
#ifdef HEAVY_EVAL
		#define HEAVY_PAWN
		#define HEAVY_PASSED_PAWN
		//#define HEAVY_TROPISM1
		#define HEAVY_TROPISM2
		#define HEAVY_KNIGHTS
		#define HEAVY_BISHOPS
		#define HEAVY_MOBYLITY
#endif
	//#define RANDOMIZER
	#define FOLD3REPEAT
	//#define WITHOPENINGS
#endif


//#define TRUERANDOM
//#define THREADS

#define MAXLEVEL 1
#define MAXTIME 2

//should be substituted by variables
#define CHECKPOINTSTEP 50 //checks if time brake occures
#define CHECKPOINTSTEPINFO 20000000 //prints game state to uci

#define CHECHSEARCHDEPTH 0 // check extensions depth //need to realyze check evasive before

# define MAX_OPENING_SIZE 50000

#define MAXPLY 150
#define MAXHISTORY 1000

#define LAZYCUTOFFSCORE 200  //STS5.0BishopvsKnight.epd max-level 6
//#define LAZYCUTOFFSCORE 150

#define PAWN_MOBILITY_MULT_MG 2
#define PAWN_MOBILITY_MULT_EG 5


#define NULLMOVEP_R 4

//#define DEBUG
//#define TREEDEBUG
//#define MLDEBUG
//#define MOVECHAINDEBUG

// pennies
#define EARLY_QUEEN             -30
#define TEST_KING_SHIELD
#ifndef TEST_KING_SHIELD
	#define HOLE_IN_CASTLE          -40
	#define KING_IN_CASTLE          120
#endif
#ifdef TEST_KING_SHIELD
	#define KING_SHIELD_2LINE		40
	#define KING_SHIELD_3LINE		10
#endif

#define KING_WANDERS			-100
#define ROOK_WANDERS			-20

//#define DE_PAWNS_NOT_DEVELOPED  30
#define DOUBLE_PAWN             -10


/*
#define PawnIsolated		-19
#define PawnBackwards		-5
#define PawnCenter			9
#define PawnPassedFreeMax	128
#define PawnPassedBlockedMax	101
#define PawnPassedKingDist	9
#define PawnPassedSquare	200
#define KnightCenter		27

#define KnightMobility		19
#define BishopCenter		9
#define BishopMobility		72
#define RookMobility		40
#define QueenKingTropism	99
#define KingCenterMid		-41
#define KingCenterEnd		33
//#define KingPawnShield		120
*/

#define KNIGHT_TRAPPED_H8  -150
#define KNIGHT_TRAPPED_H7  -100

#define BISHOP_TRAPPED_H7  -150
#define BISHOP_TRAPPED_H6  -50

#ifdef HEAVY_TROPISM1
	#define QUEEN_THROPISM_MG			-15 // how near enemy figures to our king
	#define KNIGHT_THROPISM_MG			-8
	#define ROOK_THROPISM_MG			-5
	#define QUEEN_THROPISM_EG			-50 // how near enemy figures to our king
	#define KNIGHT_THROPISM_EG			-25
	#define ROOK_THROPISM_EG			-15
#endif
//#define KNIGHT_OUTPOST				7 // subst by tables

#define PAWN_ISOLATED			-20
#define PAWN_BACKWARDS			-5
#define PAWN_PASSED_FREE_MAX	128

#define ROOK_7HT			30 // sdb02 haha . orig 24
#define ROOK_OPEN			25
#define ROOK_CONNECTED      15

#define BISHOP_PAIR_MG  35
#define BISHOP_PAIR_EG  65

#define LONE_BISHOP_PAWN_SAME_COLOR -3

// scores
#define PAWN_MG 100
#define PAWN_EG 140

#define KNIGHT_MG 350
#define KNIGHT_EG 370

#define BISHOP_MG 400
#define BISHOP_EG 430

#define ROOK_MG 600
#define ROOK_EG 650

#define QUEEN_MG 1050
#define QUEEN_EG 1150


//pawn hunt squares
#define W_PAWN_EVAL_SQS 0b0000000011111111111111111111111111111111111111110000000000000000LU
#define B_PAWN_EVAL_SQS 0b0000000000000000111111111111111111111111111111111111111100000000LU

//kings places^M
#define A1B1C1D1 0xFLU
#define F1G1H1 0xE0LU

#define A8B8C8D8 0xF00000000000000LU
#define F8G8H8  0xE000000000000000LU



//cout << "#define A6B6C6 0x" <<std::hex << BITBOARD(A6|B6|C6) <<"LU" <<std::flush;
// pawn shield
#define F3G3H3 0xE00000LU
#define A1B1C1 0x7LU
#define A3B3C3 0x70000LU
#define F6G6H6 0xE00000000000LU
#define F3G3H3 0xE00000LU
#define A8B8C8 0x700000000000000LU
#define A6B6C6 0x70000000000LU

//knight outost squares
//#define W_KNIGHT_OUTPOST_SQS 0b0000000000000000111111111111111111111111000000000000000000000000LU
//#define B_KNIGHT_OUTPOST_SQS 0b0000000000000000000000001111111111111111111111110000000000000000LU

//black/white squares
#define BLACK_SQS 0b1010101001010101101010100101010110101010010101011010101001010101LU
#define WHITE_SQS 0b0101010110101010010101011010101001010101101010100101010110101010LU

#define A1H1  0xFFLU
#define A2H2  0xFF00LU
#define A3H3  0xFF0000LU
#define A4H4  0xFF000000LU
#define A5H5  0xFF00000000LU
#define A6H6  0xFF0000000000LU
#define A7H7  0xFF000000000000LU
#define A8H8  0xFF00000000000000LU

#define A1 0x1LU
#define B1 0x2LU
#define C1 0x4LU
#define D1 0x8LU
#define E1 0x10LU
#define F1 0x20LU
#define G1 0x40LU
#define H1 0x80LU

#define F1G1 0x60LU
#define B1C1D1 0xELU

#define A2 0x100LU
#define B2 0x200LU
#define C2 0x400LU
#define D2 0x800LU
#define E2 0x1000LU
#define F2 0x2000LU
#define G2 0x4000LU
#define H2 0x8000LU

#define D2E2 0x1800LU
#define A2B2C2 0x700LU
#define F2G2H2 0xE000LU

#define A3 0x10000LU
#define B3 0x20000LU
#define C3 0x40000LU
#define D3 0x80000LU
#define E3 0x100000LU
#define F3 0x200000LU
#define G3 0x400000LU
#define H3 0x800000LU

#define A4 0x1000000LU
#define B4 0x2000000LU
#define C4 0x4000000LU
#define D4 0x8000000LU
#define E4 0x10000000LU
#define F4 0x20000000LU
#define G4 0x40000000LU
#define H4 0x80000000LU

#define A5 0x100000000LU
#define B5 0x200000000LU
#define C5 0x400000000LU
#define D5 0x800000000LU
#define E5 0x1000000000LU
#define F5 0x2000000000LU
#define G5 0x4000000000LU
#define H5 0x8000000000LU

#define A6 0x10000000000LU
#define B6 0x20000000000LU
#define C6 0x40000000000LU
#define D6 0x80000000000LU
#define E6 0x100000000000LU
#define F6 0x200000000000LU
#define G6 0x400000000000LU
#define H6 0x800000000000LU

#define A7 0x1000000000000LU
#define B7 0x2000000000000LU
#define C7 0x4000000000000LU
#define D7 0x8000000000000LU
#define E7 0x10000000000000LU
#define F7 0x20000000000000LU
#define G7 0x40000000000000LU
#define H7 0x80000000000000LU

#define D7E7 0x18000000000000LU
#define A7B7C7 0x7000000000000LU
#define F7G7H7 0xE0000000000000LU


#define A8 0x100000000000000LU
#define B8 0x200000000000000LU
#define C8 0x400000000000000LU
#define D8 0x800000000000000LU
#define E8 0x1000000000000000LU
#define F8 0x2000000000000000LU
#define G8 0x4000000000000000LU
#define H8 0x8000000000000000LU

#define F8G8 0x6000000000000000LU
#define B8C8D8 0xE00000000000000LU

#define POSA1 0
#define POSB1 1
#define POSC1 2
#define POSD1 3
#define POSE1 4
#define POSF1 5
#define POSG1 6
#define POSH1 7

#define POSA2 8
#define POSB2 9
#define POSC2 10
#define POSD2 11
#define POSE2 12
#define POSF2 13
#define POSG2 14
#define POSH2 15

#define POSA3 16
#define POSB3 17
#define POSC3 18
#define POSD3 19
#define POSE3 20
#define POSF3 21
#define POSG3 22
#define POSH3 23

#define POSA4 24
#define POSB4 25
#define POSC4 26
#define POSD4 27
#define POSE4 28
#define POSF4 29
#define POSG4 30
#define POSH4 31

#define POSA5 32
#define POSB5 33
#define POSC5 34
#define POSD5 35
#define POSE5 36
#define POSF5 37
#define POSG5 38
#define POSH5 39

#define POSA6 40
#define POSB6 41
#define POSC6 42
#define POSD6 43
#define POSE6 44
#define POSF6 45
#define POSG6 46
#define POSH6 47

#define POSA7 48
#define POSB7 49
#define POSC7 50
#define POSD7 51
#define POSE7 52
#define POSF7 53
#define POSG7 54
#define POSH7 55

#define POSA8 56
#define POSB8 57
#define POSC8 58
#define POSD8 59
#define POSE8 60
#define POSF8 61
#define POSG8 62
#define POSH8 63

#endif
