#ifndef BITBOARDS
#define BITBOARDS

#include "defenitions.h"


class BITBoards {

  public:
    BITBOARD knight_bitboard[64];
    BITBOARD king_bitboard[64];

    BITBOARD wpawn_forward_bitboard[64];
    BITBOARD bpawn_forward_bitboard[64];
    BITBOARD wpawn_capture_bitboard[64];
    BITBOARD bpawn_capture_bitboard[64];

    BITBOARD vectorm9[64];
    BITBOARD vectorm8[64];
    BITBOARD vectorm7[64];
    BITBOARD vectorm1[64];

    BITBOARD vectorp9[64];
    BITBOARD vectorp8[64];
    BITBOARD vectorp7[64];
    BITBOARD vectorp1[64];

    BITBOARD bbline[8];

    BITBOARD distance_sq[64][8];

    BITBOARD wpawn_half_passed[64]; //left and right column
    BITBOARD bpawn_half_passed[64];

    BITBOARD wpawn_connected_bitboard[64]; //   +*+
    BITBOARD bpawn_connected_bitboard[64]; //   + +

    BITBOARD bitpos[64];

  private:
    void fillVectors();

  public:
    void init();
    void dumpBITBoards();
};
#endif
